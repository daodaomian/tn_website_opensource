<?php


namespace app\common\exception;


class CategoryException extends BaseException
{
    public $code = 404;
    public $msg = '对应栏目暂无数据';
    public $errorCode = 20100;
}