<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-12-03
 * Time: 15:56
 */

namespace app\api\middleware\mp\v1;

use app\common\exception\LotteryException;
use app\common\exception\ParameterException;
use app\common\model\Lottery as LotteryModel;

class CheckLottery
{
    public function handle($request, \Closure $next)
    {
        // 判断是否当前抽奖是否存在或者开启
        if ($request->param('id')) {
            if (!LotteryModel::checkLotteryIsExist($request->param('id'))) {
                throw new LotteryException();
            }
        } else {
            throw new ParameterException();
        }

        return $next($request);
    }
}