<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-06
 * Time: 08:45
 */

namespace app\api\controller\mp\v1;


use app\api\BaseController;
use app\common\enum\OrderType;
use app\common\model\Order as OrderModel;

class Order extends BaseController
{
    /**
     * 发起预支付请求
     * @http post
     * @url /pay/pre_appreciate_order
     * :amount 金额
     * :allow_pay_subscribe 是否允许支付通知
     * :allow_refund_subscribe 是否允许退款通知
     * @return \think\response\Json
     */
    public function preCreateAppreciateOrder()
    {
        // 获取赞赏的金额数据以及相关数据
        $data = $this->request->post(['amount' => 0.01,
            'allow_pay_subscribe' => 0,'allow_refund_subscribe' => 0, 'allow_submit_success_subscribe' => 0, 'allow_pay_timeout_subscribe' => 0]);

        // 调用微信官方的统一下单接口
        $pre_data = OrderModel::createOrder([
            'body' => '图鸟科技-支付测试',
            'total_fee' => $data['amount'],
            'notify_url' => 'https://website.tnkjapp.com/tn/wx/pay/notify',
            'attach' => ['type' => OrderType::Appreciate],
            'allow_pay_subscribe' => $data['allow_pay_subscribe'],
            'allow_refund_subscribe' => $data['allow_refund_subscribe'],
            'allow_submit_success_subscribe' => $data['allow_submit_success_subscribe'],
            'allow_pay_timeout_subscribe' => $data['allow_pay_timeout_subscribe'],
            'type' => OrderType::Appreciate
        ]);

        return tn_yes('发起预支付成功', ['data' => $pre_data]);
    }

    /**
     * 发起图鸟商店订单预支付请求
     * @http post
     * @url /pay/pre_tn_shop_order
     * @return \think\response\Json
     */
    public function preCreateTNShopOrder()
    {
        // 获取用户下单的商品数据和收货相关信息
        $data = $this->request->post(['specs_data', 'address', 'notes' => '',
            'allow_pay_subscribe' => 0, 'allow_submit_success_subscribe' => 0, 'allow_pay_timeout_subscribe' => 0]);

        $pre_data = OrderModel::createTNShopOrder($data, 'https://website.tnkjapp.com/tn/wx/pay/notify');

        return tn_yes('发起预支付成功', ['data' => $pre_data]);
    }

    /**
     * 支付图鸟商店订单
     * @http post
     * @url /pay/pay_tn_shop_order
     * @return \think\response\Json
     */
    public function payTNShopOrder()
    {
        $id = $this->request->post('id', 0);

        $pre_data = OrderModel::payTNShopOrder($id);

        return tn_yes('发起再支付成功', ['data' => $pre_data]);
    }

    /**
     * 用户取消支付
     * @http DELETE
     * @url /pay/cancel_order
     * @order_no 订单编号
     */
    public function cancelOrder()
    {
        $order_no = $this->request->delete('order_no', '');

        OrderModel::cancelOrder($order_no);
    }

    /**
     * 用户取消图鸟商店订单
     * @http DELETE
     * @url /pay/cancel_tn_shop_order
     * @order_no 订单编号
     */
    public function cancelTNShopOrder()
    {
        $order_no = $this->request->delete('order_no', '');

        OrderModel::cancelTNShopOrder($order_no);
    }

    /**
     * 设置发货通知提醒
     * @http post
     * @url /tn_shop_order/set_delivery_subscribe
     * @return \think\response\Json
     */
    public function setAllowDeliverySubscribe()
    {
        $params = $this->request->post(['order_no','allow_delivery_subscribe' => 0]);

        OrderModel::setAllowDeliverySubscribe($params);

        return tn_yes('设置发货通知提醒成功');
    }

    /**
     * 获取商店订单的分页数据
     * @http get
     * @url /tn_shop_order/get_list
     * @return \think\response\Json
     */
    public function getTNShopOrderList()
    {
        $params = $this->request->get(['type' => 0, 'title' => '', 'page', 'limit']);

        $data = OrderModel::getShopOrderPaginationList($params);

        return tn_yes('获取对应订单分类的分页内容数据成功', ['data' => $data]);
    }

    /**
     * 根据id获取图鸟商店订单信息
     * @http get
     * @url /tn_shop_order/get_by_id
     * @return \think\response\Json
     */
    public function getTNShopOrderDetail()
    {
        $id = $this->request->get('id',0);

        $data = OrderModel::getShopOrderDetail($id);

        return tn_yes('获取订单详细信息成功', ['data' => $data]);
    }

    /**
     * 根据id对图鸟商店订单进行收货操作
     * @http post
     * @url /tn_shop_order/confirm_order_receipt
     * @return \think\response\Json
     */
    public function confirmReceiptTNShopOrder()
    {
        $id = $this->request->post('id',0);

        $status = OrderModel::confirmTNShopOrderReceipt($id);

        if ($status) {
            return tn_yes('确认图鸟订单收货成功');
        } else {
            return tn_no('确认图鸟订单收货失败');
        }
    }

    /**
     * 对图鸟商店订单发起退款申请
     * @http post
     * @url /tn_shop_order/launch_order_refund
     * @return \think\response\Json
     */
    public function launchTNShopOrderRefund()
    {
        $params = $this->request->post(['id','refund_reason','allow_refund_subscribe' => 0]);

        $status = OrderModel::launchTNShopOrderRefund($params);

        if ($status) {
            return tn_yes('发起图鸟商店订单退款申请成功');
        } else {
            return tn_no('发起图鸟商店订单退款申请失败');
        }
    }

    /**
     * 关闭图鸟商店订单退款申请
     * @http delete
     * @url /tn_shop_order/close_refund_application
     * @return \think\response\Json
     */
    public function closeTNShopRefundApplication()
    {
        $id = $this->request->delete('id');

        $status = OrderModel::closeTNShopRefundApplication($id);

        if ($status) {
            return tn_yes('关闭图鸟商店订单退款申请成功');
        } else {
            return tn_no('关闭图鸟商店订单退款申请失败');
        }
    }

    /**
     * 关闭图鸟商店订单
     * @http delete
     * @url /tn_shop_order/close
     * @return \think\response\Json
     */
    public function closeTNShopOrder()
    {
        $id = $this->request->delete('id',0);

        OrderModel::closeTNShopOrder($id);

        return tn_yes('关闭图鸟商店订单成功');
    }

    /**
     * 删除图鸟商店订单
     * @http delete
     * @url /tn_shop_order/delete
     * @return \think\response\Json
     */
    public function deleteTNShopOrder()
    {
        $id = $this->request->delete('id',0);

        $status = OrderModel::deleteTNShopOrder($id);

        if ($status) {
            return tn_yes('删除图鸟订单成功');
        } else {
            return tn_no('删除图鸟商店订单失败');
        }
    }
}