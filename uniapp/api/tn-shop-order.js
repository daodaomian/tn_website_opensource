import { request } from '@/utils/request'

export function getTNShopOrderList(data) {
  return request({
    url: "tn_shop_order/get_list",
    method: 'get',
    data
  })
}

export function getTNShopOrderByID(data) {
  return request({
    url: "tn_shop_order/get_by_id",
    method: 'get',
    data
  })
}

export function setTNShopAllowDeliverySubscribe(data) {
  return request({
    url: "tn_shop_order/set_delivery_subscribe",
    method: 'post',
    data
  })
}

export function closeTNShopOrder(data) {
  return request({
    url: "tn_shop_order/close",
    method: 'delete',
    data
  })
}

export function deleteTNShopOrder(data) {
  return request({
    url: "tn_shop_order/delete",
    method: 'delete',
    data
  })
}

export function confirmReceiptTNShopOrder(data) {
  return request({
    url: "tn_shop_order/confirm_receipt",
    method: 'post',
    data
  })
}

export function launchOrderRefundTNShopOrder(data) {
  return request({
    url: "tn_shop_order/launch_order_refund",
    method: 'post',
    data
  })
}

export function closeTNShopRefundApplication(data) {
  return request({
    url: "tn_shop_order/close_refund_application",
    method: 'delete',
    data
  })
}
