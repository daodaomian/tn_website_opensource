<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-10-27
 * Time: 17:44
 */

namespace app\common\model;


class ShopProductSpecs extends BaseModel
{
    protected $autoWriteTimestamp = false;

    public function property()
    {
        return $this->hasMany('ShopProductSpecsProperty','specs_id','id');
    }

    public function product()
    {
        return $this->belongsTo('ShopProduct','product_id','id');
    }

    public function getProductImageAttr($value, $data)
    {
        return [
            'value' => $value,
            'prefix' => $this->prefixImgUrl($value)
        ];
    }
}