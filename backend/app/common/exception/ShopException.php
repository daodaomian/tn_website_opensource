<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-10-07
 * Time: 10:06
 */

namespace app\common\exception;


class ShopException extends BaseException
{
    public $code = 404;
    public $msg = '图鸟商店订单活动没有对应数据';
    public $errorCode = 40203;
}