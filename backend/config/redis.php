<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-04-10
 * Time: 17:48
 */
declare(strict_types = 1);

return [
    // 全局缓存有效期(0为永久有效)
    'expire' => env('redis.expire', 0),
    // 缓存前缀
    'prefix' => env('redis.prefix', 'tuniao_'),
    // 服务器地址
    'host' => env('redis.host', '127.0.0.1'),
    'port' => env('redis.port', 6379),
    // 密码
    'password' => env('redis.password',''),
];
