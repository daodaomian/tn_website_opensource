<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-25
 * Time: 14:47
 */

namespace app\admin\controller;


use app\admin\BaseController;
use app\admin\model\BannerPos as BannerPosModel;

class BannerPos extends BaseController
{
    /**
     * 获取轮播位分页数据
     * @http get
     * @url /banner_pos/list
     * @return \think\response\Json
     */
    public function getList()
    {
        $params = $this->request->get();

        $data = BannerPosModel::getPaginationList($params);

        return tn_yes('获取轮播位分页信息成功', return_vue_element_admin_pagination_data($data));
    }

    /**
     * 根据id获取对应的轮播位信息
     * @http get
     * @url /banner_pos/get_id
     * @return \think\response\Json
     */
    public function getByID()
    {
        $id = $this->request->get('id',0);

        $data = BannerPosModel::getDataWithID($id);

        return tn_yes('获取轮播位信息成功', ['data' => $data]);
    }

    /**
     * 获取轮播位全部名称
     * @http get
     * @url /banner_pos/get_all_title
     * @return \think\response\Json
     */
    public function getAllTitle()
    {
        $data = BannerPosModel::getAllBannerPosTitle();

        return tn_yes('获取轮播位全部名称成功', ['data' => $data]);
    }

    /**
     * 添加轮播位信息
     * @http post
     * @url /banner_pos/add
     * @return \think\response\Json
     */
    public function addBannerPos()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['title']);

        $result = BannerPosModel::addBannerPos($data);

        if ($result) {
            $this->request->log_content = '添加轮播位成功';
            return tn_yes('添加轮播位成功');
        } else {
            $this->request->log_content = '添加轮播位失败';
            return tn_no('添加轮播位失败');
        }
    }

    /**
     * 编辑轮播位位信息
     * @http put
     * @url /banner_pos/edit
     * @return \think\response\Json
     */
    public function editBannerPos()
    {
        $this->checkPutUrl();

        $data = $this->request->put(['id','title']);

        $result = BannerPosModel::editBannerPos($data);

        if ($result) {
            $this->request->log_content = '编辑轮播位成功';
            return tn_yes('编辑轮播位成功');
        } else {
            $this->request->log_content = '编辑轮播位失败';
            return tn_no('编辑轮播位失败');
        }
    }

    /**
     * 删除轮播位信息
     * @http delete
     * @url /banner_pos/delete
     * @return \think\response\Json
     */
    public function deleteBannerPos()
    {
        $this->checkDeleteUrl();

        $ids = $this->request->delete('ids');

        $result = BannerPosModel::deleteBannerPos($ids);

        if ($result) {
            $this->request->log_content = '删除轮播位成功';
            return tn_yes('删除轮播位成功');
        } else {
            $this->request->log_content = '删除轮播位失败';
            return tn_no('删除轮播位失败');
        }
    }
}