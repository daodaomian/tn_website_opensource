<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-25
 * Time: 13:59
 */

namespace app\admin\validate;


use app\common\validate\BaseValidate;

class ShopActivitiesAward extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'activities_id' => 'require|number|gt:0',
        'order_id' => 'require|gt:0',
        'award_content' => 'require',
        'award_status' => 'require|number|between:0,1',
        'excel_file' => 'require'
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'activities_id.require' => '所属活动不能为空',
        'activities_id.number' => '所属活动不能为空编号格式错误',
        'activities_id.gt' => '所属活动不能为空编号格式错误',
        'order_id.require' => '订单编号不能为空',
        'order_id.number' => '订单编号格式错误',
        'award_content.require' => '领奖内容不能为空',
        'award_status.require' => '领奖内容状态不能为空',
        'award_status.number' => '领奖内容状态格式不正确',
        'award_status.between' => '领奖内容状态格式不正确',
        'excel_file.require' => '需要导入的excel文件不能为空'
    ];

    protected $scene = [
        'add' => ['activities_id','award_content','award_status'],
        'edit' => ['id','activities_id','award_content','award_status'],
        'add_order' => ['id', 'order_id', 'award_status'],
        'add_excel' => ['activities_id', 'excel_file']
    ];
}