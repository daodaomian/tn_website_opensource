import reuqest from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function getTableTreeData(params) {
  return reuqest({
    url: api_prefix + 'auth_rule/table_tree',
    method: 'get',
    params
  })
}

export function getRuleElementTreeData() {
  return reuqest({
    url: api_prefix + 'auth_rule/element_tree',
    method: 'get'
  })
}

export function getAuthRuleByID(id) {
  return reuqest({
    url: api_prefix + 'auth_rule/get_id',
    method: 'get',
    params: { id }
  })
}

export function getAllRuleNode() {
  return reuqest({
    url: api_prefix + 'auth_rule/all_rule',
    method: 'get'
  })
}

export function getChildrenCount(pid) {
  return reuqest({
    url: api_prefix + 'auth_rule/get_children_count',
    method: 'get',
    params: { pid }
  })
}

export function addAuthRule(data) {
  return reuqest({
    url: api_prefix + 'auth_rule/add',
    method: 'post',
    data
  })
}

export function editAuthRule(data) {
  return reuqest({
    url: api_prefix + 'auth_rule/edit',
    method: 'put',
    data
  })
}

export function updateAuthRule(data) {
  return reuqest({
    url: api_prefix + 'auth_rule/update',
    method: 'put',
    data
  })
}

export function deleteAuthRule(ids) {
  return reuqest({
    url: api_prefix + 'auth_rule/delete',
    method: 'delete',
    data: { ids }
  })
}
