<?php


namespace app\admin\validate;


use app\common\validate\BaseValidate;

class ModelTable extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'cn_name' => 'require|max:60|chsDash|unique:ModelTable,cn_name',
        'en_name' => 'require|max:60|alphaDash|unique:ModelTable,en_name',
        'fields_data' => 'checkFieldData',
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'cn_name.require' => '模型名称不能为空',
        'cn_name.max' => '模型名称最大长度为60',
        'cn_name.chsDash' => '模型名称存在非法字符',
        'cn_name.unique' => '模型名称不能重复',
        'en_name.require' => '模型表名称不能为空',
        'en_name.max' => '模型表名称最大长度为60',
        'en_name.alphaDash' => '模型表名称存在非法字符',
        'en_name.unique' => '模型表名称不能重复',
    ];

    protected $scene = [
        'add' => ['cn_name','en_name','fields_data'],
        'edit' => ['id','cn_name','en_name','fields_data'],
    ];

    /**
     * 验证哦行字段信息
     * @param $value
     * @param $rule
     * @param array $data
     * @return string
     */
    protected function checkFieldData($value, $rule, $data = [])
    {
        // 如果需要验证的值不是数组放回false
        if (!is_array($value)) {
            return '模型字段数据格式错误';
        }

        if (!$this->checkFieldEnnameUnique($value)) {
            return '模型字段英文名称有重复';
        }

        foreach ($value as $item) {
            if (empty($item['cn_name'])) {
                return '模型字段中文名称不能为空';
            }
            if (!(is_scalar($item['cn_name']) && 1 === preg_match('/^[\x{4e00}-\x{9fa5}a-zA-Z0-9\_\-]+$/u', (string) $item['cn_name'])) ||
                !$this->max($item['cn_name'], 60)) {
                return '模型字段中文名称格式不正确';
            }
            if (empty($item['en_name'])) {
                return '模型字段英文名称不能为空';
            }
            if (!(is_scalar($item['en_name']) && 1 === preg_match('/^[A-Za-z0-9\-\_]+$/', (string) $item['en_name'])) ||
                !$this->max($item['en_name'], 60)) {
                return '模型字段英文名称格式不正确';
            }
            if (empty($item['type'])) {
                return '模型字段类型不能为空';
            }
            if (!ctype_digit((string) $item['type']) ||
                !$this->between($item['type'], '1,11')) {
                return '模型字段类型格式不正确';
            }
            if (!$this->max($item['values'], 255)) {
                return '模型字段可选值格式不正确';
            }
            if (!$this->max($item['tips'], 255)) {
                return '模型字段提示信息格式不正确';
            }
        }

        return true;
    }

    /**
     * 检查字段的英文名在当前的模型中是否唯一
     * @param array $data
     * @return bool
     */
    protected function checkFieldEnnameUnique($data)
    {
        // 提取en_name
        $value = [];

        foreach ($data as $item) {
            $value[] = $item['en_name'];
        }

        // 先判断提交的数据中有无重复的
        $rawLen = count($value);
        $uniqueValue = array_unique($value);
        $uniqueLen = count($uniqueValue);
        if ($rawLen != $uniqueLen) {
            return false;
        }

        return true;
    }
}