<?php


namespace app\common\exception;


class AuthorizeFailException extends BaseException
{
    public $code = 401;
    public $msg = '非法访问';
    public $errorCode = '10100';
}