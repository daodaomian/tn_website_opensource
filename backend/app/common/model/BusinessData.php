<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-05-26
 * Time: 21:27
 */

namespace app\common\model;


class BusinessData extends BaseModel
{
    protected $autoWriteTimestamp = false;

    protected $pk = 'title_id';
}