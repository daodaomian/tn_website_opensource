<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-10-29
 * Time: 18:19
 */

namespace app\admin\controller;


use app\admin\BaseController;
use app\common\model\ShopProduct as ShopProductModel;

class ShopProduct extends BaseController
{
    /**
     * 获取商店商品分页列表数据
     * @http get
     * @url /shop_product/list
     * @return \think\response\Json
     */
    public function getList()
    {
        $params = $this->request->get();

        $data = ShopProductModel::getPaginationList($params);

        return tn_yes('获取商店商品列表数据成功',return_vue_element_admin_pagination_data($data));
    }

    /**
     * 获取商店商品全部标题信息
     * @http get
     * @url /shop_product/get_all_title
     * @return \think\response\Json
     */
    public function getAllProductTitle()
    {
        $title = $this->request->get('title', '');

        $data = ShopProductModel::getAllProductTitle($title);

        return tn_yes('获取商店商品全部标题信息成功', ['data' => $data]);
    }

    /**
     * 获取商店商品标题规格数据
     * @http get
     * @url /shop_product/get_all_product_specs
     * @return \think\response\Json
     */
    public function getAllProductSpecs()
    {
        $data = $this->request->get(['title' => '']);

        $data = ShopProductModel::getAllProductSpecsData($data);

        return tn_yes('获取商店商品标题规格数据成功', ['data' => $data]);
    }

    /**
     * 根据id获取商店商品数据
     * @http get
     * @url /shop_product/get_id
     * @return \think\response\Json
     */
    public function getByID()
    {
        $id = $this->request->get('id',0);

        $data = ShopProductModel::getProductByID($id);

        return tn_yes('获取商店商品数据成功', ['data' => $data]);
    }

    /**
     * 获取对应分类的商品数量
     * @http get
     * @url /shop_product/get_category_count
     * @return \think\response\Json
     */
    public function getCategoryProductCount()
    {
        $category_id = $this->request->get('category_id', 0);

        $count = ShopProductModel::getChildrenCount($category_id,'category_id');

        return tn_yes('获取分类的商品数量成功', ['count' => $count]);
    }

    /**
     * 获取全部商品的数量
     * @http get
     * @url /shop_product/get_all_count
     * @return \think\response\Json
     */
    public function getAllCount()
    {
        $count = ShopProductModel::getDataAllCount();

        return tn_yes('获取全部商品数量成功', ['count' => $count]);
    }

    /**
     * 添加商品
     * @http post
     * @url /shop_product/add
     * @return \think\response\Json
     */
    public function addProduct()
    {
        $this->checkPostUrl();

        $data = $this->request->post(['title','main_image','category_id','keyword','desc','content',
            'virtual_sales_volume','virtual_product' => 0,'sort' => 1, 'recomm' => 0, 'status' => 1,
            'property','specs', 'parameters'
            ]);

        $result = ShopProductModel::addProduct($data);

        if ($result) {
            $this->request->log_content = '添加商店商品成功';
            return tn_yes('添加商店商品成功');
        } else {
            $this->request->log_content = '添加商店商品失败';
            return tn_no('添加商店商品失败');
        }
    }

    /**
     * 编辑商品
     * @http post
     * @url /shop_product/edit
     * @return \think\response\Json
     */
    public function editProduct()
    {
        $this->checkPutUrl();

        $data = $this->request->post(['id','title','main_image','category_id','keyword','desc','content',
            'virtual_sales_volume','virtual_product' => 0,'sort' => 1, 'recomm' => 0, 'status' => 1,
            'property','specs', 'parameters'
        ]);

        $result = ShopProductModel::editProduct($data);

        if ($result) {
            $this->request->log_content = '编辑商店商品成功';
            return tn_yes('编辑商店商品成功');
        } else {
            $this->request->log_content = '编辑商店商品失败';
            return tn_no('编辑商店商品失败');
        }
    }

    /**
     * 更新商店商品信息
     * @http PUT
     * @url /shop_product/update
     * @return \think\response\Json
     */
    public function updateProduct()
    {
        $this->checkPutUrl();

        // 获取put的数据
        $data = $this->request->put();

        $this->checkUpdateValidate($data);

        $result = ShopProductModel::updateInfo($data['id'],[
            $data['field'] => $data['value']
        ]);

        if ($result) {
            $this->request->log_content = '更新商店商品信息成功';
            return tn_yes('更新商店商品信息成功');
        }else {
            $this->request->log_content = '更新商店商品信息失败';
            return tn_no('更新商店商品信息失败');
        }
    }

    /**
     * 删除指定的商店商品信息
     * @http DELETE
     * @url /shop_product/delete
     * @return \think\response\Json
     */
    public function deleteProduct()
    {
        $this->checkDeleteUrl();

        $data = $this->request->delete('ids');

        $result = ShopProductModel::delByIDs($data);

        if ($result) {
            $this->request->log_content = '删除商店商品信息成功';
            return tn_yes('删除商店商品信息成功');
        }else {
            $this->request->log_content = '删除商店商品信息失败';
            return tn_no('删除商店商品信息失败');
        }
    }
}