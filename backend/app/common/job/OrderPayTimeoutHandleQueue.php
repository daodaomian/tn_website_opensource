<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-10-20
 * Time: 21:38
 */

namespace app\common\job;


use app\api\model\mp\v1\OrderSubscribeMessage;
use app\api\model\mp\v1\TNShopOrderNotifyTemplateMessage;
use app\common\enum\OrderEnum;
use app\common\enum\OrderType;
use think\queue\Job;
use app\common\model\Order as OrderModel;

class OrderPayTimeoutHandleQueue
{
    /**
     * fire是消息队列默认调用的方法
     * @param Job $job 当前的任务对象
     * @param array|mixed $data 发布任务时自定义的数据
     */
    public function fire(Job $job, $data)
    {
        //有效消息到达消费者时可能已经不再需要执行了
        if (!$this->checkJob($data)) {
            $job->delete();
            return;
        }
        //执行业务处理
        if ($this->doJob($data)) {
            $job->delete();//任务执行成功后删除
            echo '[订单支付超时关闭通知]订单'.$data['order_no'].'通知成功'.PHP_EOL;
        } else {
            //检查任务重试次数
            if ($job->attempts() > 3) {
                echo '[订单支付超时关闭通知]订单'.$data['order_no'].'通知失败，达到最大重试数'.PHP_EOL;
                $job->delete();
            }
        }
    }

    /**
     * 接收队列消息的失败回调和告警
     * @param $e 消息队列出错的相关信息
     */
    public function failed($e)
    {
        print_r('消息队列出错，出错信息如下');
        var_dump($e);
    }

    /**
     * 简单当前订单是否需要执行本消息
     * @param array|mixed $data 发布任务时自定义的数据
     * @return boolean 任务执行的结果
     */
    private function checkJob($data)
    {
        $order_no = $data["order_no"];

        // 判断订单是否存在或者是否为已提交状态
        if (OrderModel::checkOrderStatusByOrderNo($order_no, OrderEnum::CREATE_ORDER)) {
            return true;
        }

        (new OrderModel())->db()->getConnection()->close();
        return false;
    }

    /**
     * 根据消息中的数据进行实际的业务处理
     */
    private function doJob($data)
    {
        // 实际业务流程处理
        $order_no = $data["order_no"];

        // 更新订单信息
        OrderModel::updateByOrderNo($order_no, ['status', 'order_end_time'], [
            'status' => OrderEnum::CREATE_PAY_TIMEOUT,
            'order_end_time' => time()
        ]);

        // 恢复扣除的商品库存
        OrderModel::recoveryOrderStock($order_no);

        // 删除对应订单的微信支付信息
        OrderModel::deleteTnShopWxPayOptionsFromCache($order_no);

        (new OrderModel())->db()->getConnection()->close();

        return true;
    }
}