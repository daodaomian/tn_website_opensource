<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-06-05
 * Time: 21:47
 */

namespace app\common\controller;


use app\BaseController;
use app\common\service\WxPay as WxPayService;

class WxPay extends BaseController
{
    /**
     * 接收微信支付通知的信息
     */
    public function notify()
    {
        return (new WxPayService())->notifyOrder();
    }

    /**
     * 接收微信退款通知的消息
     */
    public function notifyRefund()
    {
        return (new WxPayService())->notifyRefundOrder();
    }
}