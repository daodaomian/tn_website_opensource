<?php
use think\facade\App;

return [
    'token'          => '',
    'appid'          => '',
    'appsecret'      => '',
    'encodingaeskey' => '',
    // 配置商户支付参数
    'mch_id'         => "[商户id]",
    'mch_key'        => '[商户密钥]',
    // 配置商户支付双向证书目录 （p12 | key,cert 二选一，两者都配置时p12优先）
    'ssl_p12'        => App::getRootPath() . 'tn_wx_cert/[证书名称包含后缀]',
    // 'ssl_key'        => __DIR__ . DIRECTORY_SEPARATOR . 'cert' . DIRECTORY_SEPARATOR . '1332187001_20181030_key.pem',
    // 'ssl_cer'        => __DIR__ . DIRECTORY_SEPARATOR . 'cert' . DIRECTORY_SEPARATOR . '1332187001_20181030_cert.pem',
    // 配置缓存目录，需要拥有写权限
    'cache_path'     => App::getRootPath() . 'runtime/wx_cache',
];