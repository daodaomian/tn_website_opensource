import request from '@/utils/request'

const api_prefix = process.env.VUE_APP_BASE_API_PREFIX

export function getSystemLogList(params) {
  return request({
    url: api_prefix + 'system_log/list',
    method: 'get',
    params
  })
}

export function deleteSystemLog(ids) {
  return request({
    url: api_prefix + 'system_log/delete',
    method: 'delete',
    data: { ids }
  })
}

export function clearSystemLog() {
  return request({
    url: api_prefix + 'system_log/clear_all',
    method: 'delete'
  })
}
