<?php
/**
 * Created by PhpStorm.
 * User: jaylen
 * Date: 2020-11-14
 * Time: 14:06
 */

namespace app\admin\validate;


use app\common\validate\BaseValidate;
use think\File;

class ShopExpressCompany extends BaseValidate
{
    protected $rule = [
        'id' => 'require|number|gt:0',
        'name' => 'require|max:60',
        'code' => 'require|max:60',
        'excel_file' => 'require|checkFile|fileExt:xls,xlsx'
    ];

    protected $message = [
        'id.require' => 'id不能为空',
        'id.number' => 'id必须为整数',
        'id.gt' => 'id必须大于0',
        'name.require' => '快递公司名称不能为空',
        'name.max' => '快递公司名称最大长度为60',
        'code.require' => '快递公司编码不能为空',
        'code.max' => '快递公司编码最大长度为60',
        'excel_file.require' => '待读取的文件不能为空',
        'excel_file.checkFile' => '上传导入信息的文件不符合规格',
        'excel_file.fileExt' => '上传导入信息的文件不符合规格'
    ];

    protected $scene = [
        'add' => ['name','code'],
        'edit' => ['id','name','code'],
        'add_excel' => ['excel_file']
    ];

    /**
     * 文件验证，兼顾多文件
     * @param $value
     * @param $rule
     * @param $data
     * @return bool
     */
    public function checkFile($value, $rule, $data)
    {

        if (is_array($value)) {
            foreach ($value as $item) {
                if (!$item instanceof File) {
                    return false ;
                }
            }
        } else {
            if (!$value instanceof File) {
                return false;
            }
        }

        return true;
    }
}