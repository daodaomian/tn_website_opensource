/*
 Navicat Premium Data Transfer

 Source Server         : 本地数据库
 Source Server Type    : MySQL
 Source Server Version : 50732
 Source Host           : localhost:3306
 Source Schema         : tn_website_open

 Target Server Type    : MySQL
 Target Server Version : 50732
 File Encoding         : 65001

 Date: 06/07/2021 16:00:41
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tn_admin
-- ----------------------------
DROP TABLE IF EXISTS `tn_admin`;
CREATE TABLE `tn_admin`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '管理员主键id',
  `user_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '登陆用户名',
  `password` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '登陆密码',
  `gender` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员性别(0 男 1 女)',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '管理员头像',
  `nick_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '管理员别名',
  `introduction` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '管理员简介',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '管理员开启状态',
  `role_id` int(255) NOT NULL DEFAULT 0 COMMENT '所属角色的id',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存管理员信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_admin
-- ----------------------------
INSERT INTO `tn_admin` VALUES (1, 'super_admin', '$2y$10$sGzeU9wUt01x1TOHyUCqy.hjiR2dry/DsQ5JYZm6J1/ZLWNMO/Fz.', 0, '/storage/uploads/img_list/c6/aab26376f3f6dc3c5a737784e45985.png', '超级管理员', '图鸟超级管理员', 1, 1, 1588213740, 1625498070, NULL);
INSERT INTO `tn_admin` VALUES (2, 'admin', '$2y$10$.LopVHxUhiQ0kAI/Gnj2qOvbhnnVJmYi36ev0eWm97UHrAKCb4C.q', 0, '/storage/uploads/img_list/27/5c2e56b7b0967f2a52ad19fe0bb765.jpg', '图鸟管理员', '普通管理员', 0, 2, 1588215393, 1603850566, NULL);

-- ----------------------------
-- Table structure for tn_admin_we_chat_user
-- ----------------------------
DROP TABLE IF EXISTS `tn_admin_we_chat_user`;
CREATE TABLE `tn_admin_we_chat_user`  (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `we_chat_user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  UNIQUE INDEX `uid_we_chat_id`(`user_id`, `we_chat_user_id`) USING BTREE,
  INDEX `uid`(`user_id`) USING BTREE,
  INDEX `we_chat_id`(`we_chat_user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存管理员与微信用户关联的相关信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_admin_we_chat_user
-- ----------------------------
INSERT INTO `tn_admin_we_chat_user` VALUES (1, 1);
INSERT INTO `tn_admin_we_chat_user` VALUES (1, 2);

-- ----------------------------
-- Table structure for tn_atlas
-- ----------------------------
DROP TABLE IF EXISTS `tn_atlas`;
CREATE TABLE `tn_atlas`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '图集主键id',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图片的名称，对应上传文件的原名称',
  `img_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图片保存的路径',
  `md5` varchar(34) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '记录图片的md5值，用于去重复',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图片的保存类型（1、保存在本地）',
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图片所属分类',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name_md5_category`(`name`, `md5`, `category_id`) USING BTREE,
  INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 97 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存图集的图片信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_atlas
-- ----------------------------
INSERT INTO `tn_atlas` VALUES (1, 'logo.png', '/storage/uploads/img_list/c6/aab26376f3f6dc3c5a737784e45985.png', 'c6aab26376f3f6dc3c5a737784e45985', 1, 1, 1590207902, 1590207902, NULL);
INSERT INTO `tn_atlas` VALUES (3, 'spring.jpg', '/storage/uploads/img_list/28/d181dcfaa45cbbdd51c2b2e4a9379d.jpg', '28d181dcfaa45cbbdd51c2b2e4a9379d', 1, 4, 1590457210, 1590457210, NULL);
INSERT INTO `tn_atlas` VALUES (4, 'summer.jpg', '/storage/uploads/img_list/52/1648532c5e5f133243d3345cce55f8.jpg', '521648532c5e5f133243d3345cce55f8', 1, 4, 1590457829, 1590457829, NULL);
INSERT INTO `tn_atlas` VALUES (5, 'autumn.jpg', '/storage/uploads/img_list/f1/f3adf879a21152d4651985df31a24c.jpg', 'f1f3adf879a21152d4651985df31a24c', 1, 4, 1590931739, 1625554394, NULL);
INSERT INTO `tn_atlas` VALUES (6, '主题2.png', '/storage/uploads/img_list/96/00a3c9fce9978d9e767f2e2f90fc7b.png', '9600a3c9fce9978d9e767f2e2f90fc7b', 1, 4, 1592192843, 1592192843, NULL);
INSERT INTO `tn_atlas` VALUES (7, '主题3.jpg', '/storage/uploads/img_list/96/dbcde963e2a4d86fd460b6e4bf1769.jpg', '96dbcde963e2a4d86fd460b6e4bf1769', 1, 4, 1592192844, 1592192844, NULL);
INSERT INTO `tn_atlas` VALUES (8, '主题4.jpg', '/storage/uploads/img_list/1d/f27f460d2c5c9c0272324a9a9e5718.jpg', '1df27f460d2c5c9c0272324a9a9e5718', 1, 4, 1592192844, 1592192844, NULL);
INSERT INTO `tn_atlas` VALUES (9, '主题1.jpg', '/storage/uploads/img_list/95/a5526255d95418b434964c52cd056d.jpg', '95a5526255d95418b434964c52cd056d', 1, 4, 1592192844, 1592192844, NULL);
INSERT INTO `tn_atlas` VALUES (10, 'logo2 (1).jpg', '/storage/uploads/img_list/0c/2683c1e4d721f44aa466869a4c9d11.jpg', '0c2683c1e4d721f44aa466869a4c9d11', 1, 6, 1592622990, 1592622990, NULL);
INSERT INTO `tn_atlas` VALUES (11, '图鸟-关于我们.png', '/storage/uploads/img_list/b5/ca5616d6da3fa0c6a0e521fef793ec.png', 'b5ca5616d6da3fa0c6a0e521fef793ec', 1, 6, 1592623322, 1592623322, NULL);
INSERT INTO `tn_atlas` VALUES (12, 'my-bg - 副本 - 副本 (1).png', '/storage/uploads/img_list/94/65cb49497cc16e193792cd3e7151f5.png', '9465cb49497cc16e193792cd3e7151f5', 1, 6, 1592624053, 1592624053, NULL);
INSERT INTO `tn_atlas` VALUES (13, '41d46d8d2639ef508dd4fa31380b5846.jpg', '/storage/uploads/img_list/05/604f527c562a6dca8a8fa17fb00a1d.jpg', '05604f527c562a6dca8a8fa17fb00a1d', 1, 5, 1592657352, 1592657352, NULL);
INSERT INTO `tn_atlas` VALUES (14, '022c2dcfb82f3c2ff738244462d15003.jpg', '/storage/uploads/img_list/06/4247b392f77bd697b6bd525b1948c0.jpg', '064247b392f77bd697b6bd525b1948c0', 1, 5, 1592722087, 1592722087, NULL);
INSERT INTO `tn_atlas` VALUES (18, '365b3c8886c958ebd6caea70d3fa0e67.jpg', '/storage/uploads/img_list/2d/2dd5e55e55ebb040ebe7046f7d4a4e.jpg', '2d2dd5e55e55ebb040ebe7046f7d4a4e', 1, 5, 1592879367, 1592879367, NULL);
INSERT INTO `tn_atlas` VALUES (21, 'c7cca5e4f051e1c016996f98ca0d65f9.jpg', '/storage/uploads/img_list/36/a35e4109e6284a351750b7703259f4.jpg', '36a35e4109e6284a351750b7703259f4', 1, 0, 1592881552, 1592881552, NULL);
INSERT INTO `tn_atlas` VALUES (37, '你好图鸟 (2).jpg', '/storage/uploads/img_list/ec/255c43f571a04fd4252a9be0088e00.jpg', 'ec255c43f571a04fd4252a9be0088e00', 1, 5, 1593571489, 1593571489, NULL);
INSERT INTO `tn_atlas` VALUES (44, 'ezgif.gif', '/storage/uploads/img_list/b6/ba46a97a481db496f58b23b44a0b9b.gif', 'b6ba46a97a481db496f58b23b44a0b9b', 1, 5, 1593654253, 1593654253, NULL);
INSERT INTO `tn_atlas` VALUES (45, 'ezgifff.gif', '/storage/uploads/img_list/f6/72537c4c2354e890a88b27a716d099.gif', 'f672537c4c2354e890a88b27a716d099', 1, 5, 1593655077, 1593655077, NULL);
INSERT INTO `tn_atlas` VALUES (51, '2020-07-02_141412.jpg', '/storage/uploads/img_list/b8/1c7709b9e7c5d81d6e6e25840f3adb.jpg', 'b81c7709b9e7c5d81d6e6e25840f3adb', 1, 0, 1593670775, 1593670775, NULL);
INSERT INTO `tn_atlas` VALUES (65, '图鸟-关于我们.png', '/storage/uploads/img_list/5f/c66ad1b84fe9d17c1c7a7666c633db.png', '5fc66ad1b84fe9d17c1c7a7666c633db', 1, 0, 1599741089, 1599741089, NULL);
INSERT INTO `tn_atlas` VALUES (66, '图鸟-关于我们新.png', '/storage/uploads/img_list/5f/c66ad1b84fe9d17c1c7a7666c633db.png', '5fc66ad1b84fe9d17c1c7a7666c633db', 1, 6, 1599741130, 1599741130, NULL);
INSERT INTO `tn_atlas` VALUES (67, '22222.jpg', '/storage/uploads/img_list/31/5cd6608d598ec532070150a9b28673.jpg', '315cd6608d598ec532070150a9b28673', 1, 5, 1600343379, 1600343379, NULL);
INSERT INTO `tn_atlas` VALUES (69, '开源封面.jpg', '/storage/uploads/img_list/1c/bfaa4ffefcf77e857c60c97ca545da.jpg', '1cbfaa4ffefcf77e857c60c97ca545da', 1, 5, 1600572104, 1600572104, NULL);
INSERT INTO `tn_atlas` VALUES (77, '44444 (主图).jpg', '/storage/uploads/img_list/d9/9f567e458e16f3c675c9221cba0619.jpg', 'd99f567e458e16f3c675c9221cba0619', 1, 0, 1601600499, 1601600499, NULL);
INSERT INTO `tn_atlas` VALUES (80, '东东首页3.jpg', '/storage/uploads/img_list/69/73fda93b7c22061965b5ed75b7cac5.jpg', '6973fda93b7c22061965b5ed75b7cac5', 1, 0, 1602660608, 1602660608, NULL);
INSERT INTO `tn_atlas` VALUES (82, 'active.jpg', '/storage/uploads/img_list/07/0bb47670490c8bc069c1a9a2714b7f.jpg', '070bb47670490c8bc069c1a9a2714b7f', 1, 7, 1602813839, 1602813839, NULL);
INSERT INTO `tn_atlas` VALUES (83, '东东首页1.jpg', '/storage/uploads/img_list/84/96ae96629fed2067ce599fae1c8fa9.jpg', '8496ae96629fed2067ce599fae1c8fa9', 1, 0, 1604567590, 1604567590, NULL);
INSERT INTO `tn_atlas` VALUES (84, '主图.jpeg', '/storage/uploads/img_list/1c/bfaa4ffefcf77e857c60c97ca545da.jpeg', '1cbfaa4ffefcf77e857c60c97ca545da', 1, 8, 1607162643, 1607162643, NULL);
INSERT INTO `tn_atlas` VALUES (85, '主图.jpeg', '/storage/uploads/img_list/3f/3102dfdb1d1fa21451fac9b94c3113.jpeg', '3f3102dfdb1d1fa21451fac9b94c3113', 1, 8, 1607163078, 1607163078, NULL);
INSERT INTO `tn_atlas` VALUES (86, '钱钱钱.jpg', '/storage/uploads/img_list/85/0aacbea97b173f4eca6922bc60cace.jpg', '850aacbea97b173f4eca6922bc60cace', 1, 4, 1612786354, 1612786354, NULL);

-- ----------------------------
-- Table structure for tn_atlas_category
-- ----------------------------
DROP TABLE IF EXISTS `tn_atlas_category`;
CREATE TABLE `tn_atlas_category`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '图集类目主键id',
  `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图集类目名称',
  `sort` mediumint(8) UNSIGNED NOT NULL DEFAULT 1 COMMENT '图集排序',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否开启图集',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存图集类目的相关信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_atlas_category
-- ----------------------------
INSERT INTO `tn_atlas_category` VALUES (1, '图鸟官方图标', 1, 1, 1589794899, 1589797112, NULL);
INSERT INTO `tn_atlas_category` VALUES (2, '头像', 2, 1, 1589797169, 1589797169, NULL);
INSERT INTO `tn_atlas_category` VALUES (3, '杂项', 3, 1, 1590125624, 1590125624, NULL);
INSERT INTO `tn_atlas_category` VALUES (4, '轮播图', 4, 1, 1590457188, 1590457188, NULL);
INSERT INTO `tn_atlas_category` VALUES (5, '内容主图', 5, 1, 1592025864, 1592025864, NULL);
INSERT INTO `tn_atlas_category` VALUES (6, '小程序设置', 6, 1, 1592025878, 1592025878, NULL);
INSERT INTO `tn_atlas_category` VALUES (7, '微信小商店', 8, 1, 1602469041, 1603853733, NULL);
INSERT INTO `tn_atlas_category` VALUES (8, '图鸟商店', 7, 1, 1603853728, 1603853728, NULL);

-- ----------------------------
-- Table structure for tn_auth_menu
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_menu`;
CREATE TABLE `tn_auth_menu`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '角色菜单主键id',
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '角色菜单上级id',
  `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色菜单路由名字',
  `title` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色菜单侧边栏喝面包屑展示的名称',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色菜单路由地址',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色菜单文件所在路径（为空则为Layout）',
  `redirect` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色菜单重定向地址',
  `icon` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色菜单的图标',
  `is_link` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '标记角色菜单是否为链接',
  `hidden` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '标记角色菜单是否隐藏不显示在侧边栏（1 隐藏 0 显示）',
  `is_click_in_breadcrumb` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '标记角色菜单是否可以在面包屑中点击',
  `always_show` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '标记角色菜单是否总是显示在侧边栏（1 永远显示 0 不显示）',
  `no_cache` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '标记角色菜单是否被缓存（1 缓存 0 不缓存）',
  `breadcrumb_show` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '标记角色菜单是否显示在面包屑（1 显示在面包屑 0 不显示在面包屑）',
  `public_menu` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '标记角色菜单是否为公共菜单',
  `sort` mediumint(8) UNSIGNED NOT NULL DEFAULT 1 COMMENT '角色菜单排序',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '角色菜单状态',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 51 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存角色菜单的数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_auth_menu
-- ----------------------------
INSERT INTO `tn_auth_menu` VALUES (1, 0, 'Admin', '管理员', '/admin', '', '/admin/admin/list', 'tn_admin', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1588504538, 1588599355, NULL);
INSERT INTO `tn_auth_menu` VALUES (2, 1, 'AdminAdmin', '管理员人员管理', 'admin', 'admin/admin/index', '/admin/admin/list', 'tn_admin_1', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1588560688, 1606825607, NULL);
INSERT INTO `tn_auth_menu` VALUES (3, 2, 'AdminList', '人员列表', 'list', 'admin/admin/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1588562226, 1606798611, NULL);
INSERT INTO `tn_auth_menu` VALUES (4, 1, 'AdminAuth', '管理员权限管理', 'auth', 'admin/auth/index', '', 'tn_admin_auth', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1588570663, 1606802998, NULL);
INSERT INTO `tn_auth_menu` VALUES (5, 4, 'AuthRole', '角色列表', 'role', 'admin/auth/role/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1588570730, 1606798709, NULL);
INSERT INTO `tn_auth_menu` VALUES (6, 4, 'AuthMenu', '菜单列表', 'menu', 'admin/auth/menu/list', '', '', 0, 0, 1, 0, 0, 1, 0, 3, 1, 1588570858, 1606798646, NULL);
INSERT INTO `tn_auth_menu` VALUES (7, 0, 'Tuniao', '图鸟官网', 'https://tuniaokj.com/', '', '', 'international', 1, 0, 1, 0, 0, 1, 1, 20, 1, 1588571120, 1601632194, NULL);
INSERT INTO `tn_auth_menu` VALUES (8, 4, 'AuthRule', '规则列表', 'rule', 'admin/auth/rule/list', '', '', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1588852174, 1606798694, NULL);
INSERT INTO `tn_auth_menu` VALUES (9, 0, 'System', '系统', '/system', '', '', 'xitongshezhi', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1588916619, 1589505965, NULL);
INSERT INTO `tn_auth_menu` VALUES (10, 9, 'SystemConfigList', '配置项列表', 'config-list', 'system-config/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1588916672, 1606798871, NULL);
INSERT INTO `tn_auth_menu` VALUES (11, 9, 'SystemConfigMenu', '系统配置', 'menu', 'system-config/menu', '', '', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1589015089, 1606798881, NULL);
INSERT INTO `tn_auth_menu` VALUES (12, 9, 'SystemLogList', '系统日志', 'log-list', 'system-log/list', '', '', 0, 0, 1, 0, 0, 1, 0, 3, 1, 1589505946, 1606798894, NULL);
INSERT INTO `tn_auth_menu` VALUES (13, 0, 'Atlas', '图集', '/atlas', '', '/atlas/category', 'table', 0, 0, 1, 0, 0, 1, 1, 3, 1, 1589782312, 1589782312, NULL);
INSERT INTO `tn_auth_menu` VALUES (14, 13, 'AltasCategory', '图集分类', 'category', 'atlas/category', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1589783179, 1606798944, NULL);
INSERT INTO `tn_auth_menu` VALUES (15, 13, 'AtlasList', '图集列表', 'list', 'atlas/list', '', '', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1590039557, 1606798952, NULL);
INSERT INTO `tn_auth_menu` VALUES (16, 0, 'WeChat', '微信', '/wechat', '', '', 'wechat', 0, 0, 1, 0, 0, 1, 0, 10, 1, 1590375766, 1591580308, NULL);
INSERT INTO `tn_auth_menu` VALUES (17, 16, 'WeChatConfig', '配置管理', 'config', 'wechat/config/index', '/wechat/config/menu', 'config', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1590376554, 1606816602, NULL);
INSERT INTO `tn_auth_menu` VALUES (18, 17, 'WeChatConfigList', '微信配置项列表', 'list', 'wechat/config/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1590377821, 1606799622, NULL);
INSERT INTO `tn_auth_menu` VALUES (19, 17, 'WeChatConfigMenu', '微信配置', 'menu', 'wechat/config/menu', '', '', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1590377865, 1606799650, NULL);
INSERT INTO `tn_auth_menu` VALUES (20, 0, 'Banner', '轮播', '/banner', '', '/banner/banner-list', 'banner', 0, 0, 1, 0, 0, 1, 0, 4, 1, 1590401099, 1590455972, NULL);
INSERT INTO `tn_auth_menu` VALUES (21, 20, 'BannerPosList', '轮播位管理', 'banner-pos', 'banner/banner-pos/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1590401219, 1606798987, NULL);
INSERT INTO `tn_auth_menu` VALUES (22, 20, 'BannerList', '轮播图管理', 'banner-list', 'banner/banner/list', '', '', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1590455952, 1606798970, NULL);
INSERT INTO `tn_auth_menu` VALUES (23, 16, 'WeChatUserList', '用户管理', 'user', 'wechat/user/list', '', 'peoples', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1590478824, 1606799564, NULL);
INSERT INTO `tn_auth_menu` VALUES (24, 0, 'Category', '栏目', '/category', '', '/category/list', 'category', 0, 0, 1, 0, 0, 1, 0, 5, 1, 1590491178, 1590491178, NULL);
INSERT INTO `tn_auth_menu` VALUES (25, 24, 'CategoryList', '栏目管理', 'list', 'category/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1590491215, 1606799057, NULL);
INSERT INTO `tn_auth_menu` VALUES (26, 0, 'Business', '业务', '/business', '', '/business/list', 'business', 0, 0, 1, 0, 0, 1, 0, 6, 1, 1590560214, 1590560268, NULL);
INSERT INTO `tn_auth_menu` VALUES (27, 26, 'BusinessList', '业务列表', 'list', 'business/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1590560254, 1606799077, NULL);
INSERT INTO `tn_auth_menu` VALUES (28, 0, 'ModelTable', '模型', '/model-table', '', '/model-table/list', 'model', 0, 0, 1, 0, 0, 1, 0, 7, 1, 1590668835, 1590668914, NULL);
INSERT INTO `tn_auth_menu` VALUES (29, 28, 'ModelTableList', '模型列表', 'list', 'model-table/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1590668879, 1606799250, NULL);
INSERT INTO `tn_auth_menu` VALUES (30, 0, 'Content', '内容', '/content', '', '/content/list', 'content', 0, 0, 1, 0, 0, 1, 0, 8, 1, 1590819770, 1590819776, NULL);
INSERT INTO `tn_auth_menu` VALUES (31, 30, 'ContentList', '内容管理', 'list', 'content/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1590819822, 1606799280, NULL);
INSERT INTO `tn_auth_menu` VALUES (32, 0, 'Order', '订单', '/order', '', '/order/list', 'order', 0, 0, 1, 0, 0, 1, 0, 9, 1, 1591580457, 1591580457, NULL);
INSERT INTO `tn_auth_menu` VALUES (33, 32, 'OrderList', '订单详情', 'list', 'order/list', '', '', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1591580608, 1606799526, NULL);
INSERT INTO `tn_auth_menu` VALUES (34, 0, 'WXMPShop', '微信小商店', '/wx-mp-shop', '', '/wx-mp-shop/product', 'shopping', 0, 0, 1, 0, 0, 1, 0, 12, 1, 1601633082, 1625537995, 1625537995);
INSERT INTO `tn_auth_menu` VALUES (35, 34, 'WXMPShopProductList', '商品列表', 'product', 'wx-mp-shop/product/list', '', 'category', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1601633145, 1625537995, 1625537995);
INSERT INTO `tn_auth_menu` VALUES (36, 34, 'WXMPShopOrderList', '订单列表', 'order', 'wx-mp-shop/order/list', '', 'nested', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1601633240, 1625537995, 1625537995);
INSERT INTO `tn_auth_menu` VALUES (37, 38, 'WXMPShopActivitiesAwardList', '奖品内容', 'award', 'wx-mp-shop/activities/award', '', 'award', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1602319435, 1625537995, 1625537995);
INSERT INTO `tn_auth_menu` VALUES (38, 34, 'WXMPShopActivities', '活动管理', 'activities', 'wx-mp-shop/activities/index', '', 'activities', 0, 0, 1, 0, 0, 1, 0, 4, 1, 1602337962, 1625537995, 1625537995);
INSERT INTO `tn_auth_menu` VALUES (39, 38, 'WXMPShopActivitiesList', '活动信息', 'list', 'wx-mp-shop/activities/list', '', 'activities-list', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1602338068, 1625537995, 1625537995);
INSERT INTO `tn_auth_menu` VALUES (40, 38, 'WXMPShopActivitiesReceiveList', '领取信息', 'receive', 'wx-mp-shop/activities/receive', '', 'activities-receive', 0, 0, 1, 0, 0, 1, 0, 3, 1, 1602415579, 1625537995, 1625537995);
INSERT INTO `tn_auth_menu` VALUES (41, 0, 'TnShop', '图鸟商店', '/tn-shop', '', '/tn-shop/product', 'shop', 0, 0, 1, 0, 0, 1, 0, 11, 1, 1603850372, 1603850467, NULL);
INSERT INTO `tn_auth_menu` VALUES (42, 41, 'ShopProductList', '商品列表', 'product', 'shop/product/list', '', 'category', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1603850444, 1606799839, NULL);
INSERT INTO `tn_auth_menu` VALUES (43, 41, 'ShopCategoryList', '分类列表', 'category', 'shop/category/list', '', 'product-category', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1604115353, 1606799820, NULL);
INSERT INTO `tn_auth_menu` VALUES (44, 41, 'ShopExpressCompanyList', '快递公司列表', 'express-company', 'shop/express-company/list', '', 'express-company', 0, 0, 1, 0, 0, 1, 0, 3, 1, 1605340881, 1606799855, NULL);
INSERT INTO `tn_auth_menu` VALUES (45, 41, 'ShopActivities', '活动管理', 'activities', 'shop/activities/index', '/tn-shop/activities/list', 'activities', 0, 0, 1, 0, 0, 1, 0, 4, 1, 1606271832, 1625538000, 1625538000);
INSERT INTO `tn_auth_menu` VALUES (46, 45, 'ShopActivitiesList', '活动信息', 'list', 'shop/activities/list', '', 'activities-list', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1606272345, 1625538000, 1625538000);
INSERT INTO `tn_auth_menu` VALUES (47, 45, 'ShopActivitiesAwardList', '活动奖品', 'award', 'shop/activities/award', '', 'award', 0, 0, 1, 0, 0, 1, 0, 2, 1, 1606272400, 1625538000, 1625538000);
INSERT INTO `tn_auth_menu` VALUES (48, 45, 'ShopActivitiesReceiveList', '领奖信息', 'receive', 'shop/activities/receive', '', 'activities-receive', 0, 0, 1, 0, 0, 1, 0, 3, 1, 1606272472, 1625538000, 1625538000);
INSERT INTO `tn_auth_menu` VALUES (49, 0, 'Lottery', '抽奖', '/lottery', '', '/lottery/list', 'lottery', 0, 0, 1, 0, 0, 1, 0, 12, 1, 1606744027, 1625537989, 1625537989);
INSERT INTO `tn_auth_menu` VALUES (50, 49, 'LotteryList', '抽奖管理', 'list', 'lottery/list', '', 'config', 0, 0, 1, 0, 0, 1, 0, 1, 1, 1606744206, 1625537989, 1625537989);

-- ----------------------------
-- Table structure for tn_auth_role
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_role`;
CREATE TABLE `tn_auth_role`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '权限角色主键id',
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色标题',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '角色状态（1 打开 ；0 关闭）',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `role_title_name`(`title`, `name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存用户权限角色信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_auth_role
-- ----------------------------
INSERT INTO `tn_auth_role` VALUES (1, '超级管理员', 'super_admin', 1, 1586937088, 1625537978, NULL);
INSERT INTO `tn_auth_role` VALUES (2, '管理员', 'admin', 0, 1586937444, 1625538088, NULL);
INSERT INTO `tn_auth_role` VALUES (3, '访客', 'guest', 0, 1588815255, 1594086718, NULL);

-- ----------------------------
-- Table structure for tn_auth_role_half_menu
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_role_half_menu`;
CREATE TABLE `tn_auth_role_half_menu`  (
  `role_id` int(10) UNSIGNED NOT NULL COMMENT '角色id',
  `menu_id` int(11) NOT NULL COMMENT '菜单id'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存菜单半选节点id信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_auth_role_half_menu
-- ----------------------------
INSERT INTO `tn_auth_role_half_menu` VALUES (1, 41);
INSERT INTO `tn_auth_role_half_menu` VALUES (2, 20);

-- ----------------------------
-- Table structure for tn_auth_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_role_menu`;
CREATE TABLE `tn_auth_role_menu`  (
  `role_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '角色id',
  `menu_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '菜单id',
  UNIQUE INDEX `role_menu_id`(`role_id`, `menu_id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  INDEX `menu_id`(`menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存角色与菜单之间的数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_auth_role_menu
-- ----------------------------
INSERT INTO `tn_auth_role_menu` VALUES (1, 1);
INSERT INTO `tn_auth_role_menu` VALUES (1, 2);
INSERT INTO `tn_auth_role_menu` VALUES (1, 3);
INSERT INTO `tn_auth_role_menu` VALUES (1, 4);
INSERT INTO `tn_auth_role_menu` VALUES (1, 5);
INSERT INTO `tn_auth_role_menu` VALUES (1, 6);
INSERT INTO `tn_auth_role_menu` VALUES (1, 7);
INSERT INTO `tn_auth_role_menu` VALUES (1, 8);
INSERT INTO `tn_auth_role_menu` VALUES (1, 9);
INSERT INTO `tn_auth_role_menu` VALUES (1, 10);
INSERT INTO `tn_auth_role_menu` VALUES (1, 11);
INSERT INTO `tn_auth_role_menu` VALUES (1, 12);
INSERT INTO `tn_auth_role_menu` VALUES (1, 13);
INSERT INTO `tn_auth_role_menu` VALUES (1, 14);
INSERT INTO `tn_auth_role_menu` VALUES (1, 15);
INSERT INTO `tn_auth_role_menu` VALUES (1, 16);
INSERT INTO `tn_auth_role_menu` VALUES (1, 17);
INSERT INTO `tn_auth_role_menu` VALUES (1, 18);
INSERT INTO `tn_auth_role_menu` VALUES (1, 19);
INSERT INTO `tn_auth_role_menu` VALUES (1, 20);
INSERT INTO `tn_auth_role_menu` VALUES (1, 21);
INSERT INTO `tn_auth_role_menu` VALUES (1, 22);
INSERT INTO `tn_auth_role_menu` VALUES (1, 23);
INSERT INTO `tn_auth_role_menu` VALUES (1, 24);
INSERT INTO `tn_auth_role_menu` VALUES (1, 25);
INSERT INTO `tn_auth_role_menu` VALUES (1, 26);
INSERT INTO `tn_auth_role_menu` VALUES (1, 27);
INSERT INTO `tn_auth_role_menu` VALUES (1, 28);
INSERT INTO `tn_auth_role_menu` VALUES (1, 29);
INSERT INTO `tn_auth_role_menu` VALUES (1, 30);
INSERT INTO `tn_auth_role_menu` VALUES (1, 31);
INSERT INTO `tn_auth_role_menu` VALUES (1, 32);
INSERT INTO `tn_auth_role_menu` VALUES (1, 33);
INSERT INTO `tn_auth_role_menu` VALUES (1, 41);
INSERT INTO `tn_auth_role_menu` VALUES (1, 42);
INSERT INTO `tn_auth_role_menu` VALUES (1, 43);
INSERT INTO `tn_auth_role_menu` VALUES (1, 44);
INSERT INTO `tn_auth_role_menu` VALUES (2, 1);
INSERT INTO `tn_auth_role_menu` VALUES (2, 2);
INSERT INTO `tn_auth_role_menu` VALUES (2, 3);
INSERT INTO `tn_auth_role_menu` VALUES (2, 4);
INSERT INTO `tn_auth_role_menu` VALUES (2, 5);
INSERT INTO `tn_auth_role_menu` VALUES (2, 6);
INSERT INTO `tn_auth_role_menu` VALUES (2, 7);
INSERT INTO `tn_auth_role_menu` VALUES (2, 8);
INSERT INTO `tn_auth_role_menu` VALUES (2, 13);
INSERT INTO `tn_auth_role_menu` VALUES (2, 14);
INSERT INTO `tn_auth_role_menu` VALUES (2, 15);
INSERT INTO `tn_auth_role_menu` VALUES (2, 20);
INSERT INTO `tn_auth_role_menu` VALUES (2, 22);
INSERT INTO `tn_auth_role_menu` VALUES (3, 1);
INSERT INTO `tn_auth_role_menu` VALUES (3, 2);
INSERT INTO `tn_auth_role_menu` VALUES (3, 3);
INSERT INTO `tn_auth_role_menu` VALUES (3, 4);
INSERT INTO `tn_auth_role_menu` VALUES (3, 5);
INSERT INTO `tn_auth_role_menu` VALUES (3, 6);
INSERT INTO `tn_auth_role_menu` VALUES (3, 7);

-- ----------------------------
-- Table structure for tn_auth_role_rule
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_role_rule`;
CREATE TABLE `tn_auth_role_rule`  (
  `role_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '角色id',
  `rule_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '权限id',
  UNIQUE INDEX `role_rule_id`(`role_id`, `rule_id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  INDEX `rule_id`(`rule_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存权限角色和权限规则之间的关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_auth_role_rule
-- ----------------------------
INSERT INTO `tn_auth_role_rule` VALUES (1, 2);
INSERT INTO `tn_auth_role_rule` VALUES (1, 3);
INSERT INTO `tn_auth_role_rule` VALUES (1, 4);
INSERT INTO `tn_auth_role_rule` VALUES (1, 5);
INSERT INTO `tn_auth_role_rule` VALUES (1, 6);
INSERT INTO `tn_auth_role_rule` VALUES (1, 7);
INSERT INTO `tn_auth_role_rule` VALUES (1, 8);
INSERT INTO `tn_auth_role_rule` VALUES (1, 9);
INSERT INTO `tn_auth_role_rule` VALUES (1, 10);
INSERT INTO `tn_auth_role_rule` VALUES (1, 11);
INSERT INTO `tn_auth_role_rule` VALUES (1, 16);
INSERT INTO `tn_auth_role_rule` VALUES (1, 17);
INSERT INTO `tn_auth_role_rule` VALUES (1, 18);
INSERT INTO `tn_auth_role_rule` VALUES (1, 19);
INSERT INTO `tn_auth_role_rule` VALUES (1, 20);
INSERT INTO `tn_auth_role_rule` VALUES (1, 21);
INSERT INTO `tn_auth_role_rule` VALUES (1, 22);
INSERT INTO `tn_auth_role_rule` VALUES (1, 23);
INSERT INTO `tn_auth_role_rule` VALUES (1, 24);
INSERT INTO `tn_auth_role_rule` VALUES (1, 25);
INSERT INTO `tn_auth_role_rule` VALUES (1, 26);
INSERT INTO `tn_auth_role_rule` VALUES (1, 27);
INSERT INTO `tn_auth_role_rule` VALUES (1, 28);
INSERT INTO `tn_auth_role_rule` VALUES (1, 29);
INSERT INTO `tn_auth_role_rule` VALUES (1, 30);
INSERT INTO `tn_auth_role_rule` VALUES (1, 31);
INSERT INTO `tn_auth_role_rule` VALUES (1, 32);
INSERT INTO `tn_auth_role_rule` VALUES (1, 33);
INSERT INTO `tn_auth_role_rule` VALUES (1, 34);
INSERT INTO `tn_auth_role_rule` VALUES (1, 35);
INSERT INTO `tn_auth_role_rule` VALUES (1, 36);
INSERT INTO `tn_auth_role_rule` VALUES (1, 37);
INSERT INTO `tn_auth_role_rule` VALUES (1, 38);
INSERT INTO `tn_auth_role_rule` VALUES (1, 39);
INSERT INTO `tn_auth_role_rule` VALUES (1, 40);
INSERT INTO `tn_auth_role_rule` VALUES (1, 43);
INSERT INTO `tn_auth_role_rule` VALUES (1, 44);
INSERT INTO `tn_auth_role_rule` VALUES (1, 45);
INSERT INTO `tn_auth_role_rule` VALUES (1, 46);
INSERT INTO `tn_auth_role_rule` VALUES (1, 47);
INSERT INTO `tn_auth_role_rule` VALUES (1, 48);
INSERT INTO `tn_auth_role_rule` VALUES (1, 49);
INSERT INTO `tn_auth_role_rule` VALUES (1, 50);
INSERT INTO `tn_auth_role_rule` VALUES (1, 51);
INSERT INTO `tn_auth_role_rule` VALUES (1, 52);
INSERT INTO `tn_auth_role_rule` VALUES (1, 54);
INSERT INTO `tn_auth_role_rule` VALUES (1, 55);
INSERT INTO `tn_auth_role_rule` VALUES (1, 56);
INSERT INTO `tn_auth_role_rule` VALUES (1, 59);
INSERT INTO `tn_auth_role_rule` VALUES (1, 60);
INSERT INTO `tn_auth_role_rule` VALUES (1, 61);
INSERT INTO `tn_auth_role_rule` VALUES (1, 62);
INSERT INTO `tn_auth_role_rule` VALUES (1, 63);
INSERT INTO `tn_auth_role_rule` VALUES (1, 64);
INSERT INTO `tn_auth_role_rule` VALUES (1, 65);
INSERT INTO `tn_auth_role_rule` VALUES (1, 67);
INSERT INTO `tn_auth_role_rule` VALUES (1, 70);
INSERT INTO `tn_auth_role_rule` VALUES (1, 71);
INSERT INTO `tn_auth_role_rule` VALUES (1, 72);
INSERT INTO `tn_auth_role_rule` VALUES (1, 73);
INSERT INTO `tn_auth_role_rule` VALUES (1, 74);
INSERT INTO `tn_auth_role_rule` VALUES (1, 75);
INSERT INTO `tn_auth_role_rule` VALUES (1, 76);
INSERT INTO `tn_auth_role_rule` VALUES (1, 78);
INSERT INTO `tn_auth_role_rule` VALUES (1, 79);
INSERT INTO `tn_auth_role_rule` VALUES (1, 80);
INSERT INTO `tn_auth_role_rule` VALUES (1, 81);
INSERT INTO `tn_auth_role_rule` VALUES (1, 82);
INSERT INTO `tn_auth_role_rule` VALUES (1, 83);
INSERT INTO `tn_auth_role_rule` VALUES (1, 85);
INSERT INTO `tn_auth_role_rule` VALUES (1, 86);
INSERT INTO `tn_auth_role_rule` VALUES (1, 87);
INSERT INTO `tn_auth_role_rule` VALUES (1, 88);
INSERT INTO `tn_auth_role_rule` VALUES (1, 89);
INSERT INTO `tn_auth_role_rule` VALUES (1, 90);
INSERT INTO `tn_auth_role_rule` VALUES (1, 91);
INSERT INTO `tn_auth_role_rule` VALUES (1, 92);
INSERT INTO `tn_auth_role_rule` VALUES (1, 93);
INSERT INTO `tn_auth_role_rule` VALUES (1, 95);
INSERT INTO `tn_auth_role_rule` VALUES (1, 96);
INSERT INTO `tn_auth_role_rule` VALUES (1, 97);
INSERT INTO `tn_auth_role_rule` VALUES (1, 98);
INSERT INTO `tn_auth_role_rule` VALUES (1, 99);
INSERT INTO `tn_auth_role_rule` VALUES (1, 100);
INSERT INTO `tn_auth_role_rule` VALUES (1, 101);
INSERT INTO `tn_auth_role_rule` VALUES (1, 103);
INSERT INTO `tn_auth_role_rule` VALUES (1, 104);
INSERT INTO `tn_auth_role_rule` VALUES (1, 105);
INSERT INTO `tn_auth_role_rule` VALUES (1, 106);
INSERT INTO `tn_auth_role_rule` VALUES (1, 107);
INSERT INTO `tn_auth_role_rule` VALUES (1, 108);
INSERT INTO `tn_auth_role_rule` VALUES (1, 109);
INSERT INTO `tn_auth_role_rule` VALUES (1, 110);
INSERT INTO `tn_auth_role_rule` VALUES (1, 112);
INSERT INTO `tn_auth_role_rule` VALUES (1, 113);
INSERT INTO `tn_auth_role_rule` VALUES (1, 114);
INSERT INTO `tn_auth_role_rule` VALUES (1, 115);
INSERT INTO `tn_auth_role_rule` VALUES (1, 116);
INSERT INTO `tn_auth_role_rule` VALUES (1, 117);
INSERT INTO `tn_auth_role_rule` VALUES (1, 118);
INSERT INTO `tn_auth_role_rule` VALUES (1, 119);
INSERT INTO `tn_auth_role_rule` VALUES (1, 121);
INSERT INTO `tn_auth_role_rule` VALUES (1, 122);
INSERT INTO `tn_auth_role_rule` VALUES (1, 123);
INSERT INTO `tn_auth_role_rule` VALUES (1, 124);
INSERT INTO `tn_auth_role_rule` VALUES (1, 127);
INSERT INTO `tn_auth_role_rule` VALUES (1, 128);
INSERT INTO `tn_auth_role_rule` VALUES (1, 130);
INSERT INTO `tn_auth_role_rule` VALUES (1, 131);
INSERT INTO `tn_auth_role_rule` VALUES (1, 132);
INSERT INTO `tn_auth_role_rule` VALUES (1, 133);
INSERT INTO `tn_auth_role_rule` VALUES (1, 134);
INSERT INTO `tn_auth_role_rule` VALUES (1, 135);
INSERT INTO `tn_auth_role_rule` VALUES (1, 136);
INSERT INTO `tn_auth_role_rule` VALUES (1, 137);
INSERT INTO `tn_auth_role_rule` VALUES (1, 138);
INSERT INTO `tn_auth_role_rule` VALUES (1, 139);
INSERT INTO `tn_auth_role_rule` VALUES (1, 140);
INSERT INTO `tn_auth_role_rule` VALUES (1, 141);
INSERT INTO `tn_auth_role_rule` VALUES (1, 143);
INSERT INTO `tn_auth_role_rule` VALUES (1, 144);
INSERT INTO `tn_auth_role_rule` VALUES (1, 170);
INSERT INTO `tn_auth_role_rule` VALUES (1, 171);
INSERT INTO `tn_auth_role_rule` VALUES (1, 172);
INSERT INTO `tn_auth_role_rule` VALUES (1, 175);
INSERT INTO `tn_auth_role_rule` VALUES (1, 176);
INSERT INTO `tn_auth_role_rule` VALUES (1, 177);
INSERT INTO `tn_auth_role_rule` VALUES (1, 178);
INSERT INTO `tn_auth_role_rule` VALUES (1, 179);
INSERT INTO `tn_auth_role_rule` VALUES (1, 180);
INSERT INTO `tn_auth_role_rule` VALUES (1, 181);
INSERT INTO `tn_auth_role_rule` VALUES (1, 183);
INSERT INTO `tn_auth_role_rule` VALUES (1, 184);
INSERT INTO `tn_auth_role_rule` VALUES (1, 185);
INSERT INTO `tn_auth_role_rule` VALUES (1, 186);
INSERT INTO `tn_auth_role_rule` VALUES (1, 187);
INSERT INTO `tn_auth_role_rule` VALUES (1, 188);
INSERT INTO `tn_auth_role_rule` VALUES (1, 189);
INSERT INTO `tn_auth_role_rule` VALUES (1, 190);
INSERT INTO `tn_auth_role_rule` VALUES (1, 191);
INSERT INTO `tn_auth_role_rule` VALUES (1, 192);
INSERT INTO `tn_auth_role_rule` VALUES (1, 194);
INSERT INTO `tn_auth_role_rule` VALUES (1, 195);
INSERT INTO `tn_auth_role_rule` VALUES (1, 196);
INSERT INTO `tn_auth_role_rule` VALUES (1, 197);
INSERT INTO `tn_auth_role_rule` VALUES (1, 198);
INSERT INTO `tn_auth_role_rule` VALUES (1, 199);
INSERT INTO `tn_auth_role_rule` VALUES (1, 200);
INSERT INTO `tn_auth_role_rule` VALUES (1, 201);
INSERT INTO `tn_auth_role_rule` VALUES (1, 202);
INSERT INTO `tn_auth_role_rule` VALUES (1, 203);
INSERT INTO `tn_auth_role_rule` VALUES (1, 204);
INSERT INTO `tn_auth_role_rule` VALUES (1, 205);
INSERT INTO `tn_auth_role_rule` VALUES (1, 206);
INSERT INTO `tn_auth_role_rule` VALUES (1, 225);
INSERT INTO `tn_auth_role_rule` VALUES (1, 233);
INSERT INTO `tn_auth_role_rule` VALUES (2, 2);
INSERT INTO `tn_auth_role_rule` VALUES (2, 3);
INSERT INTO `tn_auth_role_rule` VALUES (2, 6);
INSERT INTO `tn_auth_role_rule` VALUES (2, 8);
INSERT INTO `tn_auth_role_rule` VALUES (2, 9);
INSERT INTO `tn_auth_role_rule` VALUES (2, 10);
INSERT INTO `tn_auth_role_rule` VALUES (2, 11);
INSERT INTO `tn_auth_role_rule` VALUES (2, 16);
INSERT INTO `tn_auth_role_rule` VALUES (2, 17);
INSERT INTO `tn_auth_role_rule` VALUES (2, 18);
INSERT INTO `tn_auth_role_rule` VALUES (2, 23);
INSERT INTO `tn_auth_role_rule` VALUES (2, 24);
INSERT INTO `tn_auth_role_rule` VALUES (2, 29);
INSERT INTO `tn_auth_role_rule` VALUES (2, 30);
INSERT INTO `tn_auth_role_rule` VALUES (2, 31);
INSERT INTO `tn_auth_role_rule` VALUES (2, 32);
INSERT INTO `tn_auth_role_rule` VALUES (2, 37);
INSERT INTO `tn_auth_role_rule` VALUES (2, 38);
INSERT INTO `tn_auth_role_rule` VALUES (2, 39);
INSERT INTO `tn_auth_role_rule` VALUES (2, 40);
INSERT INTO `tn_auth_role_rule` VALUES (2, 61);
INSERT INTO `tn_auth_role_rule` VALUES (2, 67);
INSERT INTO `tn_auth_role_rule` VALUES (3, 2);
INSERT INTO `tn_auth_role_rule` VALUES (3, 3);
INSERT INTO `tn_auth_role_rule` VALUES (3, 6);
INSERT INTO `tn_auth_role_rule` VALUES (3, 8);
INSERT INTO `tn_auth_role_rule` VALUES (3, 9);
INSERT INTO `tn_auth_role_rule` VALUES (3, 10);
INSERT INTO `tn_auth_role_rule` VALUES (3, 11);
INSERT INTO `tn_auth_role_rule` VALUES (3, 16);
INSERT INTO `tn_auth_role_rule` VALUES (3, 17);
INSERT INTO `tn_auth_role_rule` VALUES (3, 18);
INSERT INTO `tn_auth_role_rule` VALUES (3, 23);
INSERT INTO `tn_auth_role_rule` VALUES (3, 24);
INSERT INTO `tn_auth_role_rule` VALUES (3, 29);
INSERT INTO `tn_auth_role_rule` VALUES (3, 30);
INSERT INTO `tn_auth_role_rule` VALUES (3, 31);
INSERT INTO `tn_auth_role_rule` VALUES (3, 32);
INSERT INTO `tn_auth_role_rule` VALUES (3, 37);
INSERT INTO `tn_auth_role_rule` VALUES (3, 38);

-- ----------------------------
-- Table structure for tn_auth_role_user
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_role_user`;
CREATE TABLE `tn_auth_role_user`  (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '管理员id',
  `role_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '权限角色id',
  UNIQUE INDEX `uid_role_id`(`user_id`, `role_id`) USING BTREE,
  INDEX `uid`(`user_id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户与权限组之间的关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_auth_role_user
-- ----------------------------
INSERT INTO `tn_auth_role_user` VALUES (1, 1);
INSERT INTO `tn_auth_role_user` VALUES (2, 2);
INSERT INTO `tn_auth_role_user` VALUES (3, 1);
INSERT INTO `tn_auth_role_user` VALUES (4, 2);

-- ----------------------------
-- Table structure for tn_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `tn_auth_rule`;
CREATE TABLE `tn_auth_rule`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '权限规则主键id',
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '权限规则父级id',
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '权限规则标题',
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '权限规则标识',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '规则验证方式，如果为1则可以验证condition中的规则',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '权限规则排序',
  `public_rule` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否为公用权限（1 是 0 否）',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '权限状态（1 开启 0 关闭）',
  `condition` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '多重验证用到',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 240 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_auth_rule
-- ----------------------------
INSERT INTO `tn_auth_rule` VALUES (1, 0, '管理员', 'admin', 1, 1, 0, 1, '', 1588659366, 1588668625, NULL);
INSERT INTO `tn_auth_rule` VALUES (2, 1, '获取管理员列表数据', 'admin/getlist', 1, 1, 0, 1, '', 1588659441, 1588668625, NULL);
INSERT INTO `tn_auth_rule` VALUES (3, 1, '根据id获取管理员信息', 'admin/getbyid', 1, 2, 0, 1, '', 1588659466, 1588668625, NULL);
INSERT INTO `tn_auth_rule` VALUES (4, 1, '添加管理员', 'admin/addadmin', 1, 3, 0, 1, '', 1588667563, 1588668620, NULL);
INSERT INTO `tn_auth_rule` VALUES (5, 1, '编辑管理员', 'admin/editadmin', 1, 4, 0, 1, '', 1588667651, 1588668625, NULL);
INSERT INTO `tn_auth_rule` VALUES (6, 1, '更新管理员信息', 'admin/updateadmin', 1, 5, 0, 1, '', 1588667739, 1603588939, NULL);
INSERT INTO `tn_auth_rule` VALUES (7, 1, '删除指定的管理员信息', 'admin/deleteadmin', 1, 6, 0, 1, '', 1588667833, 1588668625, NULL);
INSERT INTO `tn_auth_rule` VALUES (8, 1, '管理员登陆', 'admin/login', 1, 7, 1, 1, '', 1588668922, 1588734790, NULL);
INSERT INTO `tn_auth_rule` VALUES (9, 1, '管理员退出', 'admin/logout', 1, 8, 1, 1, '', 1588668944, 1588668998, NULL);
INSERT INTO `tn_auth_rule` VALUES (10, 1, '获取对应管理员的路由信息', 'admin/routes', 1, 9, 1, 1, '', 1588669017, 1588669024, NULL);
INSERT INTO `tn_auth_rule` VALUES (11, 1, '获取对应管理员的信息', 'admin/info', 1, 10, 1, 1, '', 1588669163, 1588669166, NULL);
INSERT INTO `tn_auth_rule` VALUES (12, 0, '权限管理', 'auth', 1, 2, 0, 1, '', 1588669462, 1588669462, NULL);
INSERT INTO `tn_auth_rule` VALUES (13, 12, '权限角色', 'authrole', 1, 1, 0, 1, '', 1588669501, 1588669501, NULL);
INSERT INTO `tn_auth_rule` VALUES (14, 12, '权限菜单', 'authmenu', 1, 2, 0, 1, '', 1588669549, 1588669549, NULL);
INSERT INTO `tn_auth_rule` VALUES (15, 12, '权限规则', 'authrule', 1, 3, 0, 1, '', 1588669563, 1588669563, NULL);
INSERT INTO `tn_auth_rule` VALUES (16, 13, '获取权限角色列表数据', 'authrole/getlist', 1, 1, 0, 1, '', 1588669593, 1588669593, NULL);
INSERT INTO `tn_auth_rule` VALUES (17, 13, '获取权限角色的名称数据', 'authrole/getall', 1, 2, 0, 1, '', 1588669618, 1588827803, NULL);
INSERT INTO `tn_auth_rule` VALUES (18, 13, '根据id获取指定的数据', 'authrole/getbyid', 1, 3, 0, 1, '', 1588669704, 1588669704, NULL);
INSERT INTO `tn_auth_rule` VALUES (19, 13, '添加权限角色', 'authrole/addrole', 1, 4, 0, 1, '', 1588669723, 1588669723, NULL);
INSERT INTO `tn_auth_rule` VALUES (20, 13, '编辑权限角色', 'authrole/editrole', 1, 5, 0, 1, '', 1588669941, 1588669941, NULL);
INSERT INTO `tn_auth_rule` VALUES (21, 13, '更新权限角色信息', 'authrole/updaterole', 1, 6, 0, 1, '', 1588677433, 1588677433, NULL);
INSERT INTO `tn_auth_rule` VALUES (22, 13, '删除指定的权限信息', 'authrole/deleterole', 1, 7, 0, 1, '', 1588677457, 1588677457, NULL);
INSERT INTO `tn_auth_rule` VALUES (23, 14, '获取表格树形数据', 'authmenu/gettabletree', 1, 1, 0, 1, '', 1588677512, 1588677512, NULL);
INSERT INTO `tn_auth_rule` VALUES (24, 14, '根据id获取角色权限菜单的信息', 'authmenu/getbyid', 1, 2, 0, 1, '', 1588677542, 1588677542, NULL);
INSERT INTO `tn_auth_rule` VALUES (25, 14, '添加角色权限菜单', 'authmenu/addmenu', 1, 3, 0, 1, '', 1588677561, 1588677561, NULL);
INSERT INTO `tn_auth_rule` VALUES (26, 14, '编辑角色权限菜单', 'authmenu/editmenu', 1, 4, 0, 1, '', 1588677578, 1588677578, NULL);
INSERT INTO `tn_auth_rule` VALUES (27, 14, '更新角色权限菜单信息', 'authmenu/updatemenu', 1, 5, 0, 1, '', 1588677820, 1588677820, NULL);
INSERT INTO `tn_auth_rule` VALUES (28, 14, '删除角色权限菜单信息', 'authmenu/deletemenu', 1, 6, 0, 1, '', 1588677839, 1588677839, NULL);
INSERT INTO `tn_auth_rule` VALUES (29, 14, '获取角色权限菜单的所有节点信息', 'authmenu/getallmenunode', 1, 7, 0, 1, '', 1588677859, 1588677859, NULL);
INSERT INTO `tn_auth_rule` VALUES (30, 14, '获取指定父节点下的子节点数量', 'authmenu/getchildrencount', 1, 8, 0, 1, '', 1588677882, 1588677882, NULL);
INSERT INTO `tn_auth_rule` VALUES (31, 15, '获取表格树形数据', 'authrule/gettabletree', 1, 1, 0, 1, '', 1588678146, 1588678146, NULL);
INSERT INTO `tn_auth_rule` VALUES (32, 15, '根据id获取角色权限规则的信息', 'authrule/getbyid', 1, 2, 0, 1, '', 1588678483, 1588678483, NULL);
INSERT INTO `tn_auth_rule` VALUES (33, 15, '添加角色权限规则', 'authrule/addrule', 1, 3, 0, 1, '', 1588678597, 1588678597, NULL);
INSERT INTO `tn_auth_rule` VALUES (34, 15, '编辑角色权限规则', 'authrule/editrule', 1, 4, 0, 1, '', 1588678709, 1588678709, NULL);
INSERT INTO `tn_auth_rule` VALUES (35, 15, '更新角色权限规则信息', 'authrule/updaterule', 1, 5, 0, 1, '', 1588678730, 1588678730, NULL);
INSERT INTO `tn_auth_rule` VALUES (36, 15, '删除角色权限规则信息', 'authrule/deleterule', 1, 6, 0, 1, '', 1588678884, 1588678884, NULL);
INSERT INTO `tn_auth_rule` VALUES (37, 15, '获取角色权限规则的所有节点信息', 'authrule/getallrulenode', 1, 7, 0, 1, '', 1588678903, 1588678903, NULL);
INSERT INTO `tn_auth_rule` VALUES (38, 15, '获取指定父节点下的子节点数量', 'authrule/getchildrencount', 1, 8, 0, 1, '', 1588678923, 1588734805, NULL);
INSERT INTO `tn_auth_rule` VALUES (39, 14, '获取树形结构数据', 'authmenu/getelementtree', 1, 9, 0, 1, '', 1588852520, 1588852520, NULL);
INSERT INTO `tn_auth_rule` VALUES (40, 15, '获取树形结构数据', 'authrule/getelementtree', 1, 9, 0, 1, '', 1588852541, 1588852541, NULL);
INSERT INTO `tn_auth_rule` VALUES (41, 0, '系统', 'system', 1, 3, 0, 1, '', 1589423745, 1589423783, NULL);
INSERT INTO `tn_auth_rule` VALUES (42, 41, '系统设置', 'systemconfig', 1, 1, 0, 1, '', 1589423773, 1589423792, NULL);
INSERT INTO `tn_auth_rule` VALUES (43, 42, '获取表格树形数据', 'systemconfig/gettabletree', 1, 1, 0, 1, '', 1589423880, 1589423880, NULL);
INSERT INTO `tn_auth_rule` VALUES (44, 42, '根据id获取配置对应的信息', 'systemconfig/getbyid', 1, 2, 0, 1, '', 1589423899, 1589423899, NULL);
INSERT INTO `tn_auth_rule` VALUES (45, 42, '添加系统配置项信息', 'systemconfig/addconfig', 1, 3, 0, 1, '', 1589423922, 1589423922, NULL);
INSERT INTO `tn_auth_rule` VALUES (46, 42, '编辑系统配置项信息', 'systemconfig/editconfig', 1, 4, 0, 1, '', 1589423948, 1589423948, NULL);
INSERT INTO `tn_auth_rule` VALUES (47, 42, '更新系统配置项信息', 'systemconfig/updateconfig', 1, 5, 0, 1, '', 1589423974, 1589423974, NULL);
INSERT INTO `tn_auth_rule` VALUES (48, 42, '删除系统配置项信息', 'systemconfig/deleteconfig', 1, 6, 0, 1, '', 1589424000, 1589424000, NULL);
INSERT INTO `tn_auth_rule` VALUES (49, 42, '更新系统配置信息', 'systemconfig/commitconfigdata', 1, 7, 0, 1, '', 1589424023, 1589424023, NULL);
INSERT INTO `tn_auth_rule` VALUES (50, 42, '获取配置菜单信息', 'systemconfig/getmenudata', 1, 8, 0, 1, '', 1589424044, 1589424044, NULL);
INSERT INTO `tn_auth_rule` VALUES (51, 42, '获取系统配置的所有父节点信息', 'systemconfig/getallconfigparentnode', 1, 9, 0, 1, '', 1589424075, 1589424075, NULL);
INSERT INTO `tn_auth_rule` VALUES (52, 42, '获取指定父节点下的子节点数量', 'systemconfig/getchildrencount', 1, 10, 0, 1, '', 1589424117, 1589424117, NULL);
INSERT INTO `tn_auth_rule` VALUES (53, 41, '系统日志', 'systemlog', 1, 2, 0, 1, '', 1590208320, 1590208347, NULL);
INSERT INTO `tn_auth_rule` VALUES (54, 53, '获取日志列表信息', 'systemlog/getlist', 1, 1, 0, 1, '', 1590208691, 1590208691, NULL);
INSERT INTO `tn_auth_rule` VALUES (55, 53, '删除指定的日志信息', 'systemlog/deletelog', 1, 2, 0, 1, '', 1590208715, 1590208715, NULL);
INSERT INTO `tn_auth_rule` VALUES (56, 53, '清空日志信息', 'systemlog/clearalllog', 1, 3, 0, 1, '', 1590208737, 1590208737, NULL);
INSERT INTO `tn_auth_rule` VALUES (57, 0, '图集', 'Atlas', 1, 4, 0, 1, '', 1590208857, 1590212008, NULL);
INSERT INTO `tn_auth_rule` VALUES (58, 57, '图集栏目管理', 'atlascategory', 1, 1, 0, 1, '', 1590209372, 1590209372, NULL);
INSERT INTO `tn_auth_rule` VALUES (59, 58, '获取图集类目分类的分页数据', 'atlascategory/getlist', 1, 1, 0, 1, '', 1590211662, 1590211662, NULL);
INSERT INTO `tn_auth_rule` VALUES (60, 58, '根据id获取图集类目分类的数据', 'atlascategory/getbyid', 1, 2, 0, 1, '', 1590211816, 1590211816, NULL);
INSERT INTO `tn_auth_rule` VALUES (61, 58, '获取图集类目分类的全部名称信息', 'atlascategory/getallname', 1, 3, 1, 1, '', 1590211855, 1590211855, NULL);
INSERT INTO `tn_auth_rule` VALUES (62, 58, '添加图集类目分类信息', 'atlascategory/addatlascategory', 1, 4, 0, 1, '', 1590211888, 1590211888, NULL);
INSERT INTO `tn_auth_rule` VALUES (63, 58, '编辑图集类目分类信息', 'atlascategory/editatlascategory', 1, 5, 0, 1, '', 1590211919, 1590211919, NULL);
INSERT INTO `tn_auth_rule` VALUES (64, 58, '更新图集类目分类信息', 'atlascategory/updateatlascategory', 1, 6, 0, 1, '', 1590211956, 1590211956, NULL);
INSERT INTO `tn_auth_rule` VALUES (65, 58, '删除图集类目分类信息', 'atlascategory/deleteatalscategory', 1, 7, 0, 1, '', 1590211978, 1590211978, NULL);
INSERT INTO `tn_auth_rule` VALUES (66, 57, '图集管理', 'atlasmananger', 1, 2, 0, 1, '', 1590212040, 1590212040, NULL);
INSERT INTO `tn_auth_rule` VALUES (67, 66, '修改图集中图片的所属栏目', 'atlas/changecategory', 1, 1, 1, 1, '', 1590212181, 1590212181, NULL);
INSERT INTO `tn_auth_rule` VALUES (68, 0, '轮播', 'Banner', 1, 5, 0, 1, '', 1591597262, 1591597262, NULL);
INSERT INTO `tn_auth_rule` VALUES (69, 68, '轮播图', 'bannerimage', 1, 1, 0, 1, '', 1591597474, 1591597496, NULL);
INSERT INTO `tn_auth_rule` VALUES (70, 69, '获取轮播图分页数据', 'banner/getlist', 1, 1, 0, 1, '', 1591597521, 1591597521, NULL);
INSERT INTO `tn_auth_rule` VALUES (71, 69, '根据id获取对应的轮播图信息', 'banner/getbyid', 1, 2, 0, 1, '', 1591597539, 1591597539, NULL);
INSERT INTO `tn_auth_rule` VALUES (72, 69, '获取指定轮播位下的轮播图数量', 'banner/getbannercountbyposid', 1, 3, 0, 1, '', 1591597681, 1591597681, NULL);
INSERT INTO `tn_auth_rule` VALUES (73, 69, '添加轮播图信息', 'banner/addbanner', 1, 4, 0, 1, '', 1591597696, 1591597696, NULL);
INSERT INTO `tn_auth_rule` VALUES (74, 69, '编辑轮播图图信息', 'banner/editbanner', 1, 5, 0, 1, '', 1591598144, 1591598144, NULL);
INSERT INTO `tn_auth_rule` VALUES (75, 69, '更新轮播图信息', 'banner/updatebanner', 1, 6, 0, 1, '', 1591598161, 1591598161, NULL);
INSERT INTO `tn_auth_rule` VALUES (76, 69, '删除轮播图信息', 'banner/deletebanner', 1, 7, 0, 1, '', 1591598179, 1591598179, NULL);
INSERT INTO `tn_auth_rule` VALUES (77, 68, '轮播位', 'bannerpos', 1, 2, 0, 1, '', 1591598198, 1591598198, NULL);
INSERT INTO `tn_auth_rule` VALUES (78, 77, '获取轮播位分页数据', 'bannerpos/getlist', 1, 1, 0, 1, '', 1591598302, 1591598302, NULL);
INSERT INTO `tn_auth_rule` VALUES (79, 77, '根据id获取对应的轮播位信息', 'bannerpos/getbyid', 1, 2, 0, 1, '', 1591598319, 1591598319, NULL);
INSERT INTO `tn_auth_rule` VALUES (80, 77, '获取轮播位全部名称', 'bannerpos/getalltitle', 1, 3, 0, 1, '', 1591598337, 1591621507, NULL);
INSERT INTO `tn_auth_rule` VALUES (81, 77, '添加轮播位信息', 'bannerpos/addbannerpos', 1, 4, 0, 1, '', 1591598358, 1591598397, NULL);
INSERT INTO `tn_auth_rule` VALUES (82, 77, '编辑轮播位位信息', 'bannerpos/editbannerpos', 1, 5, 0, 1, '', 1591598387, 1591598387, NULL);
INSERT INTO `tn_auth_rule` VALUES (83, 77, '删除轮播位信息', 'bannerpos/deletebannerpod', 1, 6, 0, 1, '', 1591598414, 1591598414, NULL);
INSERT INTO `tn_auth_rule` VALUES (84, 0, '栏目', 'Category', 1, 6, 0, 1, '', 1591609105, 1591609105, NULL);
INSERT INTO `tn_auth_rule` VALUES (85, 84, '获取栏目Table树形数据', 'category/gettabletree', 1, 1, 0, 1, '', 1591609752, 1591609752, NULL);
INSERT INTO `tn_auth_rule` VALUES (86, 84, '获取全部顶级节点信息', 'category/gettopnode', 1, 2, 0, 1, '', 1591609777, 1591609777, NULL);
INSERT INTO `tn_auth_rule` VALUES (87, 84, '获取栏目全部栏目', 'category/getallnode', 1, 3, 0, 1, '', 1591609808, 1591621487, NULL);
INSERT INTO `tn_auth_rule` VALUES (88, 84, '获取对应栏目下的子栏目数量', 'category/getchildrencount', 1, 4, 0, 1, '', 1591609840, 1591609840, NULL);
INSERT INTO `tn_auth_rule` VALUES (89, 84, '根据id获取对应栏目信息', 'category/getbyid', 1, 5, 0, 1, '', 1591609909, 1591609909, NULL);
INSERT INTO `tn_auth_rule` VALUES (90, 84, '添加栏目信息', 'category/addcategory', 1, 6, 0, 1, '', 1591609926, 1591609926, NULL);
INSERT INTO `tn_auth_rule` VALUES (91, 84, '编辑栏目信息', 'category/editcategory', 1, 7, 0, 1, '', 1591610370, 1591610370, NULL);
INSERT INTO `tn_auth_rule` VALUES (92, 84, '更新栏目信息', 'category/updatecategory', 1, 8, 0, 1, '', 1591610389, 1591610389, NULL);
INSERT INTO `tn_auth_rule` VALUES (93, 84, '删除栏目信息', 'category/deletecategory', 1, 9, 0, 1, '', 1591610439, 1591610439, NULL);
INSERT INTO `tn_auth_rule` VALUES (94, 0, '业务', 'Business', 1, 7, 0, 1, '', 1591610573, 1591610573, NULL);
INSERT INTO `tn_auth_rule` VALUES (95, 94, '获取业务分页数据', 'business/getlist', 1, 1, 0, 1, '', 1591610723, 1591621344, NULL);
INSERT INTO `tn_auth_rule` VALUES (96, 94, '根据id获取业务信息', 'business/getbyid', 1, 2, 0, 1, '', 1591610800, 1591621336, NULL);
INSERT INTO `tn_auth_rule` VALUES (97, 94, '获取业务的指定用户信息', 'business/getoperationuser', 1, 3, 0, 1, '', 1591610818, 1591610818, NULL);
INSERT INTO `tn_auth_rule` VALUES (98, 94, '添加业务信息', 'business/addbusiness', 1, 4, 0, 1, '', 1591610846, 1591610846, NULL);
INSERT INTO `tn_auth_rule` VALUES (99, 94, '编辑业务信息', 'business/editbusiness', 1, 5, 0, 1, '', 1591620931, 1591620931, NULL);
INSERT INTO `tn_auth_rule` VALUES (100, 94, '更新业务信息', 'business/updatebusiness', 1, 6, 0, 1, '', 1591620946, 1591620946, NULL);
INSERT INTO `tn_auth_rule` VALUES (101, 94, '删除业务信息', 'business/deletebusiness', 1, 7, 0, 1, '', 1591620964, 1591620964, NULL);
INSERT INTO `tn_auth_rule` VALUES (102, 0, '模型', 'ModelTable', 1, 8, 0, 1, '', 1591620991, 1591620991, NULL);
INSERT INTO `tn_auth_rule` VALUES (103, 102, '获取模型分页数据', 'modeltable/getlist', 1, 1, 0, 1, '', 1591621254, 1591621306, NULL);
INSERT INTO `tn_auth_rule` VALUES (104, 102, '根据id获取模型信息数据', 'modeltable/getbyid', 1, 2, 0, 1, '', 1591621287, 1591621287, NULL);
INSERT INTO `tn_auth_rule` VALUES (105, 102, '获取全部模型名称', 'modeltable/getallname', 1, 3, 0, 1, '', 1591621545, 1591621545, NULL);
INSERT INTO `tn_auth_rule` VALUES (106, 102, '获取模型字段数据', 'modeltable/getfieldsdata', 1, 4, 0, 1, '', 1591621635, 1591621635, NULL);
INSERT INTO `tn_auth_rule` VALUES (107, 102, '添加模型信息', 'modeltable/addmodeltable', 1, 5, 0, 1, '', 1591621652, 1591621652, NULL);
INSERT INTO `tn_auth_rule` VALUES (108, 102, '编辑模型信息', 'modeltable/editmodeltable', 1, 6, 0, 1, '', 1591621668, 1591621668, NULL);
INSERT INTO `tn_auth_rule` VALUES (109, 102, '更新模型信息', 'modeltable/updatemodeltable', 1, 7, 0, 1, '', 1591621685, 1591621685, NULL);
INSERT INTO `tn_auth_rule` VALUES (110, 102, '删除指定的模型信息', 'modeltable/deletemodeltable', 1, 8, 0, 1, '', 1591621704, 1591621704, NULL);
INSERT INTO `tn_auth_rule` VALUES (111, 0, '内容', 'Content', 1, 9, 0, 1, '', 1591621785, 1591621785, NULL);
INSERT INTO `tn_auth_rule` VALUES (112, 111, '获取内容的分页数据', 'content/getlist', 1, 1, 0, 1, '', 1591621797, 1591621797, NULL);
INSERT INTO `tn_auth_rule` VALUES (113, 111, '获取内容的指定用户信息', 'content/getoperationuser', 1, 2, 0, 1, '', 1591621822, 1591621822, NULL);
INSERT INTO `tn_auth_rule` VALUES (114, 111, '根据id获取内容信息', 'content/getbyid', 1, 3, 0, 1, '', 1591621836, 1591621836, NULL);
INSERT INTO `tn_auth_rule` VALUES (115, 111, '获取指定栏目下内容的数量', 'content/getcategorychildrencount', 1, 4, 0, 1, '', 1591621870, 1591621870, NULL);
INSERT INTO `tn_auth_rule` VALUES (116, 111, '添加内容信息', 'content/addcontent', 1, 5, 0, 1, '', 1591622078, 1591622078, NULL);
INSERT INTO `tn_auth_rule` VALUES (117, 111, '编辑内容信息', 'content/editcontent', 1, 6, 0, 1, '', 1591622093, 1591622093, NULL);
INSERT INTO `tn_auth_rule` VALUES (118, 111, '更新内容信息', 'content/updatecontent', 1, 7, 0, 1, '', 1591622108, 1591622108, NULL);
INSERT INTO `tn_auth_rule` VALUES (119, 111, '删除指定的内容信息', 'content/deletecontent', 1, 8, 0, 1, '', 1591622124, 1591622124, NULL);
INSERT INTO `tn_auth_rule` VALUES (120, 0, '订单', 'Order', 1, 10, 0, 1, '', 1591622278, 1591622278, NULL);
INSERT INTO `tn_auth_rule` VALUES (121, 120, '获取订单分页数据信息', 'order/getlist', 1, 1, 0, 1, '', 1591622314, 1591622314, NULL);
INSERT INTO `tn_auth_rule` VALUES (122, 120, '根据id获取订单信息', 'order/getbyid', 1, 2, 0, 1, '', 1591622329, 1591622329, NULL);
INSERT INTO `tn_auth_rule` VALUES (123, 120, '发起订单退款信息', 'order/refundorder', 1, 3, 0, 1, '', 1591622346, 1591622346, NULL);
INSERT INTO `tn_auth_rule` VALUES (124, 120, '删除订单信息', 'order/deleteorder', 1, 4, 0, 1, '', 1591622359, 1591622359, NULL);
INSERT INTO `tn_auth_rule` VALUES (125, 0, '微信', 'WeChat', 1, 11, 0, 1, '', 1594084469, 1594084469, NULL);
INSERT INTO `tn_auth_rule` VALUES (126, 125, '用户管理', 'wechatuser', 1, 1, 0, 1, '', 1594086101, 1594086101, NULL);
INSERT INTO `tn_auth_rule` VALUES (127, 126, '获取授权微信用户的分页数据', 'wechatuser/getlist', 1, 1, 0, 1, '', 1594086138, 1594086138, NULL);
INSERT INTO `tn_auth_rule` VALUES (128, 126, '更新授权微信用户的相关信息', 'wechatuser/updatewechatuser', 1, 2, 0, 1, '', 1594086206, 1594086206, NULL);
INSERT INTO `tn_auth_rule` VALUES (129, 125, '微信设置', 'wechatconfig', 1, 2, 0, 1, '', 1594086314, 1594086314, NULL);
INSERT INTO `tn_auth_rule` VALUES (130, 129, '获取表格树形数据', 'wxconfig/gettabletree', 1, 1, 0, 1, '', 1594086329, 1594086329, NULL);
INSERT INTO `tn_auth_rule` VALUES (131, 129, '根据id获取配置对应的信息', 'wxconfig/getbyid', 1, 2, 0, 1, '', 1594086356, 1594086356, NULL);
INSERT INTO `tn_auth_rule` VALUES (132, 129, '获取配置菜单信息', 'wxconfig/getmenudata', 1, 3, 0, 1, '', 1594086393, 1594086393, NULL);
INSERT INTO `tn_auth_rule` VALUES (133, 129, '获取微信配置的所有父节点信息', 'wxconfig/getallconfigparentnode', 1, 4, 0, 1, '', 1594086431, 1594086431, NULL);
INSERT INTO `tn_auth_rule` VALUES (134, 129, '获取指定父节点下的子节点数量', 'wxconfig/getchildrencount', 1, 5, 0, 1, '', 1594086564, 1594086564, NULL);
INSERT INTO `tn_auth_rule` VALUES (135, 129, '添加微信配置项信息', 'wxconfig/addconfig', 1, 6, 0, 1, '', 1594086581, 1594086581, NULL);
INSERT INTO `tn_auth_rule` VALUES (136, 129, '编辑微信配置项信息', 'wxconfig/editconfig', 1, 7, 0, 1, '', 1594086597, 1594086597, NULL);
INSERT INTO `tn_auth_rule` VALUES (137, 129, '更新微信配置项信息', 'wxconfig/updateconfig', 1, 8, 0, 1, '', 1594086614, 1594086614, NULL);
INSERT INTO `tn_auth_rule` VALUES (138, 129, '删除微信配置项信息', 'wxconfig/deleteconfig', 1, 9, 0, 1, '', 1594086629, 1594086629, NULL);
INSERT INTO `tn_auth_rule` VALUES (139, 129, '更新微信配置信息', 'wxconfig/commitconfigdata', 1, 10, 0, 1, '', 1594086648, 1594086648, NULL);
INSERT INTO `tn_auth_rule` VALUES (140, 66, '上传图集列表的图片', 'atlas/uploadimage', 1, 2, 0, 1, '', 1595639973, 1595640020, NULL);
INSERT INTO `tn_auth_rule` VALUES (141, 66, '删除图集列表中的图片', 'atlas/deleteimage', 1, 3, 0, 1, '', 1595640010, 1595640010, NULL);
INSERT INTO `tn_auth_rule` VALUES (142, 41, '文件上传', 'uploadfile', 1, 3, 0, 1, '', 1595640540, 1595640552, NULL);
INSERT INTO `tn_auth_rule` VALUES (143, 142, '上传文件到本地', 'upload/uploadfile', 1, 1, 0, 1, '', 1595640570, 1595640570, NULL);
INSERT INTO `tn_auth_rule` VALUES (144, 142, '删除服务器指定的文件', 'upload/deleteuploadtempfile', 1, 2, 0, 1, '', 1595640610, 1595640610, NULL);
INSERT INTO `tn_auth_rule` VALUES (145, 0, '微信小商店', 'WXMPShop', 1, 12, 0, 1, '', 1601904272, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (146, 145, '商品管理', 'wxmpproduct', 1, 1, 0, 1, '', 1601905628, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (147, 146, '获取微信小商店商品列表', 'wxmpshop/getproductlist', 1, 1, 0, 1, '', 1601905703, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (148, 146, '更新微信小商店商品列表', 'wxmpshop/updateproductlist', 1, 3, 0, 1, '', 1601905735, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (149, 145, '订单管理', 'wxmporder', 1, 2, 0, 1, '', 1601973935, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (150, 149, '获取微信小商店订单列表', 'wxmpshop/getorderlist', 1, 1, 0, 1, '', 1601974111, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (151, 149, '更新微信小商店订单列表', 'wxmpshop/updateorderlist', 1, 2, 0, 1, '', 1601974151, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (152, 145, '活动奖品内容管理', 'wxmpactivitiesaward', 1, 4, 0, 1, '', 1602318493, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (153, 152, '获取奖品内容列表', 'wxmpshopactivitiesaward/getlist', 1, 1, 0, 1, '', 1602319056, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (154, 152, '根据id获取奖品内容', 'wxmpshopactivitiesaward/getbyid', 1, 2, 0, 1, '', 1602319087, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (155, 152, '添加奖品内容', 'wxmpshopactivitiesaward/addactivitiesaward', 1, 3, 0, 1, '', 1602319122, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (156, 152, '编辑奖品内容', 'wxmpshopactivitiesaward/editactivitiesaward', 1, 4, 0, 1, '', 1602319141, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (157, 152, '删除奖品内容', 'wxmpshopactivitiesaward/deleteactivitiesaward', 1, 5, 0, 1, '', 1602319162, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (158, 145, '订单活动管理', 'wxmpshopactivities', 1, 3, 0, 1, '', 1602337400, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (159, 158, '获取微信小商店活动列表', 'wxmpshopactivities/getlist', 1, 1, 0, 1, '', 1602337459, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (160, 158, '根据id获取微信小商店订单活动信息', 'wxmpshopactivities/getbyid', 1, 2, 0, 1, '', 1602337503, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (161, 158, '获取微信小程序商店的全部标题', 'wxmpshopactivities/getalltitle', 1, 3, 0, 1, '', 1602337534, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (162, 158, '添加微信小商店订单活动信息', 'wxmpshopactivities/addactivities', 1, 4, 0, 1, '', 1602337560, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (163, 158, '编辑微信小商店订单活动信息', 'wxmpshopactivities/editactivities', 1, 6, 0, 1, '', 1602337600, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (164, 158, '删除微信小商店订单活动信息', 'wxmpshopactivities/deleteactivities', 1, 7, 0, 1, '', 1602337645, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (165, 158, '更新微信小商店订单活动信息', 'wxmpshopactivities/updateactivities', 1, 5, 0, 1, '', 1602392240, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (166, 145, '订单活动管理', 'wxmporderactivities', 1, 5, 0, 1, '', 1602403267, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (167, 166, '获取订单活动领取信息列表', 'wxmpshoporderactivities/getlist', 1, 1, 0, 1, '', 1602403308, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (168, 146, '获取微信小商店商品的全部名称', 'wxmpshop/getallproducttitle', 1, 2, 0, 1, '', 1602416810, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (169, 152, '通过Excel导入奖品内容', 'wxmpshopactivitiesaward/addactivitiesawardwithexcel', 1, 6, 0, 1, '', 1602652612, 1625538033, 1625538033);
INSERT INTO `tn_auth_rule` VALUES (170, 1, '添加指定微信用户管理员', 'admin/addwxuseradmin', 1, 11, 0, 1, '', 1603437225, 1603437225, NULL);
INSERT INTO `tn_auth_rule` VALUES (171, 1, '获取微信管理员列表数据', 'admin/wechatadminuserlist', 1, 12, 0, 1, '', 1603606423, 1603606423, NULL);
INSERT INTO `tn_auth_rule` VALUES (172, 1, '删除微信管理员', 'admin/deletewechatadmin', 1, 13, 0, 1, '', 1603616135, 1603616135, NULL);
INSERT INTO `tn_auth_rule` VALUES (173, 0, '图鸟商店', 'TNShop', 1, 12, 0, 1, '', 1603979213, 1606272646, NULL);
INSERT INTO `tn_auth_rule` VALUES (174, 173, '商品管理', 'tnshopproduct', 1, 2, 0, 1, '', 1603979264, 1604111931, NULL);
INSERT INTO `tn_auth_rule` VALUES (175, 174, '获取商店商品列表数据', 'shopproduct/getlist', 1, 1, 0, 1, '', 1603979573, 1603979573, NULL);
INSERT INTO `tn_auth_rule` VALUES (176, 174, '根据id获取商品信息', 'shopproduct/getbyid', 1, 2, 0, 1, '', 1603979606, 1603979606, NULL);
INSERT INTO `tn_auth_rule` VALUES (177, 174, '添加商品数据', 'shopproduct/addproduct', 1, 3, 0, 1, '', 1603979630, 1603979665, NULL);
INSERT INTO `tn_auth_rule` VALUES (178, 174, '编辑商品数据', 'shopproduct/editproduct', 1, 4, 0, 1, '', 1603979653, 1603979653, NULL);
INSERT INTO `tn_auth_rule` VALUES (179, 174, '更新商品信息', 'shopproduct/updateproduct', 1, 5, 0, 1, '', 1603979690, 1603979690, NULL);
INSERT INTO `tn_auth_rule` VALUES (180, 174, '删除商品信息', 'shopproduct/deleteproduct', 1, 6, 0, 1, '', 1603979723, 1603979723, NULL);
INSERT INTO `tn_auth_rule` VALUES (181, 174, '根据指定分类获取商品的数量', 'shopproduct/getcategoryproductcount', 1, 7, 0, 1, '', 1603979781, 1603979781, NULL);
INSERT INTO `tn_auth_rule` VALUES (182, 173, '分类管理', 'tnshopcategory', 1, 1, 0, 1, '', 1604111962, 1604111962, NULL);
INSERT INTO `tn_auth_rule` VALUES (183, 182, '获取商店分类列表数据', 'shopcategory/getlist', 1, 1, 0, 1, '', 1604112031, 1604112031, NULL);
INSERT INTO `tn_auth_rule` VALUES (184, 182, '根据id获取分类信息', 'shopcategory/getbyid', 1, 2, 0, 1, '', 1604112218, 1604112218, NULL);
INSERT INTO `tn_auth_rule` VALUES (185, 182, '添加分类数据', 'shopcategory/addcategory', 1, 3, 0, 1, '', 1604112247, 1604112247, NULL);
INSERT INTO `tn_auth_rule` VALUES (186, 182, '编辑分类数句', 'shopcategory/editcategory', 1, 4, 0, 1, '', 1604112314, 1604112314, NULL);
INSERT INTO `tn_auth_rule` VALUES (187, 182, '更新分类信息', 'shopcategory/updatecategory', 1, 5, 0, 1, '', 1604112344, 1604112344, NULL);
INSERT INTO `tn_auth_rule` VALUES (188, 182, '删除分类信息', 'shopcategory/deletecategory', 1, 6, 0, 1, '', 1604112552, 1604112552, NULL);
INSERT INTO `tn_auth_rule` VALUES (189, 182, '获取数据的数量', 'shopcategory/getallcount', 1, 7, 0, 1, '', 1604112785, 1604112785, NULL);
INSERT INTO `tn_auth_rule` VALUES (190, 182, '获取分类全部标题', 'shopcategory/getalltitle', 1, 8, 0, 1, '', 1604126371, 1604126371, NULL);
INSERT INTO `tn_auth_rule` VALUES (191, 174, '获取全部的商品数量', 'shopproduct/getallcount', 1, 8, 0, 1, '', 1604136743, 1604136743, NULL);
INSERT INTO `tn_auth_rule` VALUES (192, 174, '获取商品的全部标题信息', 'shopproduct/getallproducttitle', 1, 9, 0, 1, '', 1604575465, 1604575465, NULL);
INSERT INTO `tn_auth_rule` VALUES (193, 173, '快递公司管理', 'tnshopexpresscompany', 1, 3, 0, 1, '', 1605339464, 1605339464, NULL);
INSERT INTO `tn_auth_rule` VALUES (194, 193, '获取商店快递公司列表数据', 'shopexpresscompany/getlist', 1, 1, 0, 1, '', 1605339607, 1605339607, NULL);
INSERT INTO `tn_auth_rule` VALUES (195, 193, '根据id获取商店快递公司信息', 'shopexpresscompany/getbyid', 1, 2, 0, 1, '', 1605339641, 1605339641, NULL);
INSERT INTO `tn_auth_rule` VALUES (196, 193, '添加商店快递公司数据', 'shopexpresscompany/addexpresscompany', 1, 3, 0, 1, '', 1605339752, 1605339752, NULL);
INSERT INTO `tn_auth_rule` VALUES (197, 193, '编辑商店快递公司数据', 'shopexpresscompany/editexpresscompany', 1, 4, 0, 1, '', 1605339774, 1605339774, NULL);
INSERT INTO `tn_auth_rule` VALUES (198, 193, '更新商店快递公司信息', 'shopexpresscompany/updateexpresscompany', 1, 5, 0, 1, '', 1605339813, 1605339813, NULL);
INSERT INTO `tn_auth_rule` VALUES (199, 193, '删除商店快递公司信息', 'shopexpresscompany/deleteexpresscompany', 1, 6, 0, 1, '', 1605340364, 1605340364, NULL);
INSERT INTO `tn_auth_rule` VALUES (200, 193, '获取商店快递公司全部名称', 'shopexpresscompany/getallexpresscompanyname', 1, 7, 0, 1, '', 1605340412, 1605340412, NULL);
INSERT INTO `tn_auth_rule` VALUES (201, 193, '通过Excel表导入商店快递公司信息', 'shopexpresscompany/addexpresscompanybyexcel', 1, 8, 0, 1, '', 1605361877, 1605361877, NULL);
INSERT INTO `tn_auth_rule` VALUES (202, 120, '对商店订单进行发货', 'order/deliveryshoporder', 1, 5, 0, 1, '', 1605942818, 1605942874, NULL);
INSERT INTO `tn_auth_rule` VALUES (203, 120, '刷新商店订单的物流信息', 'order/refreshshoporderexpress', 1, 7, 0, 1, '', 1605942856, 1606475173, NULL);
INSERT INTO `tn_auth_rule` VALUES (204, 120, '获取全部订单excel数据', 'order/getallorderexceldata', 1, 8, 0, 1, '', 1606117577, 1606475174, NULL);
INSERT INTO `tn_auth_rule` VALUES (205, 120, '获取待发货订单Excel数据', 'order/getdeliveryorderexceldata', 1, 9, 0, 1, '', 1606187921, 1606475177, NULL);
INSERT INTO `tn_auth_rule` VALUES (206, 174, '获取商品的全部规格数据', 'shopproduct/getAllProductSpecs', 1, 10, 0, 1, '', 1606272742, 1606272742, NULL);
INSERT INTO `tn_auth_rule` VALUES (207, 173, '活动管理', 'TNShopActivities', 1, 4, 0, 1, '', 1606272774, 1625538105, 1625538105);
INSERT INTO `tn_auth_rule` VALUES (208, 207, '活动信息管理', 'TNShopActivitiesInfo', 1, 1, 0, 1, '', 1606272870, 1625538105, 1625538105);
INSERT INTO `tn_auth_rule` VALUES (209, 208, '获取图鸟商店活动列表', 'shopactivities/getlist', 1, 1, 0, 1, '', 1606272923, 1625538105, 1625538105);
INSERT INTO `tn_auth_rule` VALUES (210, 208, '根据id获取图鸟商店订单活动信息', 'shopactivities/getbyid', 1, 2, 0, 1, '', 1606272950, 1625538105, 1625538105);
INSERT INTO `tn_auth_rule` VALUES (211, 208, '获取图鸟商店活动的全部标题', 'shopactivities/getalltitle', 1, 3, 0, 1, '', 1606273014, 1625538105, 1625538105);
INSERT INTO `tn_auth_rule` VALUES (212, 208, '添加图鸟商店订单活动信息', 'shopactivities/addactivities', 1, 4, 0, 1, '', 1606273046, 1625538105, 1625538105);
INSERT INTO `tn_auth_rule` VALUES (213, 208, '更新图鸟商店订单活动信息', 'shopactivities/updateactivities', 1, 5, 0, 1, '', 1606273066, 1625538105, 1625538105);
INSERT INTO `tn_auth_rule` VALUES (214, 208, '编辑图鸟商店订单活动信息', 'shopactivities/editactivities', 1, 6, 0, 1, '', 1606273086, 1625538105, 1625538105);
INSERT INTO `tn_auth_rule` VALUES (215, 208, '删除图鸟商店订单活动信息', 'shopactivities/deleteactivities', 1, 7, 0, 1, '', 1606273102, 1625538105, 1625538105);
INSERT INTO `tn_auth_rule` VALUES (216, 207, '活动奖品内容管理', 'TNShopActivitiesAward', 1, 2, 0, 1, '', 1606273142, 1625538105, 1625538105);
INSERT INTO `tn_auth_rule` VALUES (217, 216, '获取奖品内容列表', 'shopactivitiesaward/getlist', 1, 1, 0, 1, '', 1606273164, 1625538105, 1625538105);
INSERT INTO `tn_auth_rule` VALUES (218, 216, '根据id获取奖品内容', 'shopactivitiesaward/getbyid', 1, 2, 0, 1, '', 1606273181, 1625538105, 1625538105);
INSERT INTO `tn_auth_rule` VALUES (219, 216, '添加奖品内容', 'shopactivitiesaward/addactivitiesaward', 1, 3, 0, 1, '', 1606273198, 1625538105, 1625538105);
INSERT INTO `tn_auth_rule` VALUES (220, 216, '编辑奖品内容', 'shopactivitiesaward/editactivitiesaward', 1, 4, 0, 1, '', 1606273215, 1625538105, 1625538105);
INSERT INTO `tn_auth_rule` VALUES (221, 216, '删除奖品内容', 'shopactivitiesaward/deleteactivitiesaward', 1, 5, 0, 1, '', 1606273234, 1625538105, 1625538105);
INSERT INTO `tn_auth_rule` VALUES (222, 216, '通过Excel导入奖品内容', 'shopactivitiesaward/addactivitiesawardwithexcel', 1, 6, 0, 1, '', 1606273252, 1625538105, 1625538105);
INSERT INTO `tn_auth_rule` VALUES (223, 207, '活动订单管理', 'TNShopActivitiesOrder', 1, 3, 0, 1, '', 1606273281, 1625538105, 1625538105);
INSERT INTO `tn_auth_rule` VALUES (224, 223, '获取订单活动领取信息列表', 'shoporderactivities/getlist', 1, 1, 0, 1, '', 1606273298, 1625538105, 1625538105);
INSERT INTO `tn_auth_rule` VALUES (225, 120, '对虚拟商品订单进行服务', 'order/servevirtualproductorder', 1, 6, 0, 1, '', 1606475159, 1606475179, NULL);
INSERT INTO `tn_auth_rule` VALUES (226, 0, '抽奖管理', 'Lottery', 1, 14, 0, 1, '', 1606743781, 1625538070, 1625538070);
INSERT INTO `tn_auth_rule` VALUES (227, 226, '获取抽奖活动分页数据', 'lottery/getlist', 1, 1, 0, 1, '', 1606743815, 1625538070, 1625538070);
INSERT INTO `tn_auth_rule` VALUES (228, 226, '根据id获取抽奖信息', 'lottery/getbyid', 1, 2, 0, 1, '', 1606743845, 1625538070, 1625538070);
INSERT INTO `tn_auth_rule` VALUES (229, 226, '添加抽奖信息', 'lottery/addlottery', 1, 3, 0, 1, '', 1606743869, 1625538070, 1625538070);
INSERT INTO `tn_auth_rule` VALUES (230, 226, '编辑抽奖信息', 'lottery/editlottery', 1, 4, 0, 1, '', 1606743888, 1625538070, 1625538070);
INSERT INTO `tn_auth_rule` VALUES (231, 226, '更新抽奖信息', 'lottery/updatelottery', 1, 5, 0, 1, '', 1606743918, 1625538070, 1625538070);
INSERT INTO `tn_auth_rule` VALUES (232, 226, '删除抽奖信息', 'lottery/deletelottery', 1, 6, 0, 1, '', 1606743936, 1625538070, 1625538070);
INSERT INTO `tn_auth_rule` VALUES (233, 126, '根据用户名和openid获取用户信息', 'wechatuser/finduserwithnicknameoropenid', 1, 3, 0, 1, '', 1606876870, 1606876870, NULL);
INSERT INTO `tn_auth_rule` VALUES (234, 226, '根据id获取抽奖奖品信息', 'lottery/getprizebylotteryid', 1, 7, 0, 1, '', 1606877436, 1625538070, 1625538070);
INSERT INTO `tn_auth_rule` VALUES (235, 226, '根据id获取抽奖默认用户信息', 'lottery/getdefaultuserbylotteryid', 1, 8, 0, 1, '', 1606877471, 1625538070, 1625538070);
INSERT INTO `tn_auth_rule` VALUES (236, 226, '检查用户对应抽奖情况信息', 'lottery/checkuseriswinning', 1, 9, 0, 1, '', 1606877494, 1625538070, 1625538070);
INSERT INTO `tn_auth_rule` VALUES (237, 226, '添加抽奖默认用户信息', 'lottery/adddefaultuser', 1, 10, 0, 1, '', 1606877520, 1625538070, 1625538070);
INSERT INTO `tn_auth_rule` VALUES (238, 226, '根据id获取抽奖已参加用户信息', 'lottery/getwinninguserbylotteryid', 1, 11, 0, 1, '', 1606913625, 1625538070, 1625538070);
INSERT INTO `tn_auth_rule` VALUES (239, 226, '删除抽奖活动已抽奖用户信息', 'lottery/deletewinninguser', 1, 12, 0, 1, '', 1606913649, 1625538070, 1625538070);

-- ----------------------------
-- Table structure for tn_banner
-- ----------------------------
DROP TABLE IF EXISTS `tn_banner`;
CREATE TABLE `tn_banner`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '轮播图主键id',
  `title` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '轮播图的标题',
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '轮播图图片地址',
  `pos_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '轮播图所属轮播位',
  `article_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '轮播图所属文章id',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '排序',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态（1显示 0不显示）',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存轮播图信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_banner
-- ----------------------------
INSERT INTO `tn_banner` VALUES (1, '首页轮播图1', '/storage/uploads/img_list/95/a5526255d95418b434964c52cd056d.jpg', 1, 0, 1, 1, 1590457764, 1614177054, NULL);
INSERT INTO `tn_banner` VALUES (2, '首页轮播图2', '/storage/uploads/img_list/96/00a3c9fce9978d9e767f2e2f90fc7b.png', 1, 0, 2, 1, 1590457844, 1592192876, NULL);
INSERT INTO `tn_banner` VALUES (3, '首页轮播图3', '/storage/uploads/img_list/96/dbcde963e2a4d86fd460b6e4bf1769.jpg', 1, 0, 3, 1, 1592192926, 1592192926, NULL);
INSERT INTO `tn_banner` VALUES (4, '首页轮播图4', '/storage/uploads/img_list/1d/f27f460d2c5c9c0272324a9a9e5718.jpg', 1, 0, 4, 1, 1592192956, 1592192956, NULL);

-- ----------------------------
-- Table structure for tn_banner_pos
-- ----------------------------
DROP TABLE IF EXISTS `tn_banner_pos`;
CREATE TABLE `tn_banner_pos`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '轮播位的主键id',
  `title` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '轮播位的名称',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存轮播图位置的相关信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_banner_pos
-- ----------------------------
INSERT INTO `tn_banner_pos` VALUES (1, '首页顶部', 1590401346, 1590401588, NULL);
INSERT INTO `tn_banner_pos` VALUES (2, '文章底部', 1590401583, 1590401592, NULL);

-- ----------------------------
-- Table structure for tn_business
-- ----------------------------
DROP TABLE IF EXISTS `tn_business`;
CREATE TABLE `tn_business`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '业务主键id',
  `title` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '业务标题',
  `query_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '咨询次数',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '排序',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存业务信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_business
-- ----------------------------
INSERT INTO `tn_business` VALUES (1, 'UI设计', 226, 1, 1, 1578622937, 1624335965, NULL);
INSERT INTO `tn_business` VALUES (2, '微信小程序', 328, 2, 1, 1578623191, 1624601173, NULL);
INSERT INTO `tn_business` VALUES (3, '网站开发', 67, 3, 1, 1578623257, 1624423218, NULL);
INSERT INTO `tn_business` VALUES (4, '其他业务', 163, 4, 1, 1578623291, 1623824878, NULL);

-- ----------------------------
-- Table structure for tn_business_advisory_user
-- ----------------------------
DROP TABLE IF EXISTS `tn_business_advisory_user`;
CREATE TABLE `tn_business_advisory_user`  (
  `business_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  INDEX `business_id`(`business_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `business_user_id`(`business_id`, `user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存业务与咨询用户之间的关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_business_advisory_user
-- ----------------------------

-- ----------------------------
-- Table structure for tn_business_data
-- ----------------------------
DROP TABLE IF EXISTS `tn_business_data`;
CREATE TABLE `tn_business_data`  (
  `title_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '对应业务流程标题id',
  `title` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '内容主标题',
  `sub_title` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '内容次标题',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '内容'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存业务流程下面的信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_business_data
-- ----------------------------
INSERT INTO `tn_business_data` VALUES (112, '没有原型', '原型设计', '没有原型，也就是没有业务流程图，是画不了UI图的，请先提供完整业务原型，或前往沟通洽谈，原型付费设计100+每页（不谈价）');
INSERT INTO `tn_business_data` VALUES (112, '有原型', '沟通交流', '请提供表述完善的原型图，且需确定风格，主题颜色，配色要求，如难以表述，请提供相关案例图');
INSERT INTO `tn_business_data` VALUES (113, '设计时长', '确认时长', '设计时间周期我们会根据需求给予相关的设计时间，例如，业务流程原型图设计完善，且页面数量在20个内，设计时间为7个工作日（参考值）');
INSERT INTO `tn_business_data` VALUES (113, '已确认相关需求', '评估报价', '根据提供的UI设计需求，双方沟通讨论，并对UI设计需求的合理性和可行性分析，确定最终的UI设计需求，包括图标设计，UI设计200—500每页');
INSERT INTO `tn_business_data` VALUES (114, '微信支付', '定金金额', '设计需求确认后，客户需支付最终报价费用的50%作为定金，并提供邮箱');
INSERT INTO `tn_business_data` VALUES (115, 'UI设计', '首页稿确认', '收到定金后，根据最终的设计需求，首先进行首页的效果图设计，且将首页UI图发送客户初步确认');
INSERT INTO `tn_business_data` VALUES (116, '设计稿交付', '终稿确认', '终稿提供PSD格式，以iphonex为例，客户的前端工程师可将设计稿进行切图');
INSERT INTO `tn_business_data` VALUES (117, '微信支付', '尾款金额', '支付剩余定制设计费用（尾款），设计稿文件将发送邮箱');
INSERT INTO `tn_business_data` VALUES (118, '注意事项', '注意事项', '设计不支持额外的新增的功能的设计，且不支持风格的更改与重新设计，望须知');
INSERT INTO `tn_business_data` VALUES (124, '开发业务', '开发业务', '不属于以上三种业务之一，开发类型的业务，需求表达清晰，有相应预算可接');
INSERT INTO `tn_business_data` VALUES (125, '设计业务', '设计业务', '如图标设计，H5设计业务等');
INSERT INTO `tn_business_data` VALUES (126, '网站类型', '网站类型', '网站分企业官网、品牌网站、营销网站、电商网站、门户网站等等，我们主要开发企业官网，官网建设主要展示企业形象、企业产品或业务、新闻资讯、公司联系信息等。 企业网站建设的普遍意义是企业对外宣传交流的窗口');
INSERT INTO `tn_business_data` VALUES (126, '网站需求', '需求细节', '需要确定开发静态官网还是全套，如果是全套，可以上 http://www.tuniaoweb.com/anlizhanshi.html  ，图鸟web提供196套案例模板，例如【妙珮网站官网】就是开发案例之一');
INSERT INTO `tn_business_data` VALUES (126, '服务器', '服务器', '如果你没有服务器，服务器可自行上阿里云或腾讯云购买，更或者我们协助你们购买（因为云服务器购买需要实名认证，而服务器我们只是替你们配置好并存放项目，也就是服务器是你们的，如果有需求，可代管理。\n阿里云建议使用支付宝账号进行注册然后绑定手机号，并且完成实名认证）');
INSERT INTO `tn_business_data` VALUES (127, '定制官网', '定制官网', '如果是定制官网，需要提供风格，以及相关主色要求，同时提供相关内容资料，我们一般采用自适应开发，也就是一套网站，可以适应所有屏幕的显示。');
INSERT INTO `tn_business_data` VALUES (128, '相关功能点', '评估报价', '根据功能点和相关实现难点，给予评估报价，实现包括“原型流程，UI设计，后台开发，前端开发，项目上线”，如果能提供原型流程图，那么是最好的');
INSERT INTO `tn_business_data` VALUES (128, '开发时长', '确定时长', '开发时长往往取决于官网设计类型和相关功能点，一般官网开发上线时长在1个月内');
INSERT INTO `tn_business_data` VALUES (129, '支付定金', '定金金额', '项目开始着手，客户需支付最终报价费用的40%作为定金，并以46形式收取（定金40%，确认无误60%），可开发票');
INSERT INTO `tn_business_data` VALUES (130, '注意事项', '注意事项', '如果需要额外增加功能，那么需要再次评估并额外支付新功能款项');
INSERT INTO `tn_business_data` VALUES (131, '小程序类型', '确定类型', '首先确定需要设计开发的小程序类型，例如工具类、内容资讯类、生活服务类、游戏娱乐类、电商类');
INSERT INTO `tn_business_data` VALUES (131, '资质', '确定资质', '如果是个人资质，那么限制是很多的，有个体工商户营业执照，或者企业资质是最好的，同时有已认证好的公众号能够直接复用资质给小程序账号');
INSERT INTO `tn_business_data` VALUES (131, '服务器', '服务器', '如果你没有服务器，服务器可自行上阿里云或腾讯云购买，更或者我们协助你们购买（因为云服务器购买需要实名认证，而服务器我们只是替你们配置好并存放项目，也就是服务器是你们的，如果有需求，可代管理。\n阿里云建议使用支付宝账号进行注册然后绑定手机号，并且完成实名认证）');
INSERT INTO `tn_business_data` VALUES (132, '相关功能点', '评估报价', '根据功能点和相关实现难点，给予评估报价，实现包括“原型流程，UI设计，后台开发，前端开发，项目上线”，如果能提供原型流程图，那么是最好的');
INSERT INTO `tn_business_data` VALUES (132, '开发时长', '确定时长', '开发时长往往取决于小程序类型和相关功能点，一般小程序开发上线时长在2-3个月');
INSERT INTO `tn_business_data` VALUES (133, '定金支付', '定金金额', '小程序项目开始着手，客户需支付最终报价费用的50%作为定金，并以541形式收取（定金50%，上线40%，确认无误10%）');
INSERT INTO `tn_business_data` VALUES (133, '对公账户', '可开票', '如需开票，需提前说明，可开图鸟电子发票');
INSERT INTO `tn_business_data` VALUES (134, '小程序上线', '小程序', '项目正常上线');
INSERT INTO `tn_business_data` VALUES (135, '注意事项', '注意事项', '如果需要额外增加功能，那么需要再次评估并额外支付新功能款项');

-- ----------------------------
-- Table structure for tn_business_title
-- ----------------------------
DROP TABLE IF EXISTS `tn_business_title`;
CREATE TABLE `tn_business_title`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '业务流程标题主键id',
  `title` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '流程标题',
  `business_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '对应的业务id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 136 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存业务流程的标题' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_business_title
-- ----------------------------
INSERT INTO `tn_business_title` VALUES (90, '达到放大2', 6);
INSERT INTO `tn_business_title` VALUES (91, 'UI恶化就开始恢复到健康对方副科级地方1', 6);
INSERT INTO `tn_business_title` VALUES (92, '独家开发计划将4', 6);
INSERT INTO `tn_business_title` VALUES (112, '需求沟通', 1);
INSERT INTO `tn_business_title` VALUES (113, '需求评估', 1);
INSERT INTO `tn_business_title` VALUES (114, '支付定金', 1);
INSERT INTO `tn_business_title` VALUES (115, '相关设计', 1);
INSERT INTO `tn_business_title` VALUES (116, '终稿确认', 1);
INSERT INTO `tn_business_title` VALUES (117, '支付余款', 1);
INSERT INTO `tn_business_title` VALUES (118, '注意事项', 1);
INSERT INTO `tn_business_title` VALUES (124, '开发业务', 4);
INSERT INTO `tn_business_title` VALUES (125, '设计业务', 4);
INSERT INTO `tn_business_title` VALUES (126, '需求沟通', 3);
INSERT INTO `tn_business_title` VALUES (127, '风格相关', 3);
INSERT INTO `tn_business_title` VALUES (128, '需求评估', 3);
INSERT INTO `tn_business_title` VALUES (129, '付款模式', 3);
INSERT INTO `tn_business_title` VALUES (130, '注意事项', 3);
INSERT INTO `tn_business_title` VALUES (131, '需求沟通', 2);
INSERT INTO `tn_business_title` VALUES (132, '需求评估', 2);
INSERT INTO `tn_business_title` VALUES (133, '付款模式', 2);
INSERT INTO `tn_business_title` VALUES (134, '项目上线', 2);
INSERT INTO `tn_business_title` VALUES (135, '注意事项', 2);

-- ----------------------------
-- Table structure for tn_category
-- ----------------------------
DROP TABLE IF EXISTS `tn_category`;
CREATE TABLE `tn_category`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '栏目主键id',
  `pid` int(10) UNSIGNED NOT NULL COMMENT '上级栏目id',
  `title` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '栏目标题',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '排序',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '栏目状态（1开启 0关闭）',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存栏目信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_category
-- ----------------------------
INSERT INTO `tn_category` VALUES (1, 0, '案例', 1, 1, 1577858261, 1590491275, NULL);
INSERT INTO `tn_category` VALUES (2, 0, '资讯', 2, 1, 1577858282, 1577858282, NULL);
INSERT INTO `tn_category` VALUES (3, 1, '小程序', 1, 1, 1577858312, 1590492159, NULL);
INSERT INTO `tn_category` VALUES (4, 1, '网站', 2, 1, 1577858324, 1577858324, NULL);
INSERT INTO `tn_category` VALUES (5, 1, '电商', 4, 1, 1577858339, 1583118738, NULL);
INSERT INTO `tn_category` VALUES (6, 1, 'UI设计', 3, 1, 1577858349, 1583118747, NULL);
INSERT INTO `tn_category` VALUES (7, 2, 'UI设计', 1, 1, 1577858377, 1577858377, NULL);
INSERT INTO `tn_category` VALUES (8, 2, '版本迭代', 2, 1, 1578619054, 1600159752, NULL);
INSERT INTO `tn_category` VALUES (9, 1, '其他', 5, 1, 1592881061, 1592881061, NULL);
INSERT INTO `tn_category` VALUES (10, 2, 'icon', 3, 1, 1592881395, 1592881395, NULL);
INSERT INTO `tn_category` VALUES (11, 2, '小程序', 4, 1, 1592885410, 1592885410, NULL);
INSERT INTO `tn_category` VALUES (12, 2, '开源', 5, 1, 1600159737, 1600159737, NULL);
INSERT INTO `tn_category` VALUES (13, 2, '创意', 6, 1, 1614596952, 1614596952, NULL);
INSERT INTO `tn_category` VALUES (14, 2, '模型', 7, 1, 1615817753, 1615817753, NULL);
INSERT INTO `tn_category` VALUES (15, 2, '工具', 8, 1, 1618019326, 1618019326, NULL);

-- ----------------------------
-- Table structure for tn_content
-- ----------------------------
DROP TABLE IF EXISTS `tn_content`;
CREATE TABLE `tn_content`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '内容主键id',
  `title` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '内容对应的标题',
  `main_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '内容的主图',
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '栏目id',
  `model_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '所使用模型的id',
  `table_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '对应模型表存放内容的id',
  `product_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '内容对应的商店商品id',
  `recomm` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否为推荐内容（1推荐 0不推荐）',
  `view_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '浏览次数',
  `like_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '点赞次数',
  `share_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '分享次数',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '排序',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '是否为可读（1可读 0不可读）',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `category_id`(`category_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 71 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存文章内容信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_content
-- ----------------------------
INSERT INTO `tn_content` VALUES (1, '简历王者', '/storage/uploads/img_list/05/604f527c562a6dca8a8fa17fb00a1d.jpg', 3, 1, 1, 0, 0, 681, 15, 27, 2, 1, 1591185426, 1624521475, NULL);
INSERT INTO `tn_content` VALUES (3, '鼠年大吉封面设计', '/storage/uploads/img_list/06/4247b392f77bd697b6bd525b1948c0.jpg', 6, 1, 2, 3, 1, 1314, 75, 30, 1, 1, 1592722093, 1624525208, NULL);
INSERT INTO `tn_content` VALUES (5, '语雀素材地址资源', '/storage/uploads/img_list/84/96ae96629fed2067ce599fae1c8fa9.jpg', 7, 2, 2, 0, 1, 806, 17, 32, 1, 1, 1592724656, 1624599615, NULL);
INSERT INTO `tn_content` VALUES (6, '图鸟社区', '/storage/uploads/img_list/2d/2dd5e55e55ebb040ebe7046f7d4a4e.jpg', 4, 1, 4, 0, 0, 210, 4, 18, 1, 1, 1592879425, 1625552786, NULL);
INSERT INTO `tn_content` VALUES (25, '你好七月，你好图鸟', '/storage/uploads/img_list/ec/255c43f571a04fd4252a9be0088e00.jpg', 9, 1, 21, 0, 0, 117, 3, 25, 4, 1, 1593571581, 1624762536, NULL);
INSERT INTO `tn_content` VALUES (30, '个人二维码设计', '/storage/uploads/img_list/f6/72537c4c2354e890a88b27a716d099.gif', 9, 1, 26, 0, 1, 357, 8, 29, 20, 1, 1593608411, 1624593101, NULL);
INSERT INTO `tn_content` VALUES (48, '图鸟V3.0项目开源辣', '/storage/uploads/img_list/1c/bfaa4ffefcf77e857c60c97ca545da.jpg', 12, 2, 5, 0, 0, 427, 11, 23, 1, 0, 1600160050, 1614656638, NULL);
INSERT INTO `tn_content` VALUES (49, '开源项目上线大集合', '/storage/uploads/img_list/96/00a3c9fce9978d9e767f2e2f90fc7b.png', 11, 2, 6, 0, 1, 9, 1, 3, 2, 0, 1600160552, 1600575602, NULL);
INSERT INTO `tn_content` VALUES (51, '图鸟V3.0项目开源辣', '/storage/uploads/img_list/1c/bfaa4ffefcf77e857c60c97ca545da.jpg', 3, 2, 7, 1, 1, 2376, 50, 29, 1, 1, 1600572000, 1624721954, NULL);

-- ----------------------------
-- Table structure for tn_content_like_user
-- ----------------------------
DROP TABLE IF EXISTS `tn_content_like_user`;
CREATE TABLE `tn_content_like_user`  (
  `content_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  UNIQUE INDEX `content_user_id`(`content_id`, `user_id`) USING BTREE,
  INDEX `content_id`(`content_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存内容点赞数与用户之间的关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_content_like_user
-- ----------------------------

-- ----------------------------
-- Table structure for tn_content_share_user
-- ----------------------------
DROP TABLE IF EXISTS `tn_content_share_user`;
CREATE TABLE `tn_content_share_user`  (
  `content_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  UNIQUE INDEX `content_user_id`(`content_id`, `user_id`) USING BTREE,
  INDEX `content_id`(`content_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存内容分享数与用户之间的关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_content_share_user
-- ----------------------------

-- ----------------------------
-- Table structure for tn_content_view_user
-- ----------------------------
DROP TABLE IF EXISTS `tn_content_view_user`;
CREATE TABLE `tn_content_view_user`  (
  `content_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  UNIQUE INDEX `content_user_id`(`content_id`, `user_id`) USING BTREE,
  INDEX `content_id`(`content_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存内容查看数与用户之间的关系' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_content_view_user
-- ----------------------------

-- ----------------------------
-- Table structure for tn_model_fields
-- ----------------------------
DROP TABLE IF EXISTS `tn_model_fields`;
CREATE TABLE `tn_model_fields`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '模型字段主键id',
  `cn_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '模型字段中文名',
  `en_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '模型字段英文名',
  `values` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '模型字段的可选值',
  `tips` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '模型字段的提示信息',
  `type` smallint(5) UNSIGNED NOT NULL DEFAULT 0 COMMENT '模型字段的类型',
  `model_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '模型字段所属模型',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '排序',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `model_id`(`model_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存模型对应字段信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_model_fields
-- ----------------------------
INSERT INTO `tn_model_fields` VALUES (1, '关键词', 'keywords', '', '请输入关键词', 5, 1, 1);
INSERT INTO `tn_model_fields` VALUES (2, '描述', 'desc', '', '请输入描述', 5, 1, 2);
INSERT INTO `tn_model_fields` VALUES (3, '内容', 'content', '', '', 6, 1, 3);
INSERT INTO `tn_model_fields` VALUES (4, '关键词', 'keywords', '', '请输入关键词', 5, 2, 1);
INSERT INTO `tn_model_fields` VALUES (5, '描述', 'desc', '', '请输入描述', 5, 2, 2);
INSERT INTO `tn_model_fields` VALUES (6, '内容', 'content', '', '', 6, 2, 3);

-- ----------------------------
-- Table structure for tn_model_table
-- ----------------------------
DROP TABLE IF EXISTS `tn_model_table`;
CREATE TABLE `tn_model_table`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '模型主键id',
  `cn_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '模型中文名称',
  `en_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '模型英文名称',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '状态 （1 开启 0 关闭）',
  `create_time` int(11) NULL DEFAULT NULL,
  `update_time` int(11) NULL DEFAULT NULL,
  `delete_time` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `enname`(`en_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存模型与模型表之间的信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_model_table
-- ----------------------------
INSERT INTO `tn_model_table` VALUES (1, '案例模型', 'website_case', 1, 1591068338, 1591068338, NULL);
INSERT INTO `tn_model_table` VALUES (2, '资讯模型', 'website_information', 1, 1591068415, 1591068415, NULL);

-- ----------------------------
-- Table structure for tn_order
-- ----------------------------
DROP TABLE IF EXISTS `tn_order`;
CREATE TABLE `tn_order`  (
  `id` bigint(16) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '订单主键id',
  `title` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单标题',
  `img_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单的主图',
  `address_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单收货地址id',
  `freight` decimal(8, 2) UNSIGNED NOT NULL COMMENT '订单运费',
  `notes` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单备注',
  `user_id` int(10) UNSIGNED NOT NULL COMMENT '用户id',
  `order_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单编号',
  `out_order_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '退款订单编号',
  `prepay_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '预支付码',
  `transaction_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '微信支付订单号',
  `amount` double(10, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '订单金额（元）',
  `user_application_refund_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户申请退款的原因',
  `refund_amount` double(10, 2) UNSIGNED NOT NULL DEFAULT 0.00 COMMENT '退款金额',
  `refund_desc` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单退款原因',
  `refund_order_status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户发起退款时订单的状态',
  `allow_submit_success_subscribe` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '是否允许推送下单成功的通知',
  `allow_pay_timeout_subscribe` tinyint(1) UNSIGNED NULL DEFAULT 0 COMMENT '是否允许推动订单支付超时通知',
  `allow_pay_subscribe` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否允许推送支付成功消息',
  `allow_delivery_subscribe` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否允许推送发货通知',
  `allow_refund_subscribe` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否允许推送退款通知',
  `type` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单类型（1赞赏）',
  `status` tinyint(2) UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单状态 （1已提交 2 已付款 3付款失败 4结束订单 5发起退款 6退款成功 7退款异常 8退款关闭 9已发货）',
  `pay_time` int(11) UNSIGNED NULL DEFAULT NULL COMMENT '订单支付时间',
  `delivery_time` int(11) UNSIGNED NULL DEFAULT NULL COMMENT '订单发货时间',
  `order_end_time` int(11) UNSIGNED NULL DEFAULT NULL COMMENT '订单结束时间',
  `refund_create_time` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '创建退款的时间',
  `user_application_refund_time` int(11) UNSIGNED NULL DEFAULT NULL COMMENT '用户申请退款时间',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `order_no`(`order_no`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `prepay_id`(`prepay_id`) USING BTREE,
  INDEX `out_order_no`(`out_order_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 236 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存订单信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_order
-- ----------------------------

-- ----------------------------
-- Table structure for tn_order_address
-- ----------------------------
DROP TABLE IF EXISTS `tn_order_address`;
CREATE TABLE `tn_order_address`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '订单地址主键id',
  `user_id` bigint(20) NULL DEFAULT NULL COMMENT '滴滴地址所属用户id',
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单地址收货人姓名',
  `tel_number` varchar(13) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '订单地址收货人联系电话',
  `province_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单地址第一级地址',
  `city_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单地址第二级地址',
  `county_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单地址第三级地址',
  `detail_info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单地址详细收货信息',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存订单地址信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_order_address
-- ----------------------------

-- ----------------------------
-- Table structure for tn_order_express
-- ----------------------------
DROP TABLE IF EXISTS `tn_order_express`;
CREATE TABLE `tn_order_express`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '订单快递信息主键id',
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '快递信息对应的订单id',
  `express_company_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0 COMMENT '快递信息对应的快递公司id',
  `express_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '快递订单号',
  `logistics_information` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '快递物流信息',
  `express_status` tinyint(2) NOT NULL DEFAULT -1 COMMENT '快递物流的状态',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `express_no`(`express_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存订单快递物流信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_order_express
-- ----------------------------

-- ----------------------------
-- Table structure for tn_order_product
-- ----------------------------
DROP TABLE IF EXISTS `tn_order_product`;
CREATE TABLE `tn_order_product`  (
  `order_id` bigint(16) UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单id',
  `specs_id` bigint(16) UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单商品规格id',
  `product_id` bigint(16) UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单商品对应的id',
  `virtual_product` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单商品是否为虚拟商品',
  `specs_data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '订单商品规格数据',
  `product_price` decimal(8, 2) UNSIGNED NOT NULL COMMENT '订单商品价格',
  `product_count` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '订单商品数量',
  `product_title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单商品标题',
  `product_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '订单商品主图',
  INDEX `order_id`(`order_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存订单与商品之间的数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_order_product
-- ----------------------------

-- ----------------------------
-- Table structure for tn_shop_category
-- ----------------------------
DROP TABLE IF EXISTS `tn_shop_category`;
CREATE TABLE `tn_shop_category`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '图鸟商店商品分类主键id',
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '图鸟商店商品分类标题',
  `sort` int(10) UNSIGNED NULL DEFAULT 1 COMMENT '图鸟商店商品分类排序',
  `status` tinyint(1) UNSIGNED NULL DEFAULT 1 COMMENT '图鸟商店商品分类状态',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存图鸟商店商品分类信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_shop_category
-- ----------------------------
INSERT INTO `tn_shop_category` VALUES (1, '设计', 1, 1, 1604121094, 1604121267, NULL);
INSERT INTO `tn_shop_category` VALUES (2, '开发', 2, 1, 1604121255, 1604121272, NULL);

-- ----------------------------
-- Table structure for tn_shop_express_company
-- ----------------------------
DROP TABLE IF EXISTS `tn_shop_express_company`;
CREATE TABLE `tn_shop_express_company`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '图鸟商店快递公司主键id',
  `name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图鸟商店快递公司名称',
  `code` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图鸟商店快递公司编码',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `code`(`code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1120 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存图鸟商店快递公司信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_shop_express_company
-- ----------------------------
INSERT INTO `tn_shop_express_company` VALUES (1, '韵达快递', 'yunda', 1605345623, 1605345636, NULL);
INSERT INTO `tn_shop_express_company` VALUES (2, '圆通速递', 'yuantong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (3, '顺丰速运', 'shunfeng', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (4, '中通快递', 'zhongtong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (5, '邮政快递包裹', 'youzhengguonei', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (6, '百世快递', 'huitongkuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (7, '申通快递', 'shentong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (8, '京东物流', 'jd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (9, 'EMS', 'ems', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (10, '天天快递', 'tiantian', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (11, '极兔速递', 'jtexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (12, '邮政标准快递', 'youzhengbk', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (13, '苏宁物流', 'suning', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (14, '德邦', 'debangwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (15, '德邦快递', 'debangkuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (16, '宅急送', 'zhaijisong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (17, '众邮快递', 'zhongyouex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (18, '优速快递', 'youshuwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (19, '百世快运', 'baishiwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (20, '圆通快运', 'yuantongkuaiyun', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (21, '韵达快运', 'yundakuaiyun', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (22, '中通快运', 'zhongtongkuaiyun', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (23, '安能快运', 'annengwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (24, '中通国际', 'zhongtongguoji', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (25, '国际包裹', 'youzhengguoji', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (26, 'DHL-全球件', 'dhlen', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (27, '跨越速运', 'kuayue', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (28, '安得物流', 'annto', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (29, 'FedEx-国际件', 'fedex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (30, '特急送', 'lntjs', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (31, '丹鸟', 'danniao', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (32, 'UPS-全球件', 'upsen', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (33, '壹米滴答', 'yimidida', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (34, 'DHL-中国件', 'dhl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (35, '同城快寄', 'shpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (36, 'EWE全球快递', 'ewe', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (37, '日日顺物流', 'rrs', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (38, '速尔快递', 'suer', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (39, 'USPS', 'usps', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (40, '联昊通', 'lianhaowuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (41, 'EMS-国际件', 'emsguoji', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (42, '芝麻开门', 'zhimakaimen', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (43, 'UPS', 'ups', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (44, '新顺丰（NSF）', 'nsf', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (45, '中铁快运', 'ztky', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (46, '优邦速运', 'ubonex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (47, '微特派', 'weitepai', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (48, '龙邦速递', 'longbanwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (49, '国通快递', 'guotongkuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (50, '澳大利亚(Australia Post)', 'auspost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (51, '顺心捷达', 'sxjdfreight', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (52, '美快国际物流', 'meiquick', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (53, '递四方', 'disifang', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (54, '快捷速递', 'kuaijiesudi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (55, '九曳供应链', 'jiuyescm', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (56, '联合快递', 'gslhkd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (57, '澳邮中国快运', 'auexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (58, '信丰物流', 'xinfengwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (59, '圆通国际', 'yuantongguoji', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (60, '澳洲飞跃物流', 'rlgaus', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (61, 'D速快递', 'dsukuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (62, '宇鑫物流', 'yuxinwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (63, '京广速递', 'jinguangsudikuaijian', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (64, '日本（Japan Post）', 'japanposten', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (65, '顺丰快运', 'shunfengkuaiyun', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (66, '运通速运', 'yuntong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (67, 'Xlobo贝海国际', 'xlobo', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (68, 'Austa国际速递', 'austa', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (69, '中邮物流', 'zhongyouwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (70, '邦泰快运', 'btexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (71, '速必达', 'subida', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (72, '转运四方', 'zhuanyunsifang', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (73, '大马鹿', 'idamalu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (74, '中邮速递', 'wondersyd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (75, '申通国际', 'stosolution', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (76, '联邦快递', 'lianbangkuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (77, '海信物流', 'savor', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (78, '转运中国', 'uszcn', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (79, 'TNT', 'tnt', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (80, 'YUN TRACK', 'yuntrack', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (81, '程光快递', 'flyway', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (82, '速达通', 'sdto', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (83, '安能快递', 'ane66', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (84, '如风达', 'rufengda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (85, '全峰快递', 'quanfengkuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (86, '卓志速运', 'chinaicip', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (87, '天马迅达', 'tianma', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (88, '鑫正一快递', 'zhengyikuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (89, '泛捷国际速递', 'epanex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (90, '中远e环球', 'cosco', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (91, 'Fedex-国际件-中文', 'fedexcn', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (92, '天地华宇', 'tiandihuayu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (93, '汇森速运', 'huisenky', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (94, '金岸物流', 'jinan', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (95, '增益速递', 'zengyisudi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (96, '安迅物流', 'anxl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (97, '加运美', 'jiayunmeiwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (98, 'EMS-国际件-英文', 'emsinten', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (99, 'TNT-全球件', 'tnten', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (100, '方舟速递', 'arkexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (101, 'Superb Grace', 'superb', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (102, '新西兰（New Zealand Post）', 'newzealand', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (103, 'DPD', 'dpd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (104, '富腾达国际货运', 'ftd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (105, '鼎润物流', 'la911', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (106, 'Aramex', 'aramex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (107, '广东邮政', 'guangdongyouzhengwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (108, '疯狂快递', 'crazyexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (109, '比利时（Bpost）', 'bpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (110, '中环快递', 'zhonghuan', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (111, '斑马物流', 'banma', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (112, 'DHL-德国件（DHL Deutschland）', 'dhlde', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (113, '平安达腾飞', 'pingandatengfei', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (114, '广东速腾物流', 'suteng', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (115, '燕文物流', 'yw56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (116, '嘉里大通', 'jialidatong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (117, '联合速运', 'unitedex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (118, '盛辉物流', 'shenghuiwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (119, '速通物流', 'sut56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (120, '极地快递', 'polarexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (121, '新杰物流', 'sunjex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (122, '环球速运', 'huanqiu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (123, '中铁飞豹', 'zhongtiewuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (124, '盛丰物流', 'sfwl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (125, '一速递', 'oneexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (126, 'OCS', 'ocs', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (127, '景顺物流', 'jingshun', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (128, '安达速递', 'adapost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (129, 'AAE-中国件', 'aae', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (130, '德坤物流', 'dekuncn', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (131, '联邦快递-英文', 'lianbangkuaidien', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (132, '富吉速运', 'fujisuyun', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (133, '澳天速运', 'aotsd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (134, '中国香港(HongKong Post)', 'hkpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (135, '宅急便', 'zhaijibian', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (136, '龙邦物流', 'lbex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (137, '恒路物流', 'hengluwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (138, '美国申通', 'stoexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (139, 'CJ物流', 'doortodoor', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (140, '万象物流', 'wanxiangwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (141, '澳德物流', 'auod', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (142, '百福东方', 'baifudongfang', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (143, 'TST速运通', 'tstexp', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (144, '叁虎物流', 'sanhuwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (145, 'COE', 'coe', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (146, '优优速递', 'youyou', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (147, '长江国际速递', 'changjiang', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (148, '聚盟共建', 'jumstc', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (149, '万家物流', 'wanjiawuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (150, '易达通快递', 'qexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (151, '佳怡物流', 'jiayiwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (152, 'EMS-英文', 'emsen', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (153, '中翼国际物流', 'chnexp', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (154, '集先锋快递', 'jxfex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (155, '洋包裹', 'yangbaoguo', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (156, '大田物流', 'datianwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (157, '上海缤纷物流', 'bflg', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (158, '中速快递', 'zhongsukuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (159, '迅达速递', 'xdexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (160, '速递中国', 'sendtochina', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (161, '易境达国际物流', 'uscbexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (162, '全联速运', 'guexp', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (163, '海带宝', 'haidaibao', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (164, '佐川急便', 'sagawa', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (165, '达发物流', 'dfwl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (166, '时达通', 'jssdt56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (167, '黄马甲', 'huangmajia', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (168, '百事亨通', 'bsht', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (169, '源安达', 'yuananda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (170, 'EMS包裹', 'emsbg', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (171, 'Highsince', 'highsince', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (172, '7号速递', 'express7th', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (173, '苏通快运', 'zjstky', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (174, '快捷快物流', 'gdkjk56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (175, 'TNT Australia', 'tntau', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (176, '签收快递', 'signedexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (177, '新西兰中通', 'nzzto', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (178, '南方传媒物流', 'ndwl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (179, '美通', 'valueway', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (180, 'CNPEX中邮快递', 'cnpex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (181, '出口易', 'chukou1', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (182, '耀飞同城快递', 'yaofeikuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (183, '速派快递', 'fastgoexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (184, '英国大包、EMS（Parcel Force）', 'parcelforce', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (185, '德国(Deutsche Post)', 'deutschepost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (186, '铁中快运', 'tzky', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (187, '龙枫国际快递', 'lfexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (188, '景光物流', 'jgwl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (189, '亚洲顺物流', 'yzswuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (190, '三真驿道', 'zlink', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (191, '皮牙子快递', 'bazirim', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (192, '丰通快运', 'ftky365', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (193, '成都立即送', 'lijisong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (194, '秦远物流', 'qinyuan', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (195, '新元国际', 'xynyc', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (196, 'GLS', 'gls', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (197, '安信达', 'anxindakuaixi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (198, 'DHL Benelux', 'dhlbenelux', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (199, '商桥物流', 'shangqiao56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (200, '西班牙(Correos de Espa?a)', 'correosdees', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (201, 'EMS物流', 'emswuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (202, 'DPEX', 'dpex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (203, '加拿大(Canada Post)', 'canpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (204, '荷兰邮政-中文(PostNL international registered mail)', 'postnlcn', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (205, '法国小包（colissimo）', 'colissimo', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (206, '原飞航', 'yuanfeihangwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (207, '瀚朝物流', 'hac56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (208, '德国优拜物流', 'ubuy', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (209, 'EFS Post（平安快递）', 'efs', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (210, '乌克兰邮政包裹', 'ukrpostcn', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (211, '承诺达', 'ytchengnuoda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (212, '锋鸟物流', 'beebird', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (213, '佳吉快运', 'jiajiwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (214, '久久物流', 'jiujiuwl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (215, '运通中港快递', 'ytkd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (216, '星云速递', 'nebuex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (217, 'GTS快递', 'gts', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (218, '飞洋快递', 'shipgce', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (219, 'UBI Australia', 'gotoubi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (220, '华美快递', 'hmus', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (221, '顺丰同城', 'shunfengtongcheng', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (222, '邮邦国际', 'youban', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (223, 'LUCFLOW EXPRESS', 'longfx', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (224, '鸿泰物流', 'hnht56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (225, 'CNE', 'cnexps', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (226, '顺丰-繁体', 'shunfenghk', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (227, '宏递快运', 'hd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (228, '明通国际快递', 'tnjex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (229, '春风物流', 'spring56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (230, '展勤快递', 'byht', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (231, 'UEQ快递', 'ueq', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (232, '爱尔兰(An Post)', 'anposten', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (233, '合心速递', 'hexinexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (234, '中集冷云', 'cccc58', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (235, '易客满', 'ecmscn', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (236, 'SYNSHIP快递', 'synship', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (237, '汇通天下物流', 'httx56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (238, '优速通达', 'yousutongda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (239, '威时沛运货运', 'wtdchina', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (240, '全速物流', 'quansu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (241, '合众速递(UCS）', 'ucs', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (242, '东方汇', 'est365', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (243, '全一快递', 'quanyikuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (244, '美西快递', 'meixi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (245, '一号线', 'lineone', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (246, '全时速运', 'runhengfeng', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (247, '尚橙物流', 'shangcheng', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (248, '意大利(Poste Italiane)', 'italiane', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (249, '西翼物流', 'westwing', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (250, '城晓国际快递', 'ckeex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (251, '安鲜达', 'exfresh', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (252, '渥途国际速运', 'wotu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (253, '智通物流', 'ztong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (254, '能达速递', 'ganzhongnengda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (255, '锦程快递', 'hrex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (256, 'CHS中环国际快递', 'chszhonghuanguoji', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (257, '商壹国际物流', 'com1express', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (258, '飞远配送', 'feiyuanvipshop', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (259, '考拉速递', 'koalaexp', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (260, '法国(La Poste)', 'csuivi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (261, 'airpak expresss', 'airpak', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (262, '佰麒快递', 'beckygo', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (263, '全速通', 'quansutong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (264, '安捷物流', 'anjie88', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (265, 'Hermes', 'hermes', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (266, '八达通', 'bdatong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (267, '易达通', 'yidatong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (268, '急先达', 'jixianda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (269, '品骏快递', 'pjbest', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (270, '百腾物流', 'baitengwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (271, 'PCA Express', 'pcaexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (272, '陆本速递 LUBEN EXPRESS', 'luben', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (273, '魔速达', 'mosuda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (274, '三象速递', 'sxexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (275, '淘布斯国际物流', 'taoplus', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (276, '星空国际', 'wlwex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (277, '宇佳物流', 'yujiawl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (278, '荷兰邮政-中国件', 'postnlchina', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (279, '递五方云仓', 'di5pll', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (280, '佳吉快递', 'jiajikuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (281, 'Canpar', 'canpar', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (282, '全日通', 'quanritongkuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (283, '一正达速运', 'yizhengdasuyun', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (284, '晟邦物流', 'nanjingshengbang', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (285, '英超物流', 'yingchao', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (286, '韩国（Korea Post）', 'koreapost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (287, '德尚国际速递', 'gslexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (288, '瑞典（Sweden Post）', 'ruidianyouzheng', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (289, '华瀚快递', 'hhair56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (290, '直德邮', 'zdepost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (291, '俄顺达', 'eshunda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (292, 'Toll', 'dpexen', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (293, 'OnTrac', 'ontrac', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (294, '上大物流', 'shangda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (295, '五六快运', 'wuliuky', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (296, '中国香港(HongKong Post)英文', 'hkposten', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (297, 'MoreLink', 'morelink56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (298, '中天万运', 'zhongtianwanyun', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (299, 'Asendia USA', 'asendiausa', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (300, '佳成快递 ', 'jiacheng', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (301, '荷兰速递(Nederland Post)', 'nederlandpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (302, '黑猫宅急便', 'tcat', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (303, '荷兰邮政(PostNL international registered mail)', 'postnl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (304, '店通快递', 'diantongkuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (305, '欧亚专线', 'euasia', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (306, 'wedepot物流', 'wedepot', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (307, '飞豹快递', 'feibaokuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (308, '51跨境通', 'wykjt', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (309, 'e直运', 'edtexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (310, '台湾（中华邮政）', 'postserv', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (311, '雪域易购', 'qhxyyg', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (312, 'dhl小包', 'dhlecommerce', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (313, '三态速递', 'santaisudi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (314, '一起送', 'yiqisong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (315, '韩国邮政', 'koreapostcn', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (316, 'track-parcel', 'trackparcel', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (317, '极光转运', 'jiguang', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (318, '万家康物流', 'wjkwl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (319, '雅澳物流', 'yourscm', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (320, '西游寄', 'xiyoug', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (321, '永昌物流', 'yongchangwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (322, '迅速快递', 'xunsuexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (323, '安世通快递', 'astexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (324, '北京EMS', 'bjemstckj', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (325, 'GSM', 'gsm', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (326, '云邮跨境快递', 'hkems', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (327, 'FedEx-美国件', 'fedexus', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (328, '荷兰包裹(PostNL International Parcels)', 'postnlpacle', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (329, '天翼快递', 'tykd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (330, 'TransRush', 'transrush', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (331, '城通物流', 'chengtong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (332, '比利时国际(Bpost international)', 'bpostinter', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (333, '天天欧洲物流', 'ttkeurope', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (334, '优海国际速递', 'uhi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (335, '宇捷通', 'yujtong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (336, '韵丰物流', 'yunfeng56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (337, '日本郵便', 'japanpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (338, '喀麦隆(CAMPOST)', 'cameroon', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (339, '诚和通', 'cht361', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (340, '卡塔尔（Qatar Post）', 'qpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (341, '大洋物流', 'dayangwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (342, '飞力士物流', 'flysman', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (343, '天天快物流', 'guoeryue', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (344, '骏达快递', 'jdexpressusa', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (345, 'TRAKPAK', 'trakpak', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (346, '威盛快递', 'wherexpess', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (347, 'E速达', 'exsuda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (348, 'BHT', 'bht', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (349, '创一快递', 'chuangyi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (350, '传喜物流', 'chuanxiwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (351, '家家通快递', 'newsway', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (352, '比利时(Belgium Post)', 'belgiumpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (353, '货运皇', 'kingfreight', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (354, '顺捷丰达', 'shunjiefengda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (355, '百通物流', 'buytong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (356, '飞康达', 'feikangda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (357, '上海航瑞货运', 'hangrui', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (358, '好来运', 'hlyex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (359, '澳通华人物流', 'cllexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (360, '皇家物流', 'pfcexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (361, '新加坡小包(Singapore Post)', 'singpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (362, '宇航通物流', 'yhtlogistics', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (363, '澳捷物流', 'ajlogistics', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (364, '如家国际快递', 'homecourier', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (365, '一号仓', 'onehcang', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (366, '顺达快递', 'sundarexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (367, '亚风速递', 'yafengsudi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (368, '远成物流', 'yuanchengwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (369, '百世云配', 'baishiyp', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (370, '顺通快递', 'stkd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (371, '元智捷诚', 'yuanzhijiecheng', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (372, '黑猫速运', 'heimao56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (373, '民航快递', 'minghangkuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (374, '远成快运', 'ycgky', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (375, '海红网送', 'haihongwangsong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (376, '捷安达', 'jieanda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (377, '尼尔快递', 'nell', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (378, '鹏远国际速递', 'pengyuanexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (379, '维普恩物流', 'vps', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (380, '成都东骏物流', 'dongjun', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (381, 'E通速递', 'etong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (382, '聚鼎物流', 'juding', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (383, '泰国138国际物流', 'sd138', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (384, '泰国（Thailand Thai Post）', 'thailand', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (385, '泰国中通ZTO', 'thaizto', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (386, '明大快递', 'adaexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (387, '英国(大包,EMS)', 'england', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (388, '优莎速运', 'eusacn', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (389, '共速达', 'gongsuda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (390, '吉捷国际速递', 'luckyfastex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (391, '宜送物流', 'yiex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (392, '美国云达', 'yundaexus', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (393, 'A2U速递', 'a2u', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (394, '恒宇运通', 'hyytes', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (395, '马来西亚小包（Malaysia Post(Registered)）', 'malaysiapost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (396, '北极星快运', 'polarisexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (397, '丰程物流', 'sccod', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (398, '中粮鲜到家物流', 'zlxdjwl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (399, '中骅物流', 'chunghwa56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (400, '华中快递', 'cpsair', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (401, '丹麦(Post Denmark)', 'postdanmarken', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (402, 'ABF', 'abf', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (403, '捷记方舟', 'ajexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (404, '易优包裹', 'eupackage', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (405, '骏丰国际速递', 'junfengguoji', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (406, '南非（South African Post Office）', 'southafrican', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (407, '运通中港', 'yuntongkuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (408, '卡邦配送', 'ahkbps', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (409, '河北橙配', 'chengpei', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (410, '法国大包、EMS-法文（Chronopost France）', 'chronopostfra', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (411, '速舟物流', 'cnspeedster', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (412, '安的快递', 'gda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (413, 'GE2D跨境物流', 'ge2d', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (414, 'Newgistics', 'newgistics', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (415, '全际通', 'quanjitong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (416, '新加坡EMS、大包(Singapore Speedpost)', 'speedpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (417, '瑞士邮政', 'swisspostcn', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (418, '臣邦同城', 'wto56kj', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (419, '越丰物流', 'yuefengwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (420, '法国大包、EMS-英文(Chronopost France)', 'chronopostfren', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (421, '城市映急', 'city56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (422, '邦通国际', 'comexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (423, '飞豹速递', 'hkeex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (424, '捷邦物流', 'jieborne', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (425, 'LaserShip', 'lasership', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (426, '美国快递', 'meiguokuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (427, 'MyHermes', 'myhermes', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (428, '配思货运', 'peisihuoyunkuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (429, 'Purolator', 'purolator', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (430, '赛澳递for买卖宝', 'saiaodimmb', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (431, '青岛安捷快递', 'anjiekuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (432, 'AOL澳通速递', 'aolau', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (433, '保加利亚（Bulgarian Posts）', 'bulgarian', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (434, '加拿大邮政', 'canpostfr', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (435, 'City-Link', 'citylink', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (436, 'C&C国际速递', 'cncexp', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (437, 'EASY EXPRESS', 'easyexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (438, '飞鹰物流', 'hnfy', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (439, '柬埔寨中通', 'khzto', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (440, '腾达速递', 'nntengda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (441, '日日顺快线', 'rrskx', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (442, '红马甲物流', 'sxhongmajia', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (443, '明达国际速递', 'tmwexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (444, '中运全速', 'topspeedex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (445, '万博快递', 'wanboex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (446, '安达信', 'advancing', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (447, '加拿大民航快递', 'airgtc', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (448, '中联速递', 'auvanda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (449, '奔腾物流', 'benteng', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (450, '凡宇快递', 'fanyukuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (451, '汇强快递', 'huiqiangkuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (452, '急递', 'jdpplus', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (453, '科捷物流', 'kejie', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (454, '明亮物流', 'mingliangwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (455, '猛犸速递', 'mmlogi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (456, 'NLE', 'nle', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (457, '俄罗斯邮政(Russian Post)', 'pochta', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (458, '联运通物流', 'szuem', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (459, '深圳邮政', 'szyouzheng', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (460, '祥龙运通物流', 'xianglongyuntong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (461, '一智通', '1ziton', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (462, 'ADP国际快递', 'adp', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (463, '德方物流', 'ahdf', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (464, '无忧物流', 'aliexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (465, 'EU-EXPRESS', 'euexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (466, 'FQ狂派速递', 'freakyquick', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (467, '高捷快运', 'goldjet', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (468, '贵州星程快递', 'gzxingcheng', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (469, '红远物流', 'hongywl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (470, '爱拜物流', 'ibuy8', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (471, '快达物流', 'kuaidawuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (472, '6LS EXPRESS', 'lsexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (473, '银捷速递', 'yinjiesudi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (474, '纵通速运', 'ynztsy', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (475, '中外运速递', 'zhongwaiyun', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (476, '心怡物流', 'alog', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (477, '澳世速递', 'ausexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (478, '阿塞拜疆EMS(EMS AzerExpressPost)', 'azerbaijan', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (479, '邦送物流', 'bangsongwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (480, 'CCES/国通快递', 'cces', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (481, 'Chronopost Portugal', 'chronopostport', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (482, 'CNAIR', 'cnair', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (483, '东风快递', 'dfkuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (484, '中外运', 'esinotrans', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (485, '易邮国际', 'euguoji', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (486, '飞快达', 'feikuaida', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (487, '华企快运', 'huaqikuaiyun', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (488, '大达物流', 'idada', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (489, '嘉里大荣物流', 'kerrytj', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (490, 'Landmark Global', 'landmarkglobal', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (491, '马来西亚大包、EMS（Malaysia Post(parcel,EMS)）', 'malaysiaems', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (492, '秦邦快运', 'qbexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (493, '赛澳递', 'saiaodi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (494, '乌克兰小包、大包(UkrPost)', 'ukrpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (495, '伍圆速递', 'wuyuansudi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (496, '志腾物流', 'zhitengwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (497, '中远快运', 'zy100', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (498, '亚马逊中国订单', 'amazoncnorder', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (499, '贝业物流', 'boyol', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (500, '鑫宸物流', 'cdxinchen56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (501, '银雁专送', 'cfss', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (502, '哥伦比亚(4-72 La Red Postal de Colombia)', 'colombia', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (503, '泰国中通CTO', 'ctoexp', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (504, '大道物流', 'dadaoex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (505, '龙象国际物流', 'edragon', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (506, '澳州顺风快递', 'emms', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (507, '飞狐快递', 'feihukuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (508, 'FOX国际快递', 'fox', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (509, 'Gati-中文', 'gaticn', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (510, '日日顺智慧物联', 'gooday365', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (511, '广东通路', 'guangdongtonglu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (512, '海外环球', 'haiwaihuanqiu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (513, '牙买加（Jamaica Post）', 'jamaicapost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (514, '晋越快递', 'jinyuekuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (515, '楽道物流', 'ledaowuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (516, '民邦速递', 'minbangsudi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (517, '浩博物流', 'njhaobo', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (518, '7E速递', 'qesd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (519, '林道国际快递', 'shlindao', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (520, '世华通物流', 'szshihuatong56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (521, '通和天下', 'tonghetianxia', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (522, '鑫世锐达', 'xsrd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (523, 'YODEL', 'yodel', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (524, '三三国际物流', 'zenzen', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (525, '卓实快运', 'zhuoshikuaiyun', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (526, '转瞬达集运', 'zsda56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (527, '全球快运', 'abcglobal', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (528, '澳货通', 'auex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (529, '奥地利(Austrian Post)', 'austria', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (530, '宝通快递', 'baotongkd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (531, '彪记快递', 'biaojikuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (532, '长宇物流', 'changyuwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (533, '华欣物流', 'chinastarlogistics', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (534, '中澳速递', 'cnausu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (535, '深圳德创物流', 'dechuangwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (536, 'DHL-荷兰（DHL Netherlands）', 'dhlnetherlands', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (537, 'globaltracktrace', 'globaltracktrace', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (538, '海盟速递', 'haimengsudi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (539, '驿扬国际速运', 'iyoungspeed', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (540, '嘉诚速达', 'jcsuda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (541, '金大物流', 'jindawuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (542, 'KCS', 'kcs', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (543, '淘韩国际快递', 'krtao', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (544, '中国澳门(Macau Post)', 'macao', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (545, '葡萄牙（Portugal CTT）', 'portugalctt', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (546, '人人转运', 'renrenex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (547, 'rpx', 'rpx', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (548, '四川星程快递', 'scxingcheng', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (549, '上海快通', 'shanghaikuaitong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (550, '闪货极速达', 'shanhuodidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (551, '圣安物流', 'shenganwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (552, 'SHL畅灵国际物流', 'shlexp', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (553, 'wish邮', 'shpostwish', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (554, '新速航', 'sunspeedy', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (555, 'TNT UK', 'tntuk', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (556, 'Toll Priority(Toll Online)', 'tollpriority', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (557, '优联吉运', 'uluckex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (558, '乌兹别克斯坦(Post of Uzbekistan)', 'uzbekistan', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (559, '万庚国际速递', 'vangenexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (560, '亚马逊中国', 'yamaxunwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (561, '亿领速运', 'yilingsuyun', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (562, '易欧洲国际物流', 'yiouzhou', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (563, '远航国际快运', 'yuanhhk', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (564, '玥玛速运', 'yue777', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (565, '中环转运', 'zhonghuanus', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (566, '准实快运', 'zsky123', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (567, '泰捷达国际物流', 'ztjieda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (568, '增速跨境 ', 'zyzoom', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (569, '德国雄鹰速递', 'adlerlogi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (570, '阿富汗(Afghan Post)', 'afghan', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (571, 'amazon-国内订单', 'amcnorder', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (572, '新干线快递', 'anlexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (573, 'apgecommerce', 'apgecommerce', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (574, 'Aplus物流', 'aplusex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (575, 'AUV国际快递', 'auvexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (576, '帮帮发', 'bangbangpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (577, 'BCWELT', 'bcwelt', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (578, '蜜蜂速递', 'bee001', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (579, '白俄罗斯(Belpochta)', 'belpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (580, '飛斯特', 'bester', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (581, '鑫锐达', 'bjxsrd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (582, '佰乐捷通', 'bljt56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (583, '柬埔寨(Cambodia Post)', 'cambodia', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (584, '河南次晨达', 'ccd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (585, '中邮电商', 'chinapostcb', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (586, '同舟行物流', 'chinatzx', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (587, '中欧物流', 'cneulogistics', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (588, '重庆星程快递', 'cqxingcheng', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (589, '新时速物流', 'csxss', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (590, '云南诚中物流', 'czwlyn', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (591, '德中快递', 'decnlh', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (592, '递达速运', 'didasuyun', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (593, '多道供应链', 'duodao56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (594, '易联通达', 'el56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (595, '南非EMS', 'emssouthafrica', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (596, 'europeanecom', 'europeanecom', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (597, '可可树美中速运', 'excocotree', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (598, '冠捷物流 ', 'gjwl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (599, '广通速递', 'gtongsudi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (600, '好又快物流', 'haoyoukuai', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (601, '猴急送', 'hjs', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (602, '海联快递', 'hltop', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (603, '河南全速通', 'hnqst', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (604, '中国香港环球快运', 'huanqiuabc', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (605, '匈牙利（Magyar Posta）', 'hungary', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (606, '兰州伙伴物流', 'huoban', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (607, '佳辰国际速递', 'jiachenexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (608, '加佳物流', 'jiajiawl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (609, '加州猫速递', 'jiazhoumao', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (610, '九宫物流', 'jiugong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (611, '吉祥邮（澳洲）', 'jixiangyouau', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (612, '骏绅物流', 'jsexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (613, '哈萨克斯坦(Kazpost)', 'kazpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (614, '番薯国际货运', 'koali', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (615, '蓝镖快递', 'lanbiaokuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (616, '莱索托(Lesotho Post)', 'lesotho', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (617, '联运快递', 'lianyun', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (618, '良藤国际速递', 'lmfex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (619, '隆浪快递', 'longlangkuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (620, '卢森堡(Luxembourg Post)', 'luxembourg', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (621, 'LWE', 'lwe', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (622, '马其顿(Macedonian Post)', 'macedonia', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (623, '迈隆递运', 'mailongdy', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (624, '毛里求斯(Mauritius Post)', 'mauritius', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (625, '澳洲迈速快递', 'maxeedexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (626, 'Nova Poshta', 'novaposhta', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (627, '菲律宾（Philippine Postal）', 'phlpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (628, '品速心达快递', 'pinsuxinda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (629, '土耳其', 'ptt', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (630, '急顺通', 'pzhjst', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (631, 'ANTS EXPRESS', 'qdants', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (632, '启辰国际速递', 'qichen', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (633, '千顺快递', 'qskdyxgs', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (634, '全川物流', 'quanchuan56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (635, '全信通快递', 'quanxintong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (636, '三盛快递', 'sanshengco', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (637, '塞尔维亚(PE Post of Serbia)', 'serbia', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (638, '十方通物流', 'sfift', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (639, '圣飞捷快递', 'sfjhd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (640, '神马快递', 'shenma', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (641, '阳光快递', 'shiningexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (642, '顺捷达', 'shunjieda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (643, '中外运空运', 'sinoairinex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (644, 'skynet', 'skynet', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (645, '斯洛文尼亚(Slovenia Post)', 'slovenia', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (646, '斯里兰卡(Sri Lanka Post)', 'slpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (647, '申必达', 'speedoex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (648, '穗佳物流', 'suijiawuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (649, '速配欧翼', 'superoz', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (650, '瑞士(Swiss Post)', 'swisspost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (651, '万家通快递', 'timedg', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (652, '天联快运', 'tlky', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (653, '通达兴物流', 'tongdaxing', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (654, '乌克兰小包、大包(UkrPoshta)', 'ukraine', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (655, '华夏国际速递', 'uschuaxia', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (656, '越南小包(Vietnam Posts)', 'vietnam', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (657, '越南EMS(VNPost Express)', 'vnpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (658, '豌豆物流', 'wandougongzhu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (659, '沃埃家', 'wowvip', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (660, '微转运', 'wzhaunyun', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (661, '蓝天物流', 'xflt56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (662, '鑫远东速运', 'xyd666', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (663, '云达通', 'ydglobe', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (664, 'YDH', 'ydhex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (665, '艺凡快递', 'yifankd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (666, '一柒国际物流', 'yiqiguojiwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (667, '亿顺航', 'yishunhang', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (668, '易邮速运', 'yiyou', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (669, '武汉优进汇', 'yjhgo', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (670, '洋口岸', 'ykouan', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (671, '壹品速递', 'ypsd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (672, '珠峰速运', 'zf365', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (673, '中技物流', 'zhongjiwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (674, '忠信达', 'zhongxinda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (675, '创运物流', 'zjcy56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (676, '明辉物流', 'zsmhwl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (677, 'ZTE中兴物流', 'zteexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (678, '安达易国际速递', 'adiexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (679, 'AFL', 'afl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (680, '全程快递', 'agopost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (681, '阿尔巴尼亚(Posta shqipatre)', 'albania', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (682, 'amazon-国际订单', 'amusorder', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (683, '安家同城快运', 'anjiatongcheng', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (684, '澳速物流', 'aosu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (685, '美国汉邦快递', 'aplus100', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (686, '艾瑞斯远', 'ariesfar', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (687, '阿鲁巴[荷兰]（Post Aruba）', 'aruba', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (688, '澳达国际物流', 'auadexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (689, '澳邦国际物流', 'ausbondexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (690, '澳新物流', 'axexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (691, '巴林(Bahrain Post)', 'bahrain', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (692, '孟加拉国(EMS)', 'bangladesh', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (693, '报通快递', 'baoxianda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (694, '巴巴多斯(Barbados Post)', 'barbados', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (695, '伯利兹(Belize Postal)', 'belize', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (696, '笨鸟国际', 'benniao', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (697, '青云物流', 'bjqywl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (698, 'BlueDart', 'bluedart', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (699, '标杆物流', 'bmlchina', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (700, '波黑(JP BH Posta)', 'bohei', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (701, '玻利维亚', 'bolivia', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (702, 'BorderGuru', 'borderguru', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (703, '堡昕德速递', 'bosind', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (704, '博茨瓦纳', 'botspost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (705, '速方(Sufast)', 'bphchina', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (706, '百千诚物流', 'bqcwl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (707, '巴西(Brazil Post/Correios)', 'brazilposten', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (708, '文莱(Brunei Postal)', 'brunei', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (709, '新喀里多尼亚[法国](New Caledonia)', 'caledonia', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (710, '到了港', 'camekong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (711, 'Campbell’s Express', 'campbellsexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (712, '能装能送', 'canhold', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (713, '卢森堡航空', 'cargolux', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (714, '钏博物流', 'cbo56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (715, 'CDEK', 'cdek', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (716, '捷祥物流', 'cdjx56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (717, '捷克（?eská po?ta）', 'ceskaposta', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (718, 'CEVA Logistic', 'cevalogistics', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (719, '城铁速递', 'cex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (720, '昌宇国际', 'changwooair', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (721, '成达国际速递', 'chengda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (722, '城际快递', 'chengji', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (723, '智利(Correos Chile)', 'chile', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (724, 'SQK国际速递', 'chinasqk', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (725, '嘉荣物流', 'chllog', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (726, '中国香港骏辉物流', 'chunfai', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (727, 'citysprint', 'citysprint', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (728, '大韩通运', 'cjkoreaexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (729, 'CE易欧通国际速递', 'cloudexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (730, 'CL日中速运', 'clsp', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (731, 'CNUP 中联邮', 'cnup', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (732, '中国翼', 'cnws', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (733, '莫桑比克（Correios de Moçambique）', 'correios', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (734, '乌拉圭（Correo Uruguayo）', 'correo', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (735, '阿根廷(Correo Argentina)', 'correoargentino', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (736, '哥斯达黎加(Correos de Costa Rica)', 'correos', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (737, '环旅快运', 'crossbox', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (738, '塞浦路斯(Cyprus Post)', 'cypruspost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (739, '小飞侠速递', 'cyxfx', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (740, '丹递56', 'dande56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (741, '达速物流', 'dasu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (742, 'DCS', 'dcs', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (743, 'Deltec Courier', 'deltec', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (744, '澳行快递', 'desworks', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (745, '达方物流', 'dfpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (746, 'DHL HK', 'dhlhk', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (747, 'DHL-波兰（DHL Poland）', 'dhlpoland', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (748, '云南滇驿物流', 'dianyi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (749, '叮咚澳洲转运', 'dindon', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (750, '叮咚快递', 'dingdong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (751, 'Direct Link', 'directlink', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (752, '递四方澳洲', 'disifangau', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (753, '递四方美国', 'disifangus', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (754, '天翔东捷运', 'djy56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (755, '东瀚物流', 'donghanwl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (756, '东红物流', 'donghong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (757, 'DPD Germany', 'dpdgermany', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (758, 'DPD Poland', 'dpdpoland', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (759, 'DPD UK', 'dpduk', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (760, 'DTDC India', 'dtdcindia', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (761, '东方航空物流', 'ealceair', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (762, 'E跨通', 'ecallturn', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (763, 'EC-Firstclass', 'ecfirstclass', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (764, 'ECMS Express', 'ecmsglobal', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (765, '东西E全运', 'ecotransite', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (766, '厄瓜多尔(Correos del Ecuador)', 'ecuador', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (767, '易达快运', 'edaeuexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (768, 'EFSPOST', 'efspost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (769, '埃及（Egypt Post）', 'egypt', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (770, '艾菲尔国际速递', 'eiffel', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (771, '希腊包裹（ELTA Hellenic Post）', 'elta', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (772, '希腊EMS（ELTA Courier）', 'eltahell', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (773, '阿联酋(Emirates Post)', 'emirates', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (774, '波兰小包(Poczta Polska)', 'emonitoring', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (775, '乌克兰EMS(EMS Ukraine)', 'emsukraine', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (776, '乌克兰EMS-中文(EMS Ukraine)', 'emsukrainecn', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (777, '联众国际', 'epspost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (778, 'Estafeta', 'estafeta', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (779, 'Estes', 'estes', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (780, '易达国际速递', 'eta100', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (781, 'ETEEN专线', 'eteenlog', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (782, '埃塞俄比亚(Ethiopian postal)', 'ethiopia', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (783, '中欧国际物流', 'eucnrail', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (784, '德国 EUC POST', 'eucpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (785, '败欧洲', 'europe8', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (786, '澳洲新干线快递', 'expressplus', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (787, '易转运', 'ezhuanyuan', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (788, '颿达国际快递-英文', 'fandaguoji', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (789, '颿达国际快递', 'fardarww', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (790, '泛远国际物流', 'farlogistis', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (791, '加拿大联通快运', 'fastontime', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (792, 'Fastway Ireland', 'fastway', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (793, '正途供应链', 'fastzt', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (794, '飞邦快递', 'fbkd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (795, 'FedEx-英国件（FedEx UK)', 'fedexuk', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (796, 'FedEx-英国件', 'fedexukcn', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (797, 'FedRoad 联邦转运', 'fedroad', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (798, '凤凰快递', 'fenghuangkuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (799, '丰羿', 'fengyee', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (800, '斐济(Fiji Post)', 'fiji', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (801, '芬兰(Itella Posti Oy)', 'finland', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (802, '花瓣转运', 'flowerkd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (803, '四方格', 'fourpxus', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (804, '全速快递', 'fsexp', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (805, '法翔速运', 'ftlexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (806, '飞云快递系统', 'fyex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (807, '高铁快运', 'gaotieex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (808, 'Gati-英文', 'gatien', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (809, 'Gati-KWE', 'gatikwe', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (810, '广东诚通物流', 'gdct56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (811, '全网物流', 'gdqwwl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (812, '容智快运', 'gdrz58', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (813, '新鹏快递', 'gdxp', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (814, '格鲁吉亚(Georgian Pos）', 'georgianpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (815, '环创物流', 'ghl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (816, 'GHT物流', 'ghtexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (817, '直布罗陀[英国]( Royal Gibraltar Post)', 'gibraltar', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (818, '英脉物流', 'gml', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (819, '格陵兰[丹麦]（TELE Greenland A/S）', 'greenland', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (820, '潍鸿', 'grivertek', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (821, '哥士传奇速递', 'gscq365', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (822, '万通快递', 'gswtkd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (823, 'GT国际快运', 'gtgogo', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (824, 'GTT EXPRESS快递', 'gttexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (825, '冠庭国际物流', 'guanting', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (826, '国送快运', 'guosong', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (827, '宏观国际快递', 'gvpexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (828, '光线速递', 'gxwl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (829, '海红for买卖宝', 'haihongmmb', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (830, '海星桥快递', 'haixingqiao', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (831, '海中转运', 'haizhongzhuanyun', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (832, '汉邦国际速递', 'handboy', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (833, '翰丰快递', 'hanfengjl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (834, '航宇快递', 'hangyu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (835, '开心快递', 'happylink', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (836, '亚美尼亚(Haypost-Armenian Postal)', 'haypost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (837, '汇达物流', 'hdcexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (838, '恒瑞物流', 'hengrui56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (839, '环国运物流', 'hgy56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (840, 'Hi淘易快递', 'hitaoe', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (841, '互联快运', 'hlkytj', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (842, '共联配', 'hlpgyl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (843, '顺时达物流', 'hnssd56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (844, '中强物流', 'hnzqwl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (845, '居家通', 'homexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (846, '红背心', 'hongbeixin', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (847, '宏捷国际物流', 'hongjie', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (848, '宏品物流', 'hongpinwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (849, '皇家云仓', 'hotwms', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (850, '环球通达 ', 'hqtd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (851, '卓烨快递', 'hrbzykd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (852, '高铁速递', 'hre', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (853, '克罗地亚（Hrvatska Posta）', 'hrvatska', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (854, '海硕高铁速递', 'hsgtsd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (855, '海淘物流', 'ht22', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (856, '华通快运', 'htongexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (857, '华通务达物流', 'htwd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (858, '华达快运', 'huada', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (859, '环东物流', 'huandonglg', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (860, '华夏货运', 'huaxiahuoyun', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (861, '驼峰国际', 'humpline', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (862, '户通物流', 'hutongwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (863, '鸿远物流', 'hyeship', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (864, '上海昊宏国际货物', 'hyk', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (865, '华航快递', 'hzpl', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (866, '冰岛(Iceland Post)', 'iceland', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (867, '泛太优达', 'iex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (868, 'iExpress', 'iexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (869, '无限速递', 'igcaexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (870, 'logen路坚', 'ilogen', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (871, 'ILYANG', 'ilyang', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (872, '艾姆勒', 'imlb2c', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (873, '印度(India Post)', 'india', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (874, '印度尼西亚EMS(Pos Indonesia-EMS)', 'indonesia', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (875, '多米尼加（INPOSDOM – Instituto Postal Dominicano）', 'inposdom', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (876, 'Interlink Express', 'interlink', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (877, 'UPS i-parcel', 'iparcel', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (878, '伊朗（Iran Post）', 'iran', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (879, '以色列(Israel Post)', 'israelpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (880, 'Italy SDA', 'italysad', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (881, 'jcex', 'jcex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (882, 'JDIEX', 'jdiex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (883, '泽西岛', 'jerseypost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (884, '澳速通国际速递', 'jetexpressgroup', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (885, '佳家通货运', 'jiajiatong56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (886, '锦程物流', 'jinchengwuliu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (887, '劲通快递', 'jintongkd', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (888, '冀速物流', 'jisu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (889, '久易快递', 'jiuyicn', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (890, '佳捷翔物流', 'jjx888', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (891, '约旦(Jordan Post)', 'jordan', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (892, '聚物物流', 'juwu', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (893, '聚中大', 'juzhongda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (894, '考拉国际速递', 'kaolaexpress', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (895, '肯尼亚(POSTA KENYA)', 'kenya', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (896, '启邦国际物流', 'keypon', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (897, '快服务', 'kfwnet', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (898, '跨境直邮通', 'kjde', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (899, '韩国邮政韩文', 'koreapostkr', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (900, '快速递', 'ksudi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (901, '快8速运', 'kuai8', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (902, '快淘快递', 'kuaitao', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (903, '四川快优达速递', 'kuaiyouda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (904, '凯信达', 'kxda', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (905, '吉尔吉斯斯坦(Kyrgyz Post)', 'kyrgyzpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (906, '跨跃国际', 'kyue', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (907, '蓝弧快递', 'lanhukuaidi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (908, '老挝(Lao Express) ', 'lao', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (909, '塞内加尔', 'laposte', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (910, '林安物流', 'lasy56', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (911, '拉脱维亚(Latvijas Pasts)', 'latvia', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (912, '立白宝凯物流', 'lbbk', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (913, '林道国际快递-英文', 'ldxpres', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (914, '乐递供应链', 'ledii', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (915, '云豹国际货运', 'leopard', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (916, '美联快递', 'letseml', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (917, 'lazada', 'lgs', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (918, '联合速递', 'lhexpressus', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (919, '黎巴嫩(Liban Post)', 'libanpost', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (920, 'Linex', 'linex', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (921, '丽狮物流', 'lishi', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (922, '立陶宛（Lietuvos pa?tas）', 'lithuania', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (923, '小熊物流', 'littlebearbear', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (924, '華信物流WTO', 'logistics', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (925, '加拿大龙行速运', 'longcps', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (926, '长风物流', 'longvast', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (927, '恒通快递', 'lqht', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (928, '乐天速递', 'ltexp', 1605403324, 1605403324, NULL);
INSERT INTO `tn_shop_express_company` VALUES (929, '联通快递', 'ltparcel', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (930, '论道国际物流', 'lundao', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (931, '鲁通快运', 'lutong', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (932, '麦力快递', 'mailikuaidi', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (933, '马尔代夫(Maldives Post)', 'maldives', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (934, '马耳他（Malta Post）', 'malta', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (935, '芒果速递', 'mangguo', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (936, '今枫国际快运', 'mapleexpress', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (937, '木春货运', 'mchy', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (938, '美邦国际快递', 'meibang', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (939, '美达快递', 'meidaexpress', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (940, '美泰物流', 'meitai', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (941, '墨西哥（Correos de Mexico）', 'mexico', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (942, 'Mexico Senda Express', 'mexicodenda', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (943, '银河物流', 'milkyway', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (944, '美龙快递', 'mjexp', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (945, '摩尔多瓦(Posta Moldovei)', 'moldova', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (946, '蒙古国(Mongol Post) ', 'mongolpost', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (947, '黑山(Posta Crne Gore)', 'montenegro', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (948, '摩洛哥 ( Morocco Post )', 'morocco', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (949, 'MRW', 'mrw', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (950, 'Mexico Multipack', 'multipack', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (951, '中俄速通（淼信）', 'mxe56', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (952, '新亚物流', 'nalexpress', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (953, '纳米比亚(NamPost)', 'namibia', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (954, '红马速递', 'nedahm', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (955, '尼泊尔（Nepal Postal Services）', 'nepalpost', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (956, '尼日利亚(Nigerian Postal)', 'nigerianpost', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (957, '牛仔速运', 'niuzaiexpress', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (958, '亚欧专线', 'nlebv', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (959, '华赫物流', 'nmhuahe', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (960, '诺尔国际物流', 'nuoer', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (961, '偌亚奥国际快递', 'nuoyaao', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (962, 'OCA Argentina', 'ocaargen', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (963, 'OC-Post', 'ocpost', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (964, '阿曼(Oman Post)', 'oman', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (965, '爱沙尼亚(Eesti Post)', 'omniva', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (966, '昂威物流', 'onway', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (967, 'OPEK', 'opek', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (968, '波音速递', 'overseaex', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (969, '巴基斯坦(Pakistan Post)', 'pakistan', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (970, '巴拉圭(Correo Paraguayo)', 'paraguay', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (971, '诚一物流', 'parcelchina', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (972, '英国邮政大包EMS', 'parcelforcecn', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (973, '顺捷美中速递', 'passerbyaexpress', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (974, '全球速递', 'pdstow', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (975, '派尔快递', 'peex', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (976, '陪行物流', 'peixingwuliu', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (977, '鹏程快递', 'pengcheng', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (978, '秘鲁(SERPOST)', 'peru', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (979, '品信快递', 'pinxinkuaidi', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (980, '先锋国际快递', 'pioneer', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (981, '龙行天下', 'pmt0704be', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (982, 'Portugal Seur', 'portugalseur', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (983, '坦桑尼亚（Tanzania Posts Corporation）', 'posta', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (984, 'PostElbe', 'postelbe', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (985, 'PostNord(Posten AB)', 'postenab', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (986, '挪威（Posten Norge）', 'postennorge', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (987, '巴布亚新几内亚(PNG Post)', 'postpng', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (988, '雪域快递', 'qhxykd', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (989, '千里速递', 'qianli', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (990, 'Quantium', 'quantium', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (991, '全通快运', 'quantwl', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (992, '全之鑫物流', 'qzx56', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (993, 'Red Express', 'redexpress', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (994, '叙利亚(Syrian Post)', 'republic', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (995, '睿和泰速运', 'rhtexpress', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (996, '日昱物流', 'riyuwuliu', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (997, '荣庆物流', 'rokin', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (998, '罗马尼亚（Posta Romanian）', 'romanian', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (999, '日日通国际', 'rrthk', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1000, '卢旺达(Rwanda i-posita)', 'rwanda', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1001, 'S2C', 's2c', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1002, 'Safexpress', 'safexpress', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1003, '萨摩亚(Samoa Post)', 'samoa', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1004, '沙特阿拉伯(Saudi Post)', 'saudipost', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1005, '中加国际快递', 'scic', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1006, '速佳达快运', 'scsujiada', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1007, '速呈', 'sczpds', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1008, '首达速运', 'sdsy888', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1009, 'Selektvracht', 'selektvracht', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1010, 'International Seur', 'seur', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1011, '澳丰速递', 'sfau', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1012, '曹操到', 'sfpost', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1013, '衫达快运', 'shanda56', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1014, '上海无疆for买卖宝', 'shanghaiwujiangmmb', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1015, '尚途国际货运', 'shangtuguoji', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1016, '捎客物流', 'shaoke', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1017, '杰响物流', 'shbwch', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1018, '商海德物流', 'shd56', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1019, '盛通快递', 'shengtongscm', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1020, '神骏物流', 'shenjun', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1021, '王牌快递', 'shipbyace', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1022, '苏豪快递', 'shipsoho', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1023, '世运快递', 'shiyunkuaidi', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1024, '顺邦国际物流', 'shunbang', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1025, '顺士达速运', 'shunshid', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1026, '四海快递', 'sihaiet', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1027, '四海捷运', 'sihiexpress', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1028, '中外运速递-中文', 'sinoex', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1029, 'Siodemka', 'siodemka', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1030, '易普递', 'sixroad', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1031, 'SkyNet Malaysia', 'skynetmalaysia', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1032, 'skynetworldwide', 'skynetworldwide', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1033, '荷兰Sky Post', 'skypost', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1034, '斯洛伐克(Slovenská Posta)', 'slovak', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1035, '嗖一下同城快递', 'sofast56', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1036, '行必达', 'speeda', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1037, '首通快运', 'staky', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1038, '星速递', 'starex', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1039, '星运快递', 'staryvr', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1040, '智德物流', 'stzd56', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1041, '速豹', 'subaoex', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1042, '速呈宅配', 'sucheng', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1043, '特急便物流', 'sucmj', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1044, '苏丹（Sudapost）', 'sudapost', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1045, '速风快递', 'sufengkuaidi', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1046, '郑州速捷', 'sujievip', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1047, '速品快递', 'supinexpress', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1048, '深圳DPEX', 'szdpex', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1049, '泰进物流', 'taijin', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1050, '天美快递', 'taimek', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1051, '坦桑尼亚(Tanzania Posts)', 'tanzania', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1052, 'TCI XPS', 'tcixps', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1053, 'TCXB国际物流', 'tcxbthai', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1054, 'TD Cargo', 'tdcargo', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1055, '加拿大雷霆快递', 'thunderexpress', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1056, '天翔快递', 'tianxiang', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1057, '天纵物流', 'tianzong', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1058, '株式会社T.M.G', 'tmg', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1059, 'TNT Italy', 'tntitaly', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1060, 'TNT Post', 'tntpostcn', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1061, 'TNY物流', 'tny', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1062, '顶世国际物流', 'topshey', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1063, '突尼斯EMS(Rapid-Poste)', 'tunisia', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1064, '海龟国际快递', 'turtle', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1065, '天翼物流', 'tywl99', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1066, 'UEX国际物流', 'uex', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1067, '欧洲UEX', 'uexiex', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1068, '乌干达(Posta Uganda)', 'uganda', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1069, '邮鸽速运', 'ugoexpress', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1070, 'UPS Freight', 'upsfreight', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1071, 'UPS Mail Innovations', 'upsmailinno', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1072, 'USPSCN', 'uspscn', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1073, 'UTAO优到', 'utaoscm', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1074, '瓦努阿图(Vanuatu Post)', 'vanuatu', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1075, '越中国际物流', 'vctrans', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1076, '鹰运国际速递', 'vipexpress', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1077, '宁夏万家通', 'wanjiatong', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1078, '万达美', 'wdm', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1079, '文捷航空', 'wenjiesudi', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1080, '威速递', 'wexpress', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1081, '香港伟豪国际物流', 'whgjkd', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1082, '万邑通', 'winit', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1083, '凡仕特物流', 'wlfast', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1084, 'WTD海外通', 'wtdex', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1085, '万运国际快递', 'wygj168', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1086, '国晶物流', 'xdshipping', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1087, '西安城联速递', 'xianchengliansudi', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1088, '湘达物流', 'xiangdawuliu', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1089, '翔腾物流', 'xiangteng', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1090, '小C海淘', 'xiaocex', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1091, '西安喜来快递', 'xilaikd', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1092, '新元快递', 'xingyuankuaidi', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1093, '新宁物流', 'xinning', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1094, '西邮寄', 'xipost', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1095, '鑫通宝物流', 'xtb', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1096, '一辉物流', 'yatfai', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1097, 'YCG物流', 'ycgglobal', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1098, '易达丰国际速递', 'ydfexpress', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1099, '也门(Yemen Post)', 'yemen', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1100, '一邦速递', 'yibangwuliu', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1101, '驿递汇速递', 'yidihui', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1102, '易航物流', 'yihangmall', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1103, '宜送', 'yisong', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1104, '易通达', 'yitongda', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1105, '永邦国际物流', 'yongbangwuliu', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1106, '友家速递', 'youjia', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1107, '邮来速递', 'youlai', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1108, '运通快运', 'ytky168', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1109, '远盾物流', 'yuandun', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1110, '粤中国际货运代理（上海）有限公司', 'yuezhongsh', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1111, '御风速运', 'yufeng', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1112, '德国云快递', 'yunexpress', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1113, '远为快递', 'ywexpress', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1114, '西安运逸快递', 'yyexp', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1115, '一运全成物流', 'yyqc56', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1116, '众辉达物流', 'zhdwl', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1117, '众川国际', 'zhongchuan', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1118, '众派速递', 'zhpex', 1605403325, 1605403325, NULL);
INSERT INTO `tn_shop_express_company` VALUES (1119, '振捷国际货运', 'zjgj56', 1605403325, 1605403325, NULL);

-- ----------------------------
-- Table structure for tn_shop_product
-- ----------------------------
DROP TABLE IF EXISTS `tn_shop_product`;
CREATE TABLE `tn_shop_product`  (
  `id` bigint(16) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '图鸟商店商品主键id',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图鸟商店商品标题',
  `main_image` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '图鸟商店商品主图',
  `keyword` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图鸟商店商品关键字',
  `desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图鸟商店商品描述',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '图鸟商店商品内容',
  `category_id` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图鸟商店商品所属分类',
  `virtual_product` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图鸟商店商品是否为虚拟商品',
  `sales_volume` bigint(16) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图鸟商店商品销量',
  `virtual_sales_volume` bigint(16) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图鸟商店商品虚拟销量',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '图鸟商店商品排序序号',
  `recomm` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图鸟商店商品是否推荐',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '图鸟商店商品上架状态',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存图鸟商店商品内容' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_shop_product
-- ----------------------------
INSERT INTO `tn_shop_product` VALUES (1, '图鸟小程序开源项目', '[\"\\/storage\\/uploads\\/img_list\\/1c\\/bfaa4ffefcf77e857c60c97ca545da.jpeg\"]', '图鸟小程序开源版', '图鸟小程序开源版', '<p><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/tn_shop_product_content/20201205/aa185465d9d01b70fb522c57ac55865a.jpeg\" /><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/tn_shop_product_content/20201205/b1048863e228c15d9b366d3a2516f8f4.jpeg\" /><img class=\"wscnph\" style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://website_open.tuniaokj.com/storage/uploads/tn_shop_product_content/20201205/8f4925c4404847dae6e8d3d6474c2e9d.jpeg\" /></p>', 2, 1, 11, 100, 1, 0, 1, 1607162870, 1621432387, NULL);
INSERT INTO `tn_shop_product` VALUES (2, '小程序定制开发', '[\"\\/storage\\/uploads\\/img_list\\/3f\\/3102dfdb1d1fa21451fac9b94c3113.jpeg\"]', '更多案例，请查看图鸟小程序官网案例', '更多案例，请查看图鸟小程序官网案例', '<p><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/tn_shop_product_content/20201205/f03e58ce802000926f6da6a461ffe542.jpeg\" /><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/tn_shop_product_content/20201205/4234eddcecc62f4415ac1fe99bb21b71.jpeg\" /></p>', 2, 1, 0, 99, 2, 0, 1, 1607163317, 1607163317, NULL);
INSERT INTO `tn_shop_product` VALUES (3, '微信红包封面设计', '[\"\\/storage\\/uploads\\/img_list\\/06\\/4247b392f77bd697b6bd525b1948c0.jpg\",\"\\/storage\\/uploads\\/img_list\\/9e\\/5992d8c0c7dad43edce0dff7ef0b48.jpg\"]', '微信红包封面设计', '先聊妥再慎拍', '<p><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/tn_shop_product_content/20201205/81661dd63cf1ffd498c14cc081b4c6a4.jpeg\" /><img class=\"wscnph\" style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://website_open.tuniaokj.com/storage/uploads/tn_shop_product_content/20201205/fe0305019a4d9d594a0818a7d9de3d0e.jpeg\" /></p>', 1, 1, 0, 99, 1, 0, 1, 1607163797, 1607163797, NULL);

-- ----------------------------
-- Table structure for tn_shop_product_parameters
-- ----------------------------
DROP TABLE IF EXISTS `tn_shop_product_parameters`;
CREATE TABLE `tn_shop_product_parameters`  (
  `product_id` bigint(16) UNSIGNED NOT NULL DEFAULT 0 COMMENT '产品参数对应的商品id',
  `name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '产品参数的名字',
  `value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '产品参数的值',
  INDEX `product_id`(`product_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存商品对应的产品参数' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_shop_product_parameters
-- ----------------------------
INSERT INTO `tn_shop_product_parameters` VALUES (2, '开发者', '广州图鸟科技有限公司');
INSERT INTO `tn_shop_product_parameters` VALUES (3, '开发者', '广州图鸟科技有限公司');
INSERT INTO `tn_shop_product_parameters` VALUES (1, '开发者', '广州图鸟科技有限公司');

-- ----------------------------
-- Table structure for tn_shop_product_property
-- ----------------------------
DROP TABLE IF EXISTS `tn_shop_product_property`;
CREATE TABLE `tn_shop_product_property`  (
  `id` bigint(16) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图鸟商店商品属性id',
  `name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图鸟商店商品属性名称',
  `product_id` bigint(16) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图鸟商店商品id',
  INDEX `property_id`(`id`) USING BTREE,
  INDEX `product_id`(`product_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存商品的属性信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_shop_product_property
-- ----------------------------
INSERT INTO `tn_shop_product_property` VALUES (211, '原型', 2);
INSERT INTO `tn_shop_product_property` VALUES (221, 'UI', 2);
INSERT INTO `tn_shop_product_property` VALUES (231, '服务器', 2);
INSERT INTO `tn_shop_product_property` VALUES (311, '封面', 3);
INSERT INTO `tn_shop_product_property` VALUES (321, '封面故事', 3);
INSERT INTO `tn_shop_product_property` VALUES (331, '封面故事视频', 3);
INSERT INTO `tn_shop_product_property` VALUES (111, '小程序版本', 1);
INSERT INTO `tn_shop_product_property` VALUES (121, '后台版本', 1);

-- ----------------------------
-- Table structure for tn_shop_product_specs
-- ----------------------------
DROP TABLE IF EXISTS `tn_shop_product_specs`;
CREATE TABLE `tn_shop_product_specs`  (
  `id` bigint(16) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图鸟商店商品规格',
  `product_id` bigint(16) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图鸟商店商品id',
  `product_image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '图鸟商店商品规格图片',
  `original_price` decimal(8, 2) UNSIGNED NOT NULL COMMENT '图鸟商店商品规格原价',
  `selling_price` decimal(8, 2) UNSIGNED NOT NULL COMMENT '图鸟商店商品规格售价',
  `stock` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图鸟商店商品规格库存',
  `order_stock` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '图鸟商店商品订单库存（用于记录用户下单后的库存）',
  INDEX `specs_id`(`id`) USING BTREE,
  INDEX `product_id`(`product_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存商品的规格信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_shop_product_specs
-- ----------------------------
INSERT INTO `tn_shop_product_specs` VALUES (212, 2, '', 0.00, 52089.00, 10, 0);
INSERT INTO `tn_shop_product_specs` VALUES (222, 2, '', 0.00, 20099.00, 10, 0);
INSERT INTO `tn_shop_product_specs` VALUES (312, 3, '', 4098.00, 2699.00, 100, 0);
INSERT INTO `tn_shop_product_specs` VALUES (322, 3, '', 5098.00, 3099.00, 100, 0);
INSERT INTO `tn_shop_product_specs` VALUES (332, 3, '', 6098.00, 4099.00, 100, 0);
INSERT INTO `tn_shop_product_specs` VALUES (112, 1, '', 0.00, 0.01, 0, 0);
INSERT INTO `tn_shop_product_specs` VALUES (122, 1, '', 0.00, 0.01, 993, 0);
INSERT INTO `tn_shop_product_specs` VALUES (132, 1, '', 1000.00, 8.80, 19, 0);

-- ----------------------------
-- Table structure for tn_shop_product_specs_property
-- ----------------------------
DROP TABLE IF EXISTS `tn_shop_product_specs_property`;
CREATE TABLE `tn_shop_product_specs_property`  (
  `specs_id` bigint(16) NULL DEFAULT NULL,
  `property_id` bigint(16) NULL DEFAULT NULL,
  `value` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  UNIQUE INDEX `specs_property_id`(`specs_id`, `property_id`) USING BTREE,
  INDEX `specs_id`(`specs_id`) USING BTREE,
  INDEX `property_id`(`property_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存规则对应属性的值' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_shop_product_specs_property
-- ----------------------------
INSERT INTO `tn_shop_product_specs_property` VALUES (212, 211, '没有原型');
INSERT INTO `tn_shop_product_specs_property` VALUES (212, 221, '没有UI');
INSERT INTO `tn_shop_product_specs_property` VALUES (212, 231, '没有服务器');
INSERT INTO `tn_shop_product_specs_property` VALUES (222, 211, '有原型');
INSERT INTO `tn_shop_product_specs_property` VALUES (222, 221, '有UI');
INSERT INTO `tn_shop_product_specs_property` VALUES (222, 231, '有服务器');
INSERT INTO `tn_shop_product_specs_property` VALUES (312, 311, '主页面');
INSERT INTO `tn_shop_product_specs_property` VALUES (312, 321, '不带故事');
INSERT INTO `tn_shop_product_specs_property` VALUES (312, 331, '不带故事视频');
INSERT INTO `tn_shop_product_specs_property` VALUES (322, 311, '主页面');
INSERT INTO `tn_shop_product_specs_property` VALUES (322, 321, '带故事');
INSERT INTO `tn_shop_product_specs_property` VALUES (322, 331, '不带故事视频');
INSERT INTO `tn_shop_product_specs_property` VALUES (332, 311, '主页面');
INSERT INTO `tn_shop_product_specs_property` VALUES (332, 321, '带故事');
INSERT INTO `tn_shop_product_specs_property` VALUES (332, 331, '带故事视频');
INSERT INTO `tn_shop_product_specs_property` VALUES (112, 111, '小程序');
INSERT INTO `tn_shop_product_specs_property` VALUES (112, 121, 'php');
INSERT INTO `tn_shop_product_specs_property` VALUES (122, 111, 'uni-app');
INSERT INTO `tn_shop_product_specs_property` VALUES (122, 121, 'php');
INSERT INTO `tn_shop_product_specs_property` VALUES (132, 111, '活动专属');
INSERT INTO `tn_shop_product_specs_property` VALUES (132, 121, '活动专属');

-- ----------------------------
-- Table structure for tn_system_config
-- ----------------------------
DROP TABLE IF EXISTS `tn_system_config`;
CREATE TABLE `tn_system_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '设置的主键id',
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '设置的父级id',
  `cn_name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '配置对应的中文名',
  `en_name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '配置对应的字段名称',
  `values` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置的可选值',
  `tips` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置提示值',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置的值',
  `type` tinyint(2) UNSIGNED NOT NULL DEFAULT 1 COMMENT '配置的类型 1：文本输入框 2：单选按钮 3：复选框 4：下拉菜单 5：文本域 6：富文本 7：单图片 8：多图片 9：附件上传',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '配置的排序序号',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '配置项的状态（1 开启 0 关闭）',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id_en_name`(`id`, `en_name`) USING BTREE,
  INDEX `en_name`(`en_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存系统配置的相关信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_system_config
-- ----------------------------
INSERT INTO `tn_system_config` VALUES (1, 0, '网站设置', '', '', '', '', 0, 1, 1, 1588944799, 1588944799, NULL);
INSERT INTO `tn_system_config` VALUES (2, 1, '标题', 'site_title', '', '', '图鸟科技后台管理系统', 1, 1, 1, 1589362296, 1625531074, NULL);
INSERT INTO `tn_system_config` VALUES (3, 1, '域名', 'site_url', '', '请以http://或者https://开头', 'http://website_open.tuniaokj.com', 1, 2, 1, 1589362366, 1625531074, NULL);
INSERT INTO `tn_system_config` VALUES (4, 1, '作者', 'site_author', '', '', '图鸟科技', 1, 3, 1, 1589367023, 1625531074, NULL);
INSERT INTO `tn_system_config` VALUES (5, 1, '备案号', 'site_record', '', '', '', 1, 4, 1, 1589367043, 1625531074, NULL);
INSERT INTO `tn_system_config` VALUES (6, 1, 'token缓存时间', 'site_token_expire_in', '', '设置token的缓存时间，以秒作为单位。提示：通常根据实际情况进行设置', '7200', 10, 5, 1, 1589367104, 1625531074, NULL);
INSERT INTO `tn_system_config` VALUES (7, 1, '允许上传的最大文件大小', 'site_file_max_size', '', '设置上传文件的最大大小，以KB为单位。提示：1 M = 1024 KB', '2048', 10, 6, 1, 1589367156, 1625531074, NULL);
INSERT INTO `tn_system_config` VALUES (8, 1, '开启https', 'site_https_on', '开启,关闭', '提示：此选项是开启或者关闭与微信服务器通讯的时候是否开启https(在Linus下部署请选中该选项)', '关闭', 2, 7, 1, 1589367240, 1625531074, NULL);
INSERT INTO `tn_system_config` VALUES (9, 1, 'Logo', 'site_logo', '', '', '/storage/uploads/img_list/c6/aab26376f3f6dc3c5a737784e45985.png', 7, 8, 1, 1589367265, 1625531074, NULL);
INSERT INTO `tn_system_config` VALUES (10, 0, '后台设置', '', '', '', '', 0, 2, 1, 1589367318, 1589367318, NULL);
INSERT INTO `tn_system_config` VALUES (11, 10, '权限验证', 'bk_auth_on', '开启,关闭', '', '开启', 2, 1, 1, 1589367396, 1625531074, NULL);
INSERT INTO `tn_system_config` VALUES (12, 10, '日志记录', 'bk_log_on', '开启,关闭', '', '关闭', 2, 2, 1, 1589367443, 1625531074, NULL);
INSERT INTO `tn_system_config` VALUES (13, 10, '多图片', 'multi_pic', '', '', '[]', 8, 3, 1, 1589720725, 1625531074, NULL);
INSERT INTO `tn_system_config` VALUES (14, 1, 'jwt_token缓存时间', 'jwt_token_expire_in', '', '', '43200', 10, 9, 1, 1604042730, 1625531074, NULL);

-- ----------------------------
-- Table structure for tn_system_log
-- ----------------------------
DROP TABLE IF EXISTS `tn_system_log`;
CREATE TABLE `tn_system_log`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '系统日志的主键id',
  `user_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作的用户名',
  `controller` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作的控制器',
  `method` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作的方法',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '日志信息',
  `ip_address` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '操作的ip地址',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存系统操作日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_system_log
-- ----------------------------

-- ----------------------------
-- Table structure for tn_we_chat_user
-- ----------------------------
DROP TABLE IF EXISTS `tn_we_chat_user`;
CREATE TABLE `tn_we_chat_user`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '登录用户表主键id',
  `openid` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '微信用户的open_id',
  `oc_openid` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '微信公众号openid',
  `web_openid` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '微信网站应用openid',
  `unionid` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '微信用户的unionid',
  `nick_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户的名称',
  `avatar_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户头像',
  `gender` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户性别 (0未知 1男 2女)',
  `phone_number` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户电话号码',
  `from` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '用户的登录方式（1 微信小程序 2 微信公众号 3 微信网站应用）',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '用户的状态 (0关闭 1开启)',
  `third_status` tinyint(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '第三方功能授权状态（0 没授权 1已授权）',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `openid`(`openid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1290 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存登录用户的状态' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_we_chat_user
-- ----------------------------

-- ----------------------------
-- Table structure for tn_website_case
-- ----------------------------
DROP TABLE IF EXISTS `tn_website_case`;
CREATE TABLE `tn_website_case`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '关键词',
  `desc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '描述',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 54 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存案例模型的信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_website_case
-- ----------------------------
INSERT INTO `tn_website_case` VALUES (1, '简历，小程序，简历王者，图鸟简历', '简历小程序全新上线', '<p>一个酷炫的小程序简历，能够实现职业模板的选择，简历的创建。</p>\n<p>小程序挺适合做工具栏展示类的（之前叫&ldquo;图鸟简历&rdquo;，后改名为&ldquo;简历王者&rdquo;）</p>\n<p>&nbsp;</p>\n<p>感兴趣可点击查看，然后长按识别前往小程序&ldquo;简历王者&rdquo;</p>\n<p>&nbsp;</p>\n<p>原型图链接是这个：https://www.xiaopiu.com/h5/byId?type=project&amp;id=5ddb85ea08c3bd6c1909ea95</p>\n<p>&nbsp;</p>\n<p><img class=\"wscnph\" style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200701/ee17a86a502a8bd66eb2ed8d9eca04e3.jpg\" /></p>\n<p>&nbsp;</p>\n<p><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200620/4b924e847ddb54f60223bf528e9ff75f.jpg\" /></p>\n<p><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200620/cc2101f3be347b64635488ec2748593d.jpg\" /><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200620/e3b7cd6173a742430f325c08463988db.jpg\" /><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200620/ea466c8e5fc2f2a645d5b57b6412e01b.jpg\" /><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200620/9ee0aa89a787986f868a1a5437a6521c.jpg\" /></p>');
INSERT INTO `tn_website_case` VALUES (2, '封面，微信，红包封面', '鼠年大吉封面设计', '<p>一款为群里小伙伴设计的红包封面，简约酷炫，下拉封面故事有&ldquo;新年快乐&rdquo;彩蛋。</p>\n<p>&nbsp;</p>\n<p><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200621/3b55fe39c61148750e53ad896be7da4c.jpg\" /><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200621/a1a33781c5595f87b5bd770c4e79fcbd.png\" /></p>');
INSERT INTO `tn_website_case` VALUES (4, '图鸟社区', '图鸟社区，主要是展示小伙伴们的作品，如果你也想上传自己的作品，当然热烈欢迎的。', '<p>图鸟社区，主要是展示小伙伴们的作品，如果你也想上传自己的作品，当然热烈欢迎的。</p>\n<p>&nbsp;</p>\n<h3>图鸟社区地址：<a href=\"https://www.tuniaokj.com/\">https://www.tuniaokj.com/</a></h3>\n<p>&nbsp;</p>\n<p><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200623/3659fa2038f95ba604d7be580313c858.jpg\" /><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200623/419a23af8720d1a521f79a7fc29e58f5.jpg\" /><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200623/7cd6e8b323cd00dcfb3301f043bfc5d9.jpg\" /></p>');
INSERT INTO `tn_website_case` VALUES (21, '你好，图鸟', '7月朋友圈海报', '<p><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200701/f9c90544fde3004760a72e8d83f65a6e.jpg\" /></p>');
INSERT INTO `tn_website_case` VALUES (26, '二维码，动态，gif', '个人微信二维码设计', '<p>个人微信二维码设计，因手残，已将二维码重置</p>\n<p>&nbsp;</p>\n<p><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200701/8e5c2730a643cc578a3d940f89c5eec1.jpg\" /><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200701/fd47b044434ca4e3541a059aab1d6743.jpg\" /><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200701/68c408523a010abfc437cacd8beb6237.gif\" /><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200701/769485c159c52ddcb0b9075cf90b1356.jpg\" /></p>\n<p><img class=\"wscnph\" style=\"display: block; margin-left: auto; margin-right: auto;\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200702/6d7e6dfe5f5c492e35db2ee1db242ddc.gif\" /></p>');

-- ----------------------------
-- Table structure for tn_website_information
-- ----------------------------
DROP TABLE IF EXISTS `tn_website_information`;
CREATE TABLE `tn_website_information`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键',
  `keywords` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '关键词',
  `desc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '描述',
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存资讯模型的信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_website_information
-- ----------------------------
INSERT INTO `tn_website_information` VALUES (2, '资讯，图鸟，语雀', '语雀素材地址', '<p>语雀素材地址<span style=\"color: #0000ff;\"><strong>：<a style=\"color: #0000ff;\" href=\"https://www.yuque.com/colorui/images/artboards/58145\">https://www.yuque.com/colorui/images/artboards/58145</a></strong></span></p>\n<p>语雀素材下载：点击素材右上角，点击获取原图</p>\n<p><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200621/d10d9663fd59c796f281480dbf58732a.jpg\" /></p>');
INSERT INTO `tn_website_information` VALUES (5, '图鸟，官网，开源', 'uniapp小程序+前后端开源了，更有视频安装部署教程', '<section>\n<section>\n<section>\n<section></section>\n<section>\n<section>\n<section>\n<p><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20201105/26a3e4841ec98f8d090b6f979653f998.jpg\" /></p>\n</section>\n</section>\n</section>\n<section>\n<p>&nbsp;</p>\n</section>\n</section>\n</section>\n<section>\n<section>\n<section>\n<p>图鸟官网小程序开源项目，适合信息工具类小程序：</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>## 例如公司官网类(适合公司案例的宣传，资讯的发布)，</p>\n<p>## 例如个人博客类(适合放自己的作品，写自己的文章)。</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>##&nbsp;公司官网类小程序上线前建议：</p>\n<p>如果有企业认证公众号（即服务号），建议直接复用服务号资质，进行小程序注册认证，2分钟搞掂(免审核，免300元认证费)。</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>##&nbsp;个人博客类小程序上线前建议：</p>\n<p>个人小程序限制比较多，好像没什么好建议。</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>##&nbsp;图鸟小程序持续(看心情)更新丰富酷炫的界面组件、骚操作组件，满足各种需求。</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>##<strong>小程序项目开发上线建议</strong>：</p>\n<p>想法或需求&mdash;&mdash;Xmind或Excel功能需求文档&mdash;&mdash;xiaopiu原型图&mdash;&mdash;UI设计&mdash;&mdash;前后端开发(此处一万个操作)&mdash;&mdash;小程序测试&mdash;&mdash;审核上线</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section></section>\n<section>\n<p><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20201105/90bacd531ae9e7b49075b7222778884a.jpg\" /><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20201105/77a9f9715ce7388ea11e065fd4dc4395.jpg\" /></p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<section>\n<section>\n<p>&nbsp;</p>\n</section>\n<section>\n<section>\n<section>\n<p style=\"text-align: center;\"><strong>项目可白嫖，服务请尊重</strong></p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>如果<strong>具备</strong>开发能力，可对图鸟开源项目二次开发或直接上线：</p>\n<p>【通过简单的配置】￥0</p>\n<p>【快速搭建】￥0</p>\n<p>【酷炫公司官网小程序】￥0</p>\n<p>【装逼个人博客小程序】￥0</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>如果<strong>不具备</strong>开发能力，图鸟提供小程序服务：</p>\n<p>【Icon图标设计服务】￥30+</p>\n<p>【小程序后台环境搭建服务】￥329+</p>\n<p>【小程序UI界面定制服务】￥498++</p>\n<p>【小程序二次开发服务】￥2099+++</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>图鸟科技：致力于设计与开发，真的很专业提供设计开发服务；合作？欢迎来撩。</p>\n</section>\n<section>\n<p>&nbsp;</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<section>\n<section>\n<p>&nbsp;</p>\n</section>\n<section>\n<section>\n<section>\n<p style=\"text-align: center;\"><strong>继续瞎逼逼</strong></p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>原型图地址：</p>\n<p><a href=\"https://www.xiaopiu.com/h5/byId?type=project&amp;id=5dd8e6bb08c3bd6c1909836a\">https://www.xiaopiu.com/h5/byId?type=project&amp;id=5dd8e6bb08c3bd6c1909836a</a></p>\n<p>图标库地址(日常看心情更新)：</p>\n<p><a href=\"https://www.iconfont.cn/collections/detail?cid=16272\">https://www.iconfont.cn/collections/detail?cid=16272</a></p>\n<p>UI素材库地址(日常看心情更新)：</p>\n<p><a href=\"https://www.yuque.com/colorui/images/artboards/58145\">https://www.yuque.com/colorui/images/artboards/58145</a></p>\n<p>小程序前端纯静态版本(V1.0.0)：</p>\n<p><a href=\"https://github.com/TuniaoTechnology/TuniaoTechnologyOfficialWebsite\">https://github.com/TuniaoTechnology/TuniaoTechnologyOfficialWebsite</a></p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<p><strong>项目后台部署安装视频：</strong></p>\n<p><strong><a href=\"https://www.bilibili.com/video/BV1WA411J7XT?from=search&amp;seid=6329351721018724146\" target=\"_blank\" rel=\"noopener\">https://www.bilibili.com/video/BV1WA411J7XT?from=search&amp;seid=6329351721018724146</a>&nbsp;</strong></p>\n<section>\n<section>\n<section>\n<section>\n<p><strong>项目开源地址(uniapp版本+后端)</strong><strong>：</strong></p>\n<p><a href=\"https://gitee.com/TSpecific/tn_website_opensource\"><strong>https://gitee.com/TSpecific/tn_website_opensource</strong></a></p>\n</section>\n<section>\n<p>&nbsp;</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>①觉得一般般，勿喷，秒怂</p>\n</section>\n<section>\n<p>②开源不赚钱，单纯分享，同时找合作</p>\n</section>\n<section>\n<p>&nbsp;</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>可添加微信：<strong>tuniaokewo</strong></p>\n<p>备注：【入群】或【合作】</p>\n</section>\n<p><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200919/269ce0490a78404c4d73a949995f5dd0.jpg\" /><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200919/8402e40e81079c86d5c2810d889f092c.jpg\" /></p>\n</section>\n</section>\n<section>\n<p>&nbsp;</p>\n<p>V3.0uniapp+前后端已免费开源（上面就是介绍）</p>\n</section>\n<p>V2.0小程序+前后端开源即将开源中（因为有bug）</p>\n</section>');
INSERT INTO `tn_website_information` VALUES (6, '图鸟，官网，开源', '虚位以待', '<p style=\"text-align: center;\">这里是图鸟项目开源项目二次开发案例，也就是小伙伴们的小程序案例大集合</p>\n<p style=\"text-align: center;\">&nbsp;</p>\n<p style=\"text-align: center;\">虚位以待中</p>\n<p style=\"text-align: center;\">&nbsp;</p>\n<p>&nbsp;</p>');
INSERT INTO `tn_website_information` VALUES (7, '图鸟，官网，开源', '图鸟项目小程序+前后端开源了，更有视频安装部署教程', '<section>\n<section>\n<section>\n<section>\n<p><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20201105/7af412e8c7d8642ec947a39109d50832.jpg\" /></p>\n</section>\n<section>\n<section>\n<section>\n<p>&nbsp;</p>\n</section>\n</section>\n</section>\n<section>\n<p>&nbsp;</p>\n</section>\n</section>\n</section>\n<section>\n<section>\n<section>\n<p>图鸟官网小程序开源项目，适合信息工具类小程序：</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>## 例如公司官网类(适合公司案例的宣传，资讯的发布)，</p>\n<p>## 例如个人博客类(适合放自己的作品，写自己的文章)。</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>##&nbsp;公司官网类小程序上线前建议：</p>\n<p>如果有企业认证公众号（即服务号），建议直接复用服务号资质，进行小程序注册认证，2分钟搞掂(免审核，免300元认证费)。</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>##&nbsp;个人博客类小程序上线前建议：</p>\n<p>个人小程序限制比较多，好像没什么好建议。</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>##&nbsp;图鸟小程序持续(看心情)更新丰富酷炫的界面组件、骚操作组件，满足各种需求。</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>##<strong>小程序项目开发上线建议</strong>：</p>\n<p>想法或需求&mdash;&mdash;Xmind或Excel功能需求文档&mdash;&mdash;xiaopiu原型图&mdash;&mdash;UI设计&mdash;&mdash;前后端开发(此处一万个操作)&mdash;&mdash;小程序测试&mdash;&mdash;审核上线</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section></section>\n<section>\n<p><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20201105/74893f69b1496c72a1d9765f6d09070f.jpg\" /><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20201105/9768923a9e222ebe51802339d2a473ab.jpg\" /></p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<section>\n<section>\n<p>&nbsp;</p>\n</section>\n<section>\n<section>\n<section>\n<p style=\"text-align: center;\"><strong>项目可白嫖，服务请尊重</strong></p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>如果<strong>具备</strong>开发能力，可对图鸟开源项目二次开发或直接上线：</p>\n<p>【通过简单的配置】￥0</p>\n<p>【快速搭建】￥0</p>\n<p>【酷炫公司官网小程序】￥0</p>\n<p>【装逼个人博客小程序】￥0</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>如果<strong>不具备</strong>开发能力，图鸟提供小程序服务：</p>\n<p>【Icon图标设计服务】￥30+</p>\n<p>【小程序后台环境搭建服务】￥329+</p>\n<p>【小程序UI界面定制服务】￥498++</p>\n<p>【小程序二次开发服务】￥2099+++</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>图鸟科技：致力于设计与开发，真的很专业提供设计开发服务；合作？欢迎来撩。</p>\n</section>\n<section>\n<p>&nbsp;</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<section>\n<section>\n<p>&nbsp;</p>\n</section>\n<section>\n<section>\n<section>\n<p style=\"text-align: center;\"><strong>继续瞎逼逼</strong></p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>原型图地址<span style=\"color: #0000ff;\"><strong>：</strong></span></p>\n<p><span style=\"color: #0000ff;\"><strong><a style=\"color: #0000ff;\" href=\"https://www.xiaopiu.com/h5/byId?type=project&amp;id=5dd8e6bb08c3bd6c1909836a\">https://www.xiaopiu.com/h5/byId?type=project&amp;id=5dd8e6bb08c3bd6c1909836a</a></strong></span></p>\n<p>图标库地址(日常看心情更新)<strong><span style=\"color: #0000ff;\">：</span></strong></p>\n<p><strong><span style=\"color: #0000ff;\"><a style=\"color: #0000ff;\" href=\"https://www.iconfont.cn/collections/detail?cid=16272\">https://www.iconfont.cn/collections/detail?cid=16272</a></span></strong></p>\n<p>UI素材库地址(日常看心情更新)<span style=\"color: #0000ff;\"><strong>：</strong></span></p>\n<p><span style=\"color: #0000ff;\"><strong><a style=\"color: #0000ff;\" href=\"https://www.yuque.com/colorui/images/artboards/58145\">https://www.yuque.com/colorui/images/artboards/58145</a></strong></span></p>\n<p>小程序前端纯静态版本(V1.0.0)<span style=\"color: #0000ff;\"><strong>：</strong></span></p>\n<p><span style=\"color: #0000ff;\"><strong><a style=\"color: #0000ff;\" href=\"https://github.com/TuniaoTechnology/TuniaoTechnologyOfficialWebsite\">https://github.com/TuniaoTechnology/TuniaoTechnologyOfficialWebsite</a></strong></span></p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<p><strong>项目后台部署安装视频<span style=\"color: #0000ff;\">：</span></strong></p>\n<p><span style=\"color: #0000ff;\"><strong><a style=\"color: #0000ff;\" href=\"https://www.bilibili.com/video/BV1WA411J7XT?from=search&amp;seid=6329351721018724146\" target=\"_blank\" rel=\"noopener\">https://www.bilibili.com/video/BV1WA411J7XT?from=search&amp;seid=6329351721018724146</a></strong></span></p>\n<section>\n<section>\n<section>\n<section>\n<p><strong>项目开源地址(uniapp版本+后端)<span style=\"color: #0000ff;\">：</span></strong></p>\n<p><strong><span style=\"color: #0000ff;\"><a style=\"color: #0000ff;\" href=\"https://gitee.com/TSpecific/tn_website_opensource\">https://gitee.com/TSpecific/tn_website_opensource</a></span></strong></p>\n</section>\n<section>\n<p>&nbsp;</p>\n</section>\n</section>\n</section>\n</section>\n<p><img src=\"https://cdn.jsdelivr.net/npm/tinymce-all-in-one@4.9.3/plugins/emoticons/img/smiley-laughing.gif\" alt=\"laughing\" /></p>\n<section>\n<section>\n<section>\n<section>\n<p>①觉得一般般，勿喷，秒怂</p>\n</section>\n<section>\n<p>②开源不赚钱，单纯分享，同时找合作</p>\n</section>\n<section>\n<p>&nbsp;</p>\n</section>\n</section>\n</section>\n</section>\n<p>&nbsp;</p>\n<p>&nbsp;</p>\n<section>\n<section>\n<section>\n<section>\n<p>可添加微信：<strong>tuniaokewo</strong></p>\n<p>备注：【入群】或【合作】</p>\n</section>\n<p><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200919/269ce0490a78404c4d73a949995f5dd0.jpg\" /><img class=\"wscnph\" src=\"http://website_open.tuniaokj.com/storage/uploads/content_file/20200919/8402e40e81079c86d5c2810d889f092c.jpg\" /></p>\n</section>\n</section>\n<section>\n<p>&nbsp;</p>\n</section>\n</section>');

-- ----------------------------
-- Table structure for tn_wx_config
-- ----------------------------
DROP TABLE IF EXISTS `tn_wx_config`;
CREATE TABLE `tn_wx_config`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '设置的主键id',
  `pid` int(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT '设置的父级id',
  `cn_name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '配置对应的中文名',
  `en_name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '配置对应的字段名称',
  `values` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置的可选值',
  `tips` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置提示值',
  `value` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置的值',
  `type` tinyint(2) UNSIGNED NOT NULL DEFAULT 1 COMMENT '配置的类型 1：文本输入框 2：单选按钮 3：复选框 4：下拉菜单 5：文本域 6：富文本 7：单图片 8：多图片 9：附件上传',
  `sort` int(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '配置的排序序号',
  `status` tinyint(1) UNSIGNED NOT NULL DEFAULT 1 COMMENT '配置项的状态（1 开启 0 关闭）',
  `create_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `update_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `delete_time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `id_en_name`(`id`, `en_name`) USING BTREE,
  INDEX `en_name`(`en_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '保存系统配置的相关信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tn_wx_config
-- ----------------------------
INSERT INTO `tn_wx_config` VALUES (1, 0, '小程序基础设置', '', '', '', '', 0, 1, 1, 1590383845, 1590383845, NULL);
INSERT INTO `tn_wx_config` VALUES (2, 1, 'APP_ID', 'mp_app_id', '', '请输入开发者的APP_ID', '', 1, 1, 1, 1590383974, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (3, 1, 'APP_SECRET', 'mp_app_secret', '', '请输入开发者APP_SECRET', '', 1, 2, 1, 1590384800, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (4, 1, '用户认证', 'mp_auth_user', '开启,关闭', '是否开启用户认证功能', '关闭', 2, 3, 1, 1590384902, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (5, 0, '小程序图片设置', '', '', '', '', 0, 2, 1, 1590384928, 1590384928, NULL);
INSERT INTO `tn_wx_config` VALUES (6, 5, '个人背景图', 'mp_person_bg', '', '请选择个人页面的背景图片', '/storage/uploads/img_list/94/65cb49497cc16e193792cd3e7151f5.png', 7, 1, 1, 1590384979, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (7, 5, '首页Logo图', 'mp_index_logo', '', '上传首页左上角logo图片', '/storage/uploads/img_list/0c/2683c1e4d721f44aa466869a4c9d11.jpg', 7, 2, 1, 1590385012, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (8, 5, '关于我们背景', 'mp_about_bg', '', '请选择关于我们的背景图片', '/storage/uploads/img_list/5f/c66ad1b84fe9d17c1c7a7666c633db.png', 7, 3, 1, 1590385055, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (9, 0, '小程序页面设置', '', '', '', '', 0, 3, 1, 1590385108, 1590385108, NULL);
INSERT INTO `tn_wx_config` VALUES (10, 9, '搜索框提示', 'mp_search_input_tips', '', '搜索框的提示信息', '搜索‘广州图鸟科技有限公司’', 1, 1, 1, 1590385174, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (11, 9, '默认搜索历史关键词', 'mp_default_search_keyword', '', '关键词之间用英文逗号隔开(,)', '图鸟,图鸟科技', 5, 2, 1, 1590385211, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (12, 1, '开启第三方认证', 'mp_auth_third', '开启,关闭', '是否开启第三方认证', '关闭', 2, 4, 1, 1590464707, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (13, 0, '公众号基础设置', '', '', '', '', 0, 4, 1, 1593055939, 1593055939, NULL);
INSERT INTO `tn_wx_config` VALUES (14, 13, 'APP_ID', 'oc_app_id', '', '请输入app_id', '', 1, 1, 1, 1593055970, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (15, 13, 'APP_SECRET', 'oc_app_secret', '', '请输入app_secret', '', 1, 2, 1, 1593056009, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (16, 0, '公众号图片设置', '', '', '', '', 0, 5, 1, 1593056253, 1593056253, NULL);
INSERT INTO `tn_wx_config` VALUES (17, 16, '个人背景图', 'oc_person_bg', '', '', '', 7, 1, 1, 1593056279, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (18, 16, '首页Logo图', 'oc_index_logo', '', '', '', 7, 2, 1, 1593056303, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (19, 16, '关于我们背景', 'oc_about_bg', '', '', '', 7, 3, 1, 1593057970, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (20, 0, '公众号页面设置', '', '', '', '', 0, 6, 1, 1593058004, 1593058004, NULL);
INSERT INTO `tn_wx_config` VALUES (21, 20, '搜索框提示', 'oc_search_input_tips', '', '', '搜索‘广州图鸟科技有限公司’', 1, 1, 1, 1593058056, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (22, 20, '默认搜索历史关键词', 'oc_default_search_keyword', '', '', '图鸟,图鸟科技', 1, 2, 1, 1593058097, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (23, 0, '第三方开放平台设置', '', '', '', '', 0, 7, 1, 1601367732, 1601367732, NULL);
INSERT INTO `tn_wx_config` VALUES (24, 23, 'APP_ID', 'op_tpp_app_id', '', '', '', 1, 1, 1, 1601367770, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (25, 23, 'APP_SECRET', 'op_tpp_app_secret', '', '', '', 1, 2, 1, 1601367957, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (26, 23, 'Receive_Msg_Verify_Token', 'op_tpp_receive_msg_verify_token', '', '', '', 1, 3, 1, 1601368712, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (27, 23, 'Receive_Msg_EnDe_key', 'op_tpp_receive_msg_ende_key', '', '', '', 1, 4, 1, 1601382770, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (28, 13, '公众号管理员open_id', 'oc_admin_open_id', '', '', '', 1, 3, 1, 1602138680, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (29, 13, '第三方平台授权失效通知template_id', 'oc_third_party_platform_failure_template_id', '', '', '', 1, 5, 1, 1602138830, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (30, 13, '公众号开发open_id', 'oc_development_open_id', '', '', '', 1, 4, 1, 1602496737, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (31, 1, 'Receive_Msg_Verify_Token', 'mp_receive_msg_verify_token', '', '', '', 1, 5, 1, 1602580992, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (32, 1, 'Receive_Msg_EnDe_Key', 'mp_receive_msg_ende_key', '', '', '', 1, 6, 1, 1602581023, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (33, 0, '微信网站应用配置', '', '', '', '', 0, 8, 1, 1603442306, 1603442306, NULL);
INSERT INTO `tn_wx_config` VALUES (34, 33, 'APP_ID', 'wx_web_app_app_id', '', '', '', 1, 1, 1, 1603442344, 1625554308, NULL);
INSERT INTO `tn_wx_config` VALUES (35, 33, 'APP_SECRET', 'wx_web_app_app_secret', '', '', '', 1, 2, 1, 1603442383, 1625554308, NULL);

SET FOREIGN_KEY_CHECKS = 1;
